﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.System.User
{
    public class FriendModel
    {
        public Guid SenderId { get; set; }
        public string SenderName { get; set; }
        public Guid ReceiverId { get; set; }
        public string ReceiverName { get; set; }
        public string Status { get; set; }
        public DateTime DateSendRequest { get; set; }
        public DateTime DateUpdateRequest { get; set; }
    }
}
