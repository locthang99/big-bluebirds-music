﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Page
{
    public class PagingRequest
    {
        public int Index { get; set; }
        public int PageSize { get; set; }

    }
}
