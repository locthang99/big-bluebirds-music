﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Page
{
    public class PagingRequestValidator: AbstractValidator<PagingRequest>
    {
        public PagingRequestValidator()
        {
            RuleFor(x => x.Index).NotEmpty().WithMessage("Index is not empty").GreaterThanOrEqualTo(1).WithMessage("Index must greater than or equal 1");
            RuleFor(x => x.PageSize).NotEmpty().WithMessage("Page Size is not empty").GreaterThanOrEqualTo(1).WithMessage("Page Size must greater than or equal 1"); ;
        }
    }
}
