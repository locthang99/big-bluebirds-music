﻿using Models.Catalog.Owner;
using Models.Catalog.Song;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Catalog.PlayList
{
    public class PlayListModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DateCreate { get; set; }
        public int TotalSong { get; set; }
        public int TotalListen { get; set; }
        public int TotalLike { get; set; }
        public int TotalCmt { get; set; }
        public string Thumbnail { get; set; }
        public string PlaylistType { get; set; }
        public OwnerModel Owner { get; set; }
        public List<SongModel> ListSong { get; set; }

    }
}
