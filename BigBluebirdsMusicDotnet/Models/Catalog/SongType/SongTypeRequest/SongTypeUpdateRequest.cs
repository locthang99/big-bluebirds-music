﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Catalog.SongType.SongTypeRequest
{
    public class SongTypeUpdateRequest
    {
        //public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
