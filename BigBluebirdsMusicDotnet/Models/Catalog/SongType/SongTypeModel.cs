﻿using Models.Catalog.Song;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Catalog.SongType
{
    public class SongTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        //public List<SongModel> ListSong { get; set; }
    }
}
