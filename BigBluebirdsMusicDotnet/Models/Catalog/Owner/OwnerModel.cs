﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Catalog.Owner
{
    public class OwnerModel
    {
        public Guid OwnerId { get; set; }
        public string NameOwner { get; set; }
        public string Thumbnail { get; set; }
    }
}
