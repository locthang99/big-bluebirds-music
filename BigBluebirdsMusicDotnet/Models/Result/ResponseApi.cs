﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Result
{
    public abstract class ResponseApi
    {
        public int Status { get; set;}
        public string Msg { get; set; }
        public object Data { get; set; }

    }
    public class UnauthorResApi:ResponseApi
    {
        public UnauthorResApi()
        {
            Status = 401;
            Msg = "You are not login or token invalid";
        }
    }
    public class NotfoundResApi : ResponseApi
    {
        public NotfoundResApi()
        {
            Status = 404;
        }
    }

    public class BadRequestResApi : ResponseApi
    {
        public BadRequestResApi()
        {
            Status = 400;
        }
    }

    public class OkResApi : ResponseApi
    {
        public OkResApi()
        {
            Status = 200;
        }
    }
    public class OkCUDApi : ResponseApi
    {
        public string ObjectId { get; set; }
        public OkCUDApi()
        {
            Status = 200;
        }
    }

    public class ForbirdResApi : ResponseApi
    {
        public ForbirdResApi()
        {
            Status = 403;
            Msg = "You don't have permission to chang this resource";
        }
    }

}
