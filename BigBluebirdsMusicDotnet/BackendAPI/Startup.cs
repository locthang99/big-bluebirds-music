using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.AuthorData;
using Application.Catalog.History;
using Application.Catalog.PlayList;
using Application.Catalog.Song;
using Application.Catalog.SongType;
using Application.Common;
using Application.System.FacebookAuth;
using Application.System.User;
using Data.EF;
using Data.Entities;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Models.System.User.UserRequest;
using Models.System.User.UserValidator;
using Newtonsoft.Json;
using NSwag.Generation.Processors.Security;
using NSwag;
using Application.Base;
using Application.Catalog.Tag;
using Application.System.Dashboard;
using Application.Catalog.Recommendation;
using Application.Cache;

namespace BackendAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<BigBluebirdsDbContext>(options =>
            //options.UseSqlServer("Server=.;Database=BigBluebirdsDb;Trusted_Connection=True;")
            options.UseSqlServer(Configuration.GetConnectionString("BigBluebirdsDb"), options => options.EnableRetryOnFailure())

            //sqlServerOptionsAction: sqlOptions =>
            //{
            //    sqlOptions.EnableRetryOnFailure();
            //}

            );

            services.AddIdentity<User, AppRole>(options => options.SignIn.RequireConfirmedAccount = true).AddEntityFrameworkStores<BigBluebirdsDbContext>()
                .AddDefaultTokenProviders();

            //DI service
            //services.AddTransient<IManageProductService, ManageProductService
            services.AddTransient<IManageSongService, ManageSongService>();
            services.AddTransient<IManageSongTypeService, ManageSongTypeService>();
            services.AddTransient<IManageTagService, ManageTagService>();
            services.AddTransient<IManagePlayListService, ManagePlayListService>();
            services.AddTransient<IStorageService, FileStorageService>();
            services.AddTransient<IManageHistoryService, ManageHistoryService>();
            services.AddTransient<IFacebookAuthService, FacebookAuthService>();
            services.AddTransient<IApplicationServiceBase, ApplicationServiceBase>();
            services.AddTransient<IManageDbService, ManageDbService>();
            services.AddTransient<IRecommendService, RecommendService>();
            services.AddTransient<ICacheService, CacheService>();

            //DI Identify
            services.AddTransient<UserManager<User>, UserManager<User>>();
            services.AddTransient<SignInManager<User>, SignInManager<User>>();
            services.AddTransient<RoleManager<AppRole>, RoleManager<AppRole>>();
            services.AddTransient<IManageUserService, ManageUserService>();
            services.AddTransient<IValidator<UserLoginRequest>, UserLoginValidator>();
            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));
            services.AddControllers().AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<UserLoginValidator>());

            //add Swagger
            services.AddSwaggerDocument(config =>
            {
                config.PostProcess = document =>
                {
                    document.Info.Version = "v1";
                    document.Info.Title = "BigBlurBirds API";
                    document.Info.Description = "ASP.NET Core web API";
                    //document.Info.TermsOfService = "None";
                    //document.Info.Contact = new NSwag.OpenApiContact
                    //{
                    //    Name = "Shayne Boyer",
                    //    Email = string.Empty,
                    //    Url = "https://twitter.com/spboyer"
                    //};
                    //document.Info.License = new NSwag.OpenApiLicense
                    //{
                    //    Name = "Use under LICX",
                    //    Url = "https://example.com/license"
                    //};
                };
                config.AddSecurity("JWT",Enumerable.Empty<string>(),
                new NSwag.OpenApiSecurityScheme
                {
                    Type = OpenApiSecuritySchemeType.ApiKey,
                    Name = "Authorization",
                    Description = "Copy 'Bearer ' + valid JWT token into field",
                    In = OpenApiSecurityApiKeyLocation.Header
                });
                config.OperationProcessors.Add(new AspNetCoreOperationSecurityScopeProcessor("JWT"));

            });
            //services.AddSwaggerGen(c =>
            //{
            //    c.SwaggerDoc("v1", new OpenApiInfo { Title = "Swagger Big BuleBirds", Version = "v1" });
            //    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            //    {
            //        Description = @"JWT Authorization header using the Bearer scheme. \r\n\r\n
            //          Enter 'Bearer' [space] and then your token in the text input below.
            //          \r\n\r\nExample: 'Bearer 12345abcdef'",
            //        Name = "Authorization",
            //        In = ParameterLocation.Header,
            //        Type = SecuritySchemeType.ApiKey,
            //        Scheme = "Bearer"
            //    });

            //    c.AddSecurityRequirement(new OpenApiSecurityRequirement()
            //      {
            //        {
            //          new OpenApiSecurityScheme
            //          {
            //            Reference = new OpenApiReference
            //              {
            //                Type = ReferenceType.SecurityScheme,
            //                Id = "Bearer"
            //              },
            //              Scheme = "oauth2",
            //              Name = "Bearer",
            //              In = ParameterLocation.Header,
            //            },
            //            new List<string>()
            //          }
            //        });
            //});
            string issuer = Configuration.GetValue<string>("Tokens:Issuer");
            string signingKey = Configuration.GetValue<string>("Tokens:Key");
            byte[] signingKeyBytes = System.Text.Encoding.UTF8.GetBytes(signingKey);

            //authentication header swagger
            services.AddAuthentication(opt =>
            {
                opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidIssuer = issuer,
                    ValidateAudience = true,
                    ValidAudience = issuer,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ClockSkew = System.TimeSpan.Zero,
                    IssuerSigningKey = new SymmetricSecurityKey(signingKeyBytes)
                };
            }).AddFacebook(facebookOptions =>
            {
                //facebookOptions.AppId = Configuration["Authentication:Facebook:AppId"];
                //facebookOptions.AppSecret = Configuration["Authentication:Facebook:AppSecret"];
                facebookOptions.AppId = "1007359859770404";
                facebookOptions.AppSecret = "1ec1e1c31c5998642172c70c3026c499";
            });
            services.AddHttpClient();
            //Authorize handler
            services.AddSingleton<IAuthorizationHandler, SongOwnerAuthorizationHandler>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //Auto migrate DB
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<BigBluebirdsDbContext>();
                context.Database.Migrate();
            }
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseCors("MyPolicy");
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            //use swagger
            app.UseOpenApi();
            app.UseSwaggerUi3();
            //app.UseSwagger();
            //app.UseSwaggerUI(c =>
            //{
            //    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Swagger BigBlueBirds V1");
            //});
        }
    }
}
