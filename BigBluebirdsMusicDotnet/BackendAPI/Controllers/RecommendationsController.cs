﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Catalog.Recommendation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models.Page;
using Models.Result;

namespace BackendAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RecommendationsController : ControllerBase
    {
        private readonly IRecommendService _recommendService;
        public RecommendationsController(IRecommendService recommendService)
        {
            _recommendService = recommendService;
        }
        [HttpGet("UserBase/Song")]
        public async Task<IActionResult> UserBasedCollaborativeFilterSong([FromQuery] Guid userID, [FromQuery] string ActionType,[FromQuery] PagingRequest pg)
        {
            var data = await _recommendService.UserBasedCollaborativeFilterSong(userID,1000,1000,ActionType,"SONG",pg);
            if(data==null)
                return NotFound(new NotfoundResApi() { Msg = "Get Recommend Song FAILED" });
            return Ok(new OkResApi() { Msg = "Recommend SongID by UserBase Jaccard Similarity", Data = data });
        }

        [HttpGet("UserBase/Playlist")]
        public async Task<IActionResult> UserBasedCollaborativeFilterPlaylist([FromQuery] Guid userID, [FromQuery] string ActionType, [FromQuery] PagingRequest pg)
        {
            var data = await _recommendService.UserBasedCollaborativeFilterPlaylist(userID, 1000, 1000, ActionType, "PLAYLIST", pg);
            if (data == null)
                return NotFound(new NotfoundResApi() { Msg = "Get Recommend Playlist FAILED" });
            return Ok(new OkResApi() { Msg = "Recommend PlaylistID by UserBase Jaccard Similarity", Data = data });
        }



        [HttpGet("ItemBase/Song")]
        public async Task<IActionResult> ItemBasedCollaborativeFilterSong([FromQuery] int songID, [FromQuery] string ActionType,[FromQuery] PagingRequest pg)
        {
            var data = await _recommendService.ItemBasedCollaborativeFilterSong(songID,ActionType, "SONG",pg);
            if (data == null)
                return NotFound(new NotfoundResApi() { Msg = "Get Recommend Song FAILED" });
            return Ok(new OkResApi() { Msg = "Recommend SongID by ItemBase Jaccard Similarity", Data = data });
        }

        [HttpGet("ItemBase/Playlist")]
        public async Task<IActionResult> ItemBasedCollaborativeFilterPlaylist([FromQuery] int plID, [FromQuery] string ActionType,[FromQuery] PagingRequest pg)
        {
            var data = await _recommendService.ItemBasedCollaborativeFilterPlaylist(plID, ActionType, "PLAYLIST", pg);
            if (data == null)
                return NotFound(new NotfoundResApi() { Msg = "Get Recommend Playlist FAILED" });
            return Ok(new OkResApi() { Msg = "Recommend PlaylistID by ItemBase Jaccard Similarity", Data = data });
        }
    }
}
