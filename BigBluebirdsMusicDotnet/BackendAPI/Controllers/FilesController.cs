﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace BackendAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FilesController : ControllerBase
    {
        [HttpGet("Images/{nameFile}")]
        public FileContentResult GetFileImage(string nameFile)
        {
            var myfile = System.IO.File.ReadAllBytes("wwwroot/Image/"+nameFile);
            return new FileContentResult(myfile, "image/jpg");
           
        }
        [HttpGet("Musics/{nameFile}")]
        public FileContentResult GetFileMusic(string nameFile)
        {
            var myfile = System.IO.File.ReadAllBytes("wwwroot/Audio/" + nameFile);
            return new FileContentResult(myfile, "audio/mpeg");

        }

        [HttpGet("CloneFileMusic")]
        public IActionResult Clone( )
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo = new System.Diagnostics.ProcessStartInfo()
            {
                WorkingDirectory = @".\NodeJSClone\NodeJSProject",
                UseShellExecute = false,
                CreateNoWindow = true,
                WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden,
                FileName = "cmd.exe",
                Arguments = "/C node index.js",
                RedirectStandardError = true,
                RedirectStandardOutput = true,
            };
            process.Start();
            var outPut = (process.StandardOutput.ReadToEnd());
            process.WaitForExit();
            return Ok(outPut);

        }
    }
}
