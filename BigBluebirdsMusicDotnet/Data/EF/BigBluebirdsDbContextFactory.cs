﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Data.EF
{
    public class BigBluebirdsDbContextFactory : IDesignTimeDbContextFactory<BigBluebirdsDbContext>
    {
        public BigBluebirdsDbContext CreateDbContext(string[] args)
        {           
                IConfigurationRoot configuration = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .Build();

                var connectionString = configuration.GetConnectionString("BigBluebirdsDb");
                var optionsBuilder = new DbContextOptionsBuilder<BigBluebirdsDbContext>();
                optionsBuilder.UseSqlServer(connectionString);

                return new BigBluebirdsDbContext(optionsBuilder.Options);
        }
    }
}

