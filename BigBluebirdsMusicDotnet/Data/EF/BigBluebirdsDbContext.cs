﻿using Data.Configurations;
using Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Shop.Data.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.EF
{
    public class BigBluebirdsDbContext : IdentityDbContext<User, AppRole, Guid>
    {
        public BigBluebirdsDbContext(DbContextOptions options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new AppConfigConfiguration());
            modelBuilder.ApplyConfiguration(new AppRoleConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new SongConfiguration());
            modelBuilder.ApplyConfiguration(new SongTypeConfiguration());
            modelBuilder.ApplyConfiguration(new PlayListConfiguration());
            modelBuilder.ApplyConfiguration(new Song_PlayListConfiguration());
            modelBuilder.ApplyConfiguration(new Song_SongTypeConfiguration());
            modelBuilder.ApplyConfiguration(new Song_OwnerConfiguration());
            modelBuilder.ApplyConfiguration(new User_Like_SongConfiguration());
            modelBuilder.ApplyConfiguration(new User_Cmt_SongConfiguration());
            modelBuilder.ApplyConfiguration(new FriendConfiguration());
            modelBuilder.ApplyConfiguration(new FileImageConfiguration());
            modelBuilder.ApplyConfiguration(new FileMusicConfiguration());
            modelBuilder.ApplyConfiguration(new HistoryConfiguration());
            modelBuilder.ApplyConfiguration(new TagConfiguration());
            modelBuilder.ApplyConfiguration(new Song_TagConfiguration());
            modelBuilder.ApplyConfiguration(new CacheDataConfiguration());

            modelBuilder.Entity<IdentityUserClaim<Guid>>().ToTable("UserClaims");
            modelBuilder.Entity<IdentityUserRole<Guid>>().ToTable("UserRoles").HasKey(x => new { x.UserId, x.RoleId });
            modelBuilder.Entity<IdentityUserLogin<Guid>>().ToTable("UserLogins").HasKey(x => x.UserId);
            modelBuilder.Entity<IdentityRoleClaim<Guid>>().ToTable("UserRoleClaims");
            modelBuilder.Entity<IdentityUserToken<Guid>>().ToTable("UserTokens").HasKey(x => x.UserId);
            //base.OnModelCreating(modelBuilder);
            modelBuilder.Seed();
        }
        public DbSet<AppConfig> AppConfig { get; set; }
        public DbSet<Song> Songs { get; set; }
        public DbSet<PlayList> PlayLists { get; set; }
        public DbSet<SongType> SongTypes { get; set; }
        public DbSet<Song_SongType> Song_SongTypes { get; set; }
        public DbSet<Song_PlayList> Song_PlayLists { get; set; }
        public DbSet<Song_Owner> Song_Owers { get; set; }
        public DbSet<User_Like_Song> User_Like_Songs { get; set; }
        public DbSet<User_Cmt_Song> User_Cmt_Songs { get; set; }
        public DbSet<User_Like_Playlist> User_Like_Playlists { get; set; }
        public DbSet<User_Cmt_Playlist> User_Cmt_Playlists { get; set; }
        public DbSet<Friend> Friends { get; set; }
        public DbSet<FileImage> FileImages { get; set; }
        public DbSet<FileMusic> FileMusics { get; set; }
        public DbSet<History> Histories { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Song_Tag> Song_Tags { get; set; }
        public DbSet<CacheData> CacheDatas { get; set; }

    }
}
