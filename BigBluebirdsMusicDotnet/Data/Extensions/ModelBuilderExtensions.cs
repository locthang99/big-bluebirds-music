﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Models.Enum;

namespace Shop.Data.Extensions
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {

            #region Config
            modelBuilder.Entity<AppConfig>().HasData(
               new AppConfig() { Key = "HomeTitle", Value = "This is home page of BigBluebirds" },
               new AppConfig() { Key = "HomeKeyword", Value = "This is keyword of BigBluebirds" },
               new AppConfig() { Key = "HomeDescription", Value = "This is description of BigBluebirds" }
               );
            #endregion
            #region Account and Role
            var roleIdAdmin = new Guid("00000000-0000-0000-0000-000000000001");
            var roleIdUser = new Guid("00000000-0000-0000-0000-000000000002");
            var roleIdSinger = new Guid("00000000-0000-0000-0000-000000000003");
            var roleIdAuthor = new Guid("00000000-0000-0000-0000-000000000004");

            var adminId = new Guid("00000000-0000-0000-0000-000000000001");
            var userId = new Guid("00000000-0000-0000-0000-000000000002");
            var singerId = new Guid("00000000-0000-0000-0000-000000000003");
            var authorId = new Guid("00000000-0000-0000-0000-000000000004");
            modelBuilder.Entity<AppRole>().HasData(new AppRole
            {
                Id = roleIdAdmin,
                Name = "admin",
                NormalizedName = "admin",
                Description = "Administrator role"
            },
            new AppRole
            {
                Id = roleIdUser,
                Name = "user",
                NormalizedName = "user",
                Description = "User role"
            },
            new AppRole
            {
                Id = roleIdSinger,
                Name = "singer",
                NormalizedName = "singer",
                Description = "Singer role"
            },
            new AppRole
            {
                Id = roleIdAuthor,
                Name = "author",
                NormalizedName = "author",
                Description = "Author role"
            }
            );

            var hasher = new PasswordHasher<User>();
            modelBuilder.Entity<User>().HasData(new User
            {
                Id = adminId,
                UserName = "admin",
                NormalizedUserName = "admin",
                Email = "admin@gmail.com",
                NormalizedEmail = "admin@gmail.com",
                EmailConfirmed = true,
                PasswordHash = hasher.HashPassword(null, "Abcd1234$"),
                SecurityStamp = string.Empty,
                FirstName = "Admin",
                LastName = "BigBluebirds",
                AccountType = AccountType.SYSTEM.ToString()
            },
            new User
            {
                Id = userId,
                UserName = "user",
                NormalizedUserName = "user",
                Email = "user@gmail.com",
                NormalizedEmail = "user@gmail.com",
                EmailConfirmed = true,
                PasswordHash = hasher.HashPassword(null, "Abcd1234$"),
                SecurityStamp = string.Empty,
                FirstName = "User",
                LastName = "User",
                AccountType = AccountType.SYSTEM.ToString()
            },
             new User
             {
                 Id = singerId,
                 UserName = "singer",
                 NormalizedUserName = "singer",
                 Email = "singer@gmail.com",
                 NormalizedEmail = "singer@gmail.com",
                 EmailConfirmed = true,
                 PasswordHash = hasher.HashPassword(null, "Abcd1234$"),
                 SecurityStamp = string.Empty,
                 FirstName = "Singer",
                 LastName = "Singer",
                 AccountType = AccountType.SYSTEM.ToString()
             },
             new User
             {
                 Id = authorId,
                 UserName = "author",
                 NormalizedUserName = "author",
                 Email = "author@gmail.com",
                 NormalizedEmail = "author@gmail.com",
                 EmailConfirmed = true,
                 PasswordHash = hasher.HashPassword(null, "Abcd1234$"),
                 SecurityStamp = string.Empty,
                 FirstName = "Author",
                 LastName = "Author",
                 AccountType = AccountType.SYSTEM.ToString()
             }

            );

            modelBuilder.Entity<IdentityUserRole<Guid>>().HasData(new IdentityUserRole<Guid>
            {
                RoleId = roleIdAdmin,
                UserId = adminId
            },
            new IdentityUserRole<Guid>
            {
                RoleId = roleIdUser,
                UserId = userId
            },
            new IdentityUserRole<Guid>
            {
                RoleId = roleIdSinger,
                UserId = singerId
            },
            new IdentityUserRole<Guid>
            {
                RoleId = roleIdAuthor,
                UserId = authorId
            }
            );
            #endregion
            #region Song
           // modelBuilder.Entity<Song>().HasData(
           //new Song()
           //{
           //    Id = 1,
           //    Name = "Bai Hat 1",
           //    Description = "Bai hat demo cuc manh",
           //    DateCreate = DateTime.Now,
           //    TotalListen = 0,
           //    TotalLike = 0,
           //    TotalCmt = 0,
           //    Lyric = "data.com/lyrc....",
           //    Duration = 500
           //},
           //new Song()
           //{
           //    Id = 2,
           //    Name = "Bai Hat 2",
           //    Description = "Bai hat demo 2 cuc manh",
           //    DateCreate = DateTime.Now,
           //    TotalListen = 0,
           //    TotalLike = 0,
           //    TotalCmt = 0,
           //    Lyric = "data.com/lyrc....",
           //    Duration = 1
           //}
           //);

            #endregion
            #region PlayList
            modelBuilder.Entity<PlayList>().HasData(
           new PlayList()
           {
               Id = 1,
               Name = "Nhạc Trẻ",
               DateCreate = DateTime.Now,
               Description = "Danh sách nhạc trẻ",
               TotalListen = 0,
               TotalLike = 0,
               TotalCmt = 0,
               TotalSong = 0,
               PlaylistType = PlaylistType.SYSTEM.ToString(),
               OwnerId = adminId
           },
           new PlayList()
           {
               Id = 2,
               Name = "Rock Việt",
               DateCreate = DateTime.Now,
               Description = "Danh sách Rock Việt",
               TotalListen = 0,
               TotalLike = 0,
               TotalCmt = 0,
               TotalSong = 0,
               PlaylistType = PlaylistType.SYSTEM.ToString(),
               OwnerId = adminId
           },
           new PlayList()
           {
               Id = 3,
               Name = "Nhạc Cách Mạng",
               DateCreate = DateTime.Now,
               Description = "Danh sách nhạc Cách Mạng",
               TotalListen = 0,
               TotalLike = 0,
               TotalCmt = 0,
               TotalSong = 0,
               PlaylistType = PlaylistType.SYSTEM.ToString(),
               OwnerId = adminId
           },
           new PlayList()
           {
               Id = 4,
               Name = "Nhạc Trịnh",
               DateCreate = DateTime.Now,
               Description = "Danh sách nhạc Trịnh",
               TotalListen = 0,
               TotalLike = 0,
               TotalCmt = 0,
               TotalSong = 0,
               PlaylistType = PlaylistType.SYSTEM.ToString(),
               OwnerId = adminId
           },
           new PlayList()
           {
               Id = 5,
               Name = "Nhạc Không Lời",
               DateCreate = DateTime.Now,
               Description = "Danh sách nhạc Không lời",
               TotalListen = 0,
               TotalLike = 0,
               TotalCmt = 0,
               TotalSong = 0,
               PlaylistType = PlaylistType.SYSTEM.ToString(),
               OwnerId = adminId
           },
           new PlayList()
           {
               Id = 6,
               Name = "Dance Viet ",
               DateCreate = DateTime.Now,
               Description = "Danh sách Dance Viet",
               TotalListen = 0,
               TotalLike = 0,
               TotalCmt = 0,
               TotalSong = 0,
               PlaylistType = PlaylistType.SYSTEM.ToString(),
               OwnerId = adminId
           },
           new PlayList()
           {
               Id =7,
               Name = "V-Pop ",
               DateCreate = DateTime.Now,
               Description = "Danh sách V-Pop",
               TotalListen = 0,
               TotalLike = 0,
               TotalCmt = 0,
               TotalSong = 0,
               PlaylistType = PlaylistType.SYSTEM.ToString(),
               OwnerId = adminId
           },
           new PlayList()
           {
               Id = 8,
               Name = "Rap Việt ",
               DateCreate = DateTime.Now,
               Description = "Danh sách Rap Việt",
               TotalListen = 0,
               TotalLike = 0,
               TotalCmt = 0,
               TotalSong = 0,
               PlaylistType = PlaylistType.SYSTEM.ToString(),
               OwnerId = adminId
           },
           new PlayList()
           {
               Id = 9,
               Name = "Nhạc Trữ Tình",
               DateCreate = DateTime.Now,
               Description = "Danh sách nhạc Trữ tình",
               TotalListen = 0,
               TotalLike = 0,
               TotalCmt = 0,
               TotalSong = 0,
               PlaylistType = PlaylistType.SYSTEM.ToString(),
               OwnerId = adminId
           },
           new PlayList()
           {
               Id = 10,
               Name = "Nhạc Dân Ca",
               DateCreate = DateTime.Now,
               Description = "Danh sách nhac Dân ca",
               TotalListen = 0,
               TotalLike = 0,
               TotalCmt = 0,
               TotalSong = 0,
               PlaylistType = PlaylistType.SYSTEM.ToString(),
               OwnerId = adminId
           },
           new PlayList()
           {
               Id = 11,
               Name = "Nhạc Thiếu Nhi",
               DateCreate = DateTime.Now,
               Description = "Danh sách nhạc Thiếu Nhi",
               TotalListen = 0,
               TotalLike = 0,
               TotalCmt = 0,
               TotalSong = 0,
               PlaylistType = PlaylistType.SYSTEM.ToString(),
               OwnerId = adminId
           },
           new PlayList()
           {
               Id = 12,
               Name = "Nhạc him",
               DateCreate = DateTime.Now,
               Description = "Danh sách nhạc Phim",
               TotalListen = 0,
               TotalLike = 0,
               TotalCmt = 0,
               TotalSong = 0,
               PlaylistType = PlaylistType.SYSTEM.ToString(),
               OwnerId = adminId
           },
           new PlayList()
           {
               Id = 13,
               Name = "Cải Lương",
               DateCreate = DateTime.Now,
               Description = "Danh sách Cải Lương",
               TotalListen = 0,
               TotalLike = 0,
               TotalCmt = 0,
               TotalSong = 0,
               PlaylistType = PlaylistType.SYSTEM.ToString(),
               OwnerId = adminId
           },
           new PlayList()
           {
               Id = 14,
               Name = "EDM Việt ",
               DateCreate = DateTime.Now,
               Description = "Danh sách EDM Việt",
               TotalListen = 0,
               TotalLike = 0,
               TotalCmt = 0,
               TotalSong = 0,
               PlaylistType = PlaylistType.SYSTEM.ToString(),
               OwnerId = adminId
           }
           );
            #endregion
            #region Song_PlayList
           // modelBuilder.Entity<Song_PlayList>().HasData(
           //     new Song_PlayList() { SongId = 1, PlayListId = 1 },
           //     new Song_PlayList() { SongId = 2, PlayListId = 2 }
           //     );
           // modelBuilder.Entity<SongType>().HasData(
           //new SongType()
           //{
           //    Id = 1,
           //    Name = "Nhac Acoustic",
           //    Description = "Nhac Acoustic hay cuc"
           //},
           //new SongType()
           //{
           //    Id = 2,
           //    Name = "Nhac Rock",
           //    Description = "Nhac Rock hay cuc"
           //}
           //);
            #endregion
            #region Song_SongType
            //modelBuilder.Entity<Song_SongType>().HasData(
            //    new Song_SongType() { SongId = 1, SongTypeId = 1 },
            //    new Song_SongType() { SongId = 1, SongTypeId = 2 },
            //    new Song_SongType() { SongId = 2, SongTypeId = 2 }
            //    );
            #endregion
            
            #region Friend
            modelBuilder.Entity<Friend>().HasData(
                new Friend() { Id = 1, SenderId = adminId, ReceiverId = userId, Status = 0, DateSendRequest = DateTime.Now },
                new Friend() { Id = 2, SenderId = adminId, ReceiverId = singerId, Status = 1, DateSendRequest = DateTime.Now },
                new Friend() { Id = 3, SenderId = adminId, ReceiverId = authorId, Status = -1, DateSendRequest = DateTime.Now },
                new Friend() { Id = 4, SenderId = userId, ReceiverId = singerId, Status = 1, DateSendRequest = DateTime.Now },
                new Friend() { Id = 5, SenderId = userId, ReceiverId = authorId, Status = 0, DateSendRequest = DateTime.Now }
                );
            #endregion
            #region Song_Owner
            //modelBuilder.Entity<Song_Owner>().HasData(new Song_Owner() { OwnerId = adminId, SongId = 1 });
            #endregion
            #region History
            //modelBuilder.Entity<History>().HasData(
            //    new History()
            //    {
            //        Id = 1,
            //        UserId = adminId,
            //        ActionType = Models.Enum.ActionType.SEARCH.ToString(),
            //        DateTime = DateTime.Now,
            //        ObjectId = "1",
            //        ObjectType = Models.Enum.ObjectType.SONG.ToString()
            //    },
            //    new History()
            //    {
            //        Id = 2,
            //        UserId = userId,
            //        ActionType = Models.Enum.ActionType.SEARCH.ToString(),
            //        DateTime = DateTime.Now,
            //        ObjectId = "2",
            //        ObjectType = Models.Enum.ObjectType.PLAYLIST.ToString()
            //    }
            //    );
            #endregion
            #region Song Tag
            modelBuilder.Entity<Tag>().HasData(
                new Tag() { Id = 1, Name = "Nhac Tre" },
                new Tag() { Id = 2, Name = "Nhac 2020" },
                new Tag() { Id = 3, Name = "Nhac Remix" }
                );
            //modelBuilder.Entity<Song_Tag>().HasData(
            //    new Song_Tag() { SongId = 1, TagId = 1 },
            //    new Song_Tag() { SongId = 1, TagId = 2 },
            //    new Song_Tag() { SongId = 1, TagId = 3 },
            //    new Song_Tag() { SongId = 2, TagId = 1 },
            //    new Song_Tag() { SongId = 2, TagId = 3 }
            //    );
            #endregion
        }


    }
}
