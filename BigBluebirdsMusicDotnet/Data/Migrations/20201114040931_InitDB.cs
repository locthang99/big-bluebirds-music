﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class InitDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AppConfigs",
                columns: table => new
                {
                    Key = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppConfigs", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "AppRoles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    NormalizedName = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Description = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Histories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<Guid>(nullable: false),
                    DateTime = table.Column<DateTime>(nullable: false),
                    ActionType = table.Column<string>(nullable: false),
                    ObjectType = table.Column<string>(nullable: false),
                    ObjectId = table.Column<string>(nullable: true),
                    ObjectName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Histories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PlayList",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Thumbnail = table.Column<string>(nullable: false, defaultValue: ""),
                    DateCreate = table.Column<DateTime>(nullable: false, defaultValue: new DateTime(2020, 11, 14, 11, 9, 30, 644, DateTimeKind.Local).AddTicks(6204)),
                    TotalSong = table.Column<int>(nullable: false, defaultValue: 0),
                    TotalListen = table.Column<int>(nullable: false, defaultValue: 0),
                    TotalLike = table.Column<int>(nullable: false, defaultValue: 0),
                    TotalCmt = table.Column<int>(nullable: false, defaultValue: 0),
                    PlaylistType = table.Column<string>(nullable: false),
                    OwnerId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayList", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Songs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    DateCreate = table.Column<DateTime>(nullable: false, defaultValue: new DateTime(2020, 11, 14, 11, 9, 30, 640, DateTimeKind.Local).AddTicks(3451)),
                    TotalListen = table.Column<int>(nullable: false, defaultValue: 0),
                    TotalLike = table.Column<int>(nullable: false, defaultValue: 0),
                    TotalCmt = table.Column<int>(nullable: false, defaultValue: 0),
                    TotalDownload = table.Column<int>(nullable: false, defaultValue: 0),
                    Lyric = table.Column<string>(nullable: true),
                    Duration = table.Column<int>(nullable: false, defaultValue: 0),
                    Thumbnail = table.Column<string>(nullable: false, defaultValue: ""),
                    FileMusic = table.Column<string>(nullable: false, defaultValue: "")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Songs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SongTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SongTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tag",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tag", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<Guid>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserClaims", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserLogins",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: true),
                    ProviderKey = table.Column<string>(nullable: true),
                    ProviderDisplayName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLogins", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "UserRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<Guid>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoleClaims", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    RoleId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => new { x.UserId, x.RoleId });
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserName = table.Column<string>(nullable: true),
                    NormalizedUserName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    NormalizedEmail = table.Column<string>(nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(maxLength: 200, nullable: false),
                    LastName = table.Column<string>(maxLength: 200, nullable: false),
                    Dob = table.Column<DateTime>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    AccountType = table.Column<string>(nullable: false),
                    Thumbnail = table.Column<string>(nullable: false, defaultValue: "")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserTokens",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTokens", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "FileMusics",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IdSong = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Type = table.Column<string>(nullable: false),
                    Path = table.Column<string>(nullable: false),
                    FileSize = table.Column<long>(nullable: false),
                    SongId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FileMusics", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FileMusics_Songs_SongId",
                        column: x => x.SongId,
                        principalTable: "Songs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Song_PlayLists",
                columns: table => new
                {
                    SongId = table.Column<int>(nullable: false),
                    PlayListId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Song_PlayLists", x => new { x.SongId, x.PlayListId });
                    table.ForeignKey(
                        name: "FK_Song_PlayLists_PlayList_PlayListId",
                        column: x => x.PlayListId,
                        principalTable: "PlayList",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Song_PlayLists_Songs_SongId",
                        column: x => x.SongId,
                        principalTable: "Songs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Song_SongTypes",
                columns: table => new
                {
                    SongId = table.Column<int>(nullable: false),
                    SongTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Song_SongTypes", x => new { x.SongId, x.SongTypeId });
                    table.ForeignKey(
                        name: "FK_Song_SongTypes_Songs_SongId",
                        column: x => x.SongId,
                        principalTable: "Songs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Song_SongTypes_SongTypes_SongTypeId",
                        column: x => x.SongTypeId,
                        principalTable: "SongTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Song_Tags",
                columns: table => new
                {
                    SongId = table.Column<int>(nullable: false),
                    TagId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Song_Tags", x => new { x.SongId, x.TagId });
                    table.ForeignKey(
                        name: "FK_Song_Tags_Songs_SongId",
                        column: x => x.SongId,
                        principalTable: "Songs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Song_Tags_Tag_TagId",
                        column: x => x.TagId,
                        principalTable: "Tag",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FileImages",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IdSong = table.Column<int>(nullable: true),
                    IdPlayList = table.Column<int>(nullable: true),
                    IdUser = table.Column<Guid>(nullable: true),
                    Description = table.Column<string>(nullable: false),
                    Type = table.Column<string>(nullable: false),
                    Path = table.Column<string>(nullable: false),
                    FileSize = table.Column<long>(nullable: false),
                    SongId = table.Column<int>(nullable: true),
                    PlayListId = table.Column<int>(nullable: true),
                    UserId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FileImages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FileImages_PlayList_PlayListId",
                        column: x => x.PlayListId,
                        principalTable: "PlayList",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FileImages_Songs_SongId",
                        column: x => x.SongId,
                        principalTable: "Songs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FileImages_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Friends",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SenderId = table.Column<Guid>(nullable: false),
                    ReceiverId = table.Column<Guid>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    DateSendRequest = table.Column<DateTime>(nullable: false, defaultValue: new DateTime(2020, 11, 14, 11, 9, 30, 659, DateTimeKind.Local).AddTicks(757)),
                    DateUpdateRequest = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Friends", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Friends_Users_ReceiverId",
                        column: x => x.ReceiverId,
                        principalTable: "Users",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Friends_Users_SenderId",
                        column: x => x.SenderId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Song_Owners",
                columns: table => new
                {
                    SongId = table.Column<int>(nullable: false),
                    OwnerId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Song_Owners", x => new { x.SongId, x.OwnerId });
                    table.ForeignKey(
                        name: "FK_Song_Owners_Users_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Song_Owners_Songs_SongId",
                        column: x => x.SongId,
                        principalTable: "Songs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "User_Cmt_Playlists",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<Guid>(nullable: false),
                    PlaylistId = table.Column<int>(nullable: false),
                    DateCreate = table.Column<DateTime>(nullable: false),
                    Content = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_Cmt_Playlists", x => x.Id);
                    table.ForeignKey(
                        name: "FK_User_Cmt_Playlists_PlayList_PlaylistId",
                        column: x => x.PlaylistId,
                        principalTable: "PlayList",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_User_Cmt_Playlists_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "User_Cmt_Song",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<Guid>(nullable: false),
                    SongId = table.Column<int>(nullable: false),
                    DateCreate = table.Column<DateTime>(nullable: false, defaultValue: new DateTime(2020, 11, 14, 11, 9, 30, 656, DateTimeKind.Local).AddTicks(9494)),
                    Content = table.Column<string>(maxLength: 1000, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_Cmt_Song", x => x.Id);
                    table.ForeignKey(
                        name: "FK_User_Cmt_Song_Songs_SongId",
                        column: x => x.SongId,
                        principalTable: "Songs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_User_Cmt_Song_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "User_Like_Playlists",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<Guid>(nullable: false),
                    PlaylistId = table.Column<int>(nullable: false),
                    DateCreate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_Like_Playlists", x => x.Id);
                    table.ForeignKey(
                        name: "FK_User_Like_Playlists_PlayList_PlaylistId",
                        column: x => x.PlaylistId,
                        principalTable: "PlayList",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_User_Like_Playlists_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "User_Like_Song",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    SongId = table.Column<int>(nullable: false),
                    DateCreate = table.Column<DateTime>(nullable: false, defaultValue: new DateTime(2020, 11, 14, 11, 9, 30, 654, DateTimeKind.Local).AddTicks(7234))
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_Like_Song", x => new { x.UserId, x.SongId });
                    table.ForeignKey(
                        name: "FK_User_Like_Song_Songs_SongId",
                        column: x => x.SongId,
                        principalTable: "Songs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_User_Like_Song_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AppConfigs",
                columns: new[] { "Key", "Value" },
                values: new object[,]
                {
                    { "HomeTitle", "This is home page of BigBluebirds" },
                    { "HomeKeyword", "This is keyword of BigBluebirds" },
                    { "HomeDescription", "This is description of BigBluebirds" }
                });

            migrationBuilder.InsertData(
                table: "AppRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("00000000-0000-0000-0000-000000000001"), "14143dea-4db0-4cfd-886b-c2d8fc694e96", "Administrator role", "admin", "admin" },
                    { new Guid("00000000-0000-0000-0000-000000000002"), "09319f1f-2b1c-45d4-8b9d-53a2d33c28e4", "User role", "user", "user" },
                    { new Guid("00000000-0000-0000-0000-000000000003"), "481a2113-72d5-4012-a979-85d561e64aa1", "Singer role", "singer", "singer" },
                    { new Guid("00000000-0000-0000-0000-000000000004"), "4904ee71-460d-4cd1-8e14-fee935ec5f2d", "Author role", "author", "author" }
                });

            migrationBuilder.InsertData(
                table: "PlayList",
                columns: new[] { "Id", "DateCreate", "Description", "Name", "OwnerId", "PlaylistType" },
                values: new object[,]
                {
                    { 14, new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(5389), "Danh sách EDM Việt", "EDM Việt ", new Guid("00000000-0000-0000-0000-000000000001"), "SYSTEM" },
                    { 13, new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(5387), "Danh sách Cải Lương", "Cải Lương", new Guid("00000000-0000-0000-0000-000000000001"), "SYSTEM" },
                    { 12, new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(5384), "Danh sách nhạc Phim", "Nhạc him", new Guid("00000000-0000-0000-0000-000000000001"), "SYSTEM" },
                    { 11, new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(5382), "Danh sách nhạc Thiếu Nhi", "Nhạc Thiếu Nhi", new Guid("00000000-0000-0000-0000-000000000001"), "SYSTEM" },
                    { 10, new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(5379), "Danh sách nhac Dân ca", "Nhạc Dân Ca", new Guid("00000000-0000-0000-0000-000000000001"), "SYSTEM" },
                    { 8, new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(5370), "Danh sách Rap Việt", "Rap Việt ", new Guid("00000000-0000-0000-0000-000000000001"), "SYSTEM" },
                    { 9, new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(5374), "Danh sách nhạc Trữ tình", "Nhạc Trữ Tình", new Guid("00000000-0000-0000-0000-000000000001"), "SYSTEM" },
                    { 6, new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(5365), "Danh sách Dance Viet", "Dance Viet ", new Guid("00000000-0000-0000-0000-000000000001"), "SYSTEM" },
                    { 5, new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(5362), "Danh sách nhạc Không lời", "Nhạc Không Lời", new Guid("00000000-0000-0000-0000-000000000001"), "SYSTEM" },
                    { 4, new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(5359), "Danh sách nhạc Trịnh", "Nhạc Trịnh", new Guid("00000000-0000-0000-0000-000000000001"), "SYSTEM" },
                    { 3, new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(5355), "Danh sách nhạc Cách Mạng", "Nhạc Cách Mạng", new Guid("00000000-0000-0000-0000-000000000001"), "SYSTEM" },
                    { 2, new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(5270), "Danh sách Rock Việt", "Rock Việt", new Guid("00000000-0000-0000-0000-000000000001"), "SYSTEM" },
                    { 1, new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(1122), "Danh sách nhạc trẻ", "Nhạc Trẻ", new Guid("00000000-0000-0000-0000-000000000001"), "SYSTEM" },
                    { 7, new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(5367), "Danh sách V-Pop", "V-Pop ", new Guid("00000000-0000-0000-0000-000000000001"), "SYSTEM" }
                });

            migrationBuilder.InsertData(
                table: "Tag",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 3, null, "Nhac Remix" },
                    { 2, null, "Nhac 2020" },
                    { 1, null, "Nhac Tre" }
                });

            migrationBuilder.InsertData(
                table: "UserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { new Guid("00000000-0000-0000-0000-000000000003"), new Guid("00000000-0000-0000-0000-000000000003") },
                    { new Guid("00000000-0000-0000-0000-000000000001"), new Guid("00000000-0000-0000-0000-000000000001") },
                    { new Guid("00000000-0000-0000-0000-000000000002"), new Guid("00000000-0000-0000-0000-000000000002") },
                    { new Guid("00000000-0000-0000-0000-000000000004"), new Guid("00000000-0000-0000-0000-000000000004") }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AccessFailedCount", "AccountType", "Address", "ConcurrencyStamp", "Dob", "Email", "EmailConfirmed", "FirstName", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { new Guid("00000000-0000-0000-0000-000000000001"), 0, "SYSTEM", null, "1cf4ca83-68f1-4ca5-bcb9-bcc7e2be2c4a", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "admin@gmail.com", true, "Admin", "BigBluebirds", false, null, "admin@gmail.com", "admin", "AQAAAAEAACcQAAAAEOrPONdrlI4o9ahgi/TtsNbsU6Wq3hZcHUBeda+3XFGpjM6Bj4YcBor8zHTAG6foxg==", null, false, "", false, "admin" },
                    { new Guid("00000000-0000-0000-0000-000000000002"), 0, "SYSTEM", null, "37d55bb7-ba08-4d8a-bbdd-91235789ab57", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "user@gmail.com", true, "User", "User", false, null, "user@gmail.com", "user", "AQAAAAEAACcQAAAAELYNiyYvdstVl2iIq5PJ2XHHs6LOYjyF4+/p0gS7l0fXMrZqbkDXfiP1utR/Xhwmzw==", null, false, "", false, "user" },
                    { new Guid("00000000-0000-0000-0000-000000000003"), 0, "SYSTEM", null, "a86926e8-ad5d-42d6-a158-ebad6aa51fc1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "singer@gmail.com", true, "Singer", "Singer", false, null, "singer@gmail.com", "singer", "AQAAAAEAACcQAAAAEDp21P03Q4qLGzWbQTGtu4KAKrpPf3Avvmp75N+3Rcy9rkstRvdgpZ06Apq+2xV/2g==", null, false, "", false, "singer" },
                    { new Guid("00000000-0000-0000-0000-000000000004"), 0, "SYSTEM", null, "dd6ff22e-1942-436b-bb55-ca62a8d5b4f7", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "author@gmail.com", true, "Author", "Author", false, null, "author@gmail.com", "author", "AQAAAAEAACcQAAAAECVKWLaf/aYYzeyuOmnqijx1t4AFvKyd0lUaFY8IhbtFmP1peQfxRQB0cRHWgXsILw==", null, false, "", false, "author" }
                });

            migrationBuilder.InsertData(
                table: "Friends",
                columns: new[] { "Id", "DateSendRequest", "DateUpdateRequest", "ReceiverId", "SenderId", "Status" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(8071), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("00000000-0000-0000-0000-000000000002"), new Guid("00000000-0000-0000-0000-000000000001"), 0 },
                    { 2, new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(8623), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("00000000-0000-0000-0000-000000000003"), new Guid("00000000-0000-0000-0000-000000000001"), 1 },
                    { 4, new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(8639), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("00000000-0000-0000-0000-000000000003"), new Guid("00000000-0000-0000-0000-000000000002"), 1 },
                    { 3, new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(8636), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("00000000-0000-0000-0000-000000000004"), new Guid("00000000-0000-0000-0000-000000000001"), -1 },
                    { 5, new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(8640), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("00000000-0000-0000-0000-000000000004"), new Guid("00000000-0000-0000-0000-000000000002"), 0 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_FileImages_PlayListId",
                table: "FileImages",
                column: "PlayListId");

            migrationBuilder.CreateIndex(
                name: "IX_FileImages_SongId",
                table: "FileImages",
                column: "SongId");

            migrationBuilder.CreateIndex(
                name: "IX_FileImages_UserId",
                table: "FileImages",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_FileMusics_SongId",
                table: "FileMusics",
                column: "SongId");

            migrationBuilder.CreateIndex(
                name: "IX_Friends_ReceiverId",
                table: "Friends",
                column: "ReceiverId");

            migrationBuilder.CreateIndex(
                name: "IX_Friends_SenderId",
                table: "Friends",
                column: "SenderId");

            migrationBuilder.CreateIndex(
                name: "IX_Song_Owners_OwnerId",
                table: "Song_Owners",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Song_PlayLists_PlayListId",
                table: "Song_PlayLists",
                column: "PlayListId");

            migrationBuilder.CreateIndex(
                name: "IX_Song_SongTypes_SongTypeId",
                table: "Song_SongTypes",
                column: "SongTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Song_Tags_TagId",
                table: "Song_Tags",
                column: "TagId");

            migrationBuilder.CreateIndex(
                name: "IX_User_Cmt_Playlists_PlaylistId",
                table: "User_Cmt_Playlists",
                column: "PlaylistId");

            migrationBuilder.CreateIndex(
                name: "IX_User_Cmt_Playlists_UserId",
                table: "User_Cmt_Playlists",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_User_Cmt_Song_SongId",
                table: "User_Cmt_Song",
                column: "SongId");

            migrationBuilder.CreateIndex(
                name: "IX_User_Cmt_Song_UserId",
                table: "User_Cmt_Song",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_User_Like_Playlists_PlaylistId",
                table: "User_Like_Playlists",
                column: "PlaylistId");

            migrationBuilder.CreateIndex(
                name: "IX_User_Like_Playlists_UserId",
                table: "User_Like_Playlists",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_User_Like_Song_SongId",
                table: "User_Like_Song",
                column: "SongId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AppConfigs");

            migrationBuilder.DropTable(
                name: "AppRoles");

            migrationBuilder.DropTable(
                name: "FileImages");

            migrationBuilder.DropTable(
                name: "FileMusics");

            migrationBuilder.DropTable(
                name: "Friends");

            migrationBuilder.DropTable(
                name: "Histories");

            migrationBuilder.DropTable(
                name: "Song_Owners");

            migrationBuilder.DropTable(
                name: "Song_PlayLists");

            migrationBuilder.DropTable(
                name: "Song_SongTypes");

            migrationBuilder.DropTable(
                name: "Song_Tags");

            migrationBuilder.DropTable(
                name: "User_Cmt_Playlists");

            migrationBuilder.DropTable(
                name: "User_Cmt_Song");

            migrationBuilder.DropTable(
                name: "User_Like_Playlists");

            migrationBuilder.DropTable(
                name: "User_Like_Song");

            migrationBuilder.DropTable(
                name: "UserClaims");

            migrationBuilder.DropTable(
                name: "UserLogins");

            migrationBuilder.DropTable(
                name: "UserRoleClaims");

            migrationBuilder.DropTable(
                name: "UserRoles");

            migrationBuilder.DropTable(
                name: "UserTokens");

            migrationBuilder.DropTable(
                name: "SongTypes");

            migrationBuilder.DropTable(
                name: "Tag");

            migrationBuilder.DropTable(
                name: "PlayList");

            migrationBuilder.DropTable(
                name: "Songs");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
