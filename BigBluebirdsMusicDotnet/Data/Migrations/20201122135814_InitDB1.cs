﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class InitDB1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Song_Tags_Tag_TagId",
                table: "Song_Tags");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Tag",
                table: "Tag");

            migrationBuilder.RenameTable(
                name: "Tag",
                newName: "Tags");

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "User_Like_Song",
                nullable: false,
                defaultValue: new DateTime(2020, 11, 22, 20, 58, 13, 219, DateTimeKind.Local).AddTicks(2335),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2020, 11, 14, 11, 9, 30, 654, DateTimeKind.Local).AddTicks(7234));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "User_Cmt_Song",
                nullable: false,
                defaultValue: new DateTime(2020, 11, 22, 20, 58, 13, 221, DateTimeKind.Local).AddTicks(7662),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2020, 11, 14, 11, 9, 30, 656, DateTimeKind.Local).AddTicks(9494));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "Songs",
                nullable: false,
                defaultValue: new DateTime(2020, 11, 22, 20, 58, 13, 204, DateTimeKind.Local).AddTicks(6409),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2020, 11, 14, 11, 9, 30, 640, DateTimeKind.Local).AddTicks(3451));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "PlayList",
                nullable: false,
                defaultValue: new DateTime(2020, 11, 22, 20, 58, 13, 208, DateTimeKind.Local).AddTicks(9446),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2020, 11, 14, 11, 9, 30, 644, DateTimeKind.Local).AddTicks(6204));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateSendRequest",
                table: "Friends",
                nullable: false,
                defaultValue: new DateTime(2020, 11, 22, 20, 58, 13, 223, DateTimeKind.Local).AddTicks(9337),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2020, 11, 14, 11, 9, 30, 659, DateTimeKind.Local).AddTicks(757));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Tags",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Tags",
                table: "Tags",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000001"),
                column: "ConcurrencyStamp",
                value: "3beea34f-1cac-4f0d-8919-539ff684ea9f");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000002"),
                column: "ConcurrencyStamp",
                value: "ebe2d3e0-d8d8-45d6-bbf6-517c732f5a18");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000003"),
                column: "ConcurrencyStamp",
                value: "bc515002-0d02-44d7-9ece-7f71f6b59f7d");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000004"),
                column: "ConcurrencyStamp",
                value: "31f47ed9-4d5a-44b1-aa8f-da40799a8cdc");

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateSendRequest",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(5620));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateSendRequest",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(6182));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateSendRequest",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(6194));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateSendRequest",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(6196));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateSendRequest",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(6197));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 285, DateTimeKind.Local).AddTicks(8699));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(2807));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(2892));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(2896));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(2899));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(2903));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(2906));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(2909));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(2911));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(2915));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(2918));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(2920));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(2923));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(2926));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000001"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "6b9b2d3f-706d-4e33-988c-6da1000fc7c2", "AQAAAAEAACcQAAAAEModV2bCk1tw1t1TPzgs4ec6LdE4MlG1YV6M6hG4TIjfjL8r9vmSHQQ8Tr+4HQ5Oag==" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000002"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "3f9f53a5-d775-4daf-b596-bb1f77fdbc93", "AQAAAAEAACcQAAAAEHaPcEnUf4q+xcRSFnEOHnXvSkA5vnaC/PlY0uiMmFxnJc3eKUhnn2cr/t936DRmuQ==" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000003"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "7e3ac422-f7f4-41d9-a47d-ad4091123434", "AQAAAAEAACcQAAAAEKr8TXclEhC6mzrRGQ5weNrazT4MduIiXNXdjFQfNjciUblDU2KlYXScAdMaWuq3kg==" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000004"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "b122f61d-0d2c-4c20-8e2c-601af66555bb", "AQAAAAEAACcQAAAAEF6Je8EmBgWSba7VCJIMcYu88zQ4oo3uymmbcd224y6YZxdfNDwV/ixPcKn2GLs1Xg==" });

            migrationBuilder.AddForeignKey(
                name: "FK_Song_Tags_Tags_TagId",
                table: "Song_Tags",
                column: "TagId",
                principalTable: "Tags",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Song_Tags_Tags_TagId",
                table: "Song_Tags");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Tags",
                table: "Tags");

            migrationBuilder.RenameTable(
                name: "Tags",
                newName: "Tag");

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "User_Like_Song",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2020, 11, 14, 11, 9, 30, 654, DateTimeKind.Local).AddTicks(7234),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2020, 11, 22, 20, 58, 13, 219, DateTimeKind.Local).AddTicks(2335));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "User_Cmt_Song",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2020, 11, 14, 11, 9, 30, 656, DateTimeKind.Local).AddTicks(9494),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2020, 11, 22, 20, 58, 13, 221, DateTimeKind.Local).AddTicks(7662));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "Songs",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2020, 11, 14, 11, 9, 30, 640, DateTimeKind.Local).AddTicks(3451),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2020, 11, 22, 20, 58, 13, 204, DateTimeKind.Local).AddTicks(6409));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "PlayList",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2020, 11, 14, 11, 9, 30, 644, DateTimeKind.Local).AddTicks(6204),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2020, 11, 22, 20, 58, 13, 208, DateTimeKind.Local).AddTicks(9446));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateSendRequest",
                table: "Friends",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2020, 11, 14, 11, 9, 30, 659, DateTimeKind.Local).AddTicks(757),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2020, 11, 22, 20, 58, 13, 223, DateTimeKind.Local).AddTicks(9337));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Tag",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddPrimaryKey(
                name: "PK_Tag",
                table: "Tag",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000001"),
                column: "ConcurrencyStamp",
                value: "14143dea-4db0-4cfd-886b-c2d8fc694e96");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000002"),
                column: "ConcurrencyStamp",
                value: "09319f1f-2b1c-45d4-8b9d-53a2d33c28e4");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000003"),
                column: "ConcurrencyStamp",
                value: "481a2113-72d5-4012-a979-85d561e64aa1");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000004"),
                column: "ConcurrencyStamp",
                value: "4904ee71-460d-4cd1-8e14-fee935ec5f2d");

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateSendRequest",
                value: new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(8071));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateSendRequest",
                value: new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(8623));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateSendRequest",
                value: new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(8636));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateSendRequest",
                value: new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(8639));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateSendRequest",
                value: new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(8640));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreate",
                value: new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(1122));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreate",
                value: new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(5270));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreate",
                value: new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(5355));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreate",
                value: new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(5359));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreate",
                value: new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(5362));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreate",
                value: new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(5365));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreate",
                value: new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(5367));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreate",
                value: new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(5370));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreate",
                value: new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(5374));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreate",
                value: new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(5379));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreate",
                value: new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(5382));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreate",
                value: new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(5384));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreate",
                value: new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(5387));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreate",
                value: new DateTime(2020, 11, 14, 11, 9, 30, 722, DateTimeKind.Local).AddTicks(5389));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000001"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "1cf4ca83-68f1-4ca5-bcb9-bcc7e2be2c4a", "AQAAAAEAACcQAAAAEOrPONdrlI4o9ahgi/TtsNbsU6Wq3hZcHUBeda+3XFGpjM6Bj4YcBor8zHTAG6foxg==" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000002"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "37d55bb7-ba08-4d8a-bbdd-91235789ab57", "AQAAAAEAACcQAAAAELYNiyYvdstVl2iIq5PJ2XHHs6LOYjyF4+/p0gS7l0fXMrZqbkDXfiP1utR/Xhwmzw==" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000003"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "a86926e8-ad5d-42d6-a158-ebad6aa51fc1", "AQAAAAEAACcQAAAAEDp21P03Q4qLGzWbQTGtu4KAKrpPf3Avvmp75N+3Rcy9rkstRvdgpZ06Apq+2xV/2g==" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000004"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "dd6ff22e-1942-436b-bb55-ca62a8d5b4f7", "AQAAAAEAACcQAAAAECVKWLaf/aYYzeyuOmnqijx1t4AFvKyd0lUaFY8IhbtFmP1peQfxRQB0cRHWgXsILw==" });

            migrationBuilder.AddForeignKey(
                name: "FK_Song_Tags_Tag_TagId",
                table: "Song_Tags",
                column: "TagId",
                principalTable: "Tag",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
