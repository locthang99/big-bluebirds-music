﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class InitDB5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "Users",
                nullable: false,
                defaultValue: new DateTime(2021, 1, 3, 15, 30, 29, 245, DateTimeKind.Local).AddTicks(4409),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2020, 12, 27, 9, 58, 32, 802, DateTimeKind.Local).AddTicks(3569));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "User_Like_Song",
                nullable: false,
                defaultValue: new DateTime(2021, 1, 3, 15, 30, 29, 283, DateTimeKind.Local).AddTicks(1616),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2020, 12, 27, 9, 58, 32, 838, DateTimeKind.Local).AddTicks(4118));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "User_Cmt_Song",
                nullable: false,
                defaultValue: new DateTime(2021, 1, 3, 15, 30, 29, 285, DateTimeKind.Local).AddTicks(9177),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2020, 12, 27, 9, 58, 32, 840, DateTimeKind.Local).AddTicks(8372));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "Songs",
                nullable: false,
                defaultValue: new DateTime(2021, 1, 3, 15, 30, 29, 267, DateTimeKind.Local).AddTicks(371),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2020, 12, 27, 9, 58, 32, 824, DateTimeKind.Local).AddTicks(5654));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "PlayList",
                nullable: false,
                defaultValue: new DateTime(2021, 1, 3, 15, 30, 29, 272, DateTimeKind.Local).AddTicks(823),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2020, 12, 27, 9, 58, 32, 828, DateTimeKind.Local).AddTicks(1790));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateSendRequest",
                table: "Friends",
                nullable: false,
                defaultValue: new DateTime(2021, 1, 3, 15, 30, 29, 288, DateTimeKind.Local).AddTicks(3546),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2020, 12, 27, 9, 58, 32, 842, DateTimeKind.Local).AddTicks(9877));

            migrationBuilder.CreateTable(
                name: "CacheDatas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OwnerId = table.Column<string>(nullable: false),
                    Data = table.Column<string>(nullable: false),
                    Type = table.Column<string>(nullable: false),
                    TimeExpire = table.Column<int>(nullable: false, defaultValue: 1),
                    DateCreate = table.Column<DateTime>(nullable: false, defaultValue: new DateTime(2021, 1, 3, 15, 30, 29, 302, DateTimeKind.Local).AddTicks(2413))
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CacheDatas", x => x.Id);
                });

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000001"),
                column: "ConcurrencyStamp",
                value: "15a80259-309c-410f-857b-ee6d62c89697");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000002"),
                column: "ConcurrencyStamp",
                value: "df7b2269-339d-460e-9336-37dda6dfc2d2");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000003"),
                column: "ConcurrencyStamp",
                value: "bdf39535-09f5-4027-9c4d-e1dbcdb3ea34");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000004"),
                column: "ConcurrencyStamp",
                value: "a9471afb-4297-4dc6-9e40-55bc3b6e08d8");

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateSendRequest",
                value: new DateTime(2021, 1, 3, 15, 30, 29, 363, DateTimeKind.Local).AddTicks(415));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateSendRequest",
                value: new DateTime(2021, 1, 3, 15, 30, 29, 363, DateTimeKind.Local).AddTicks(1047));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateSendRequest",
                value: new DateTime(2021, 1, 3, 15, 30, 29, 363, DateTimeKind.Local).AddTicks(1062));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateSendRequest",
                value: new DateTime(2021, 1, 3, 15, 30, 29, 363, DateTimeKind.Local).AddTicks(1064));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateSendRequest",
                value: new DateTime(2021, 1, 3, 15, 30, 29, 363, DateTimeKind.Local).AddTicks(1065));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreate",
                value: new DateTime(2021, 1, 3, 15, 30, 29, 361, DateTimeKind.Local).AddTicks(6020));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreate",
                value: new DateTime(2021, 1, 3, 15, 30, 29, 362, DateTimeKind.Local).AddTicks(4978));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreate",
                value: new DateTime(2021, 1, 3, 15, 30, 29, 362, DateTimeKind.Local).AddTicks(5186));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreate",
                value: new DateTime(2021, 1, 3, 15, 30, 29, 362, DateTimeKind.Local).AddTicks(5199));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreate",
                value: new DateTime(2021, 1, 3, 15, 30, 29, 362, DateTimeKind.Local).AddTicks(5206));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreate",
                value: new DateTime(2021, 1, 3, 15, 30, 29, 362, DateTimeKind.Local).AddTicks(5210));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreate",
                value: new DateTime(2021, 1, 3, 15, 30, 29, 362, DateTimeKind.Local).AddTicks(5219));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreate",
                value: new DateTime(2021, 1, 3, 15, 30, 29, 362, DateTimeKind.Local).AddTicks(5228));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreate",
                value: new DateTime(2021, 1, 3, 15, 30, 29, 362, DateTimeKind.Local).AddTicks(5236));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreate",
                value: new DateTime(2021, 1, 3, 15, 30, 29, 362, DateTimeKind.Local).AddTicks(5244));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreate",
                value: new DateTime(2021, 1, 3, 15, 30, 29, 362, DateTimeKind.Local).AddTicks(5396));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreate",
                value: new DateTime(2021, 1, 3, 15, 30, 29, 362, DateTimeKind.Local).AddTicks(5407));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreate",
                value: new DateTime(2021, 1, 3, 15, 30, 29, 362, DateTimeKind.Local).AddTicks(5415));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreate",
                value: new DateTime(2021, 1, 3, 15, 30, 29, 362, DateTimeKind.Local).AddTicks(5424));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000001"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "581d908b-ba02-43a4-979c-4a1e0669caa6", "AQAAAAEAACcQAAAAEKoM1DJh6OpaKCUJIEKVQvd6UJqcKGbrY9NdgZmvxZp84cED03fVhdINBq0kyTyvdQ==", "" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000002"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "8d5dda19-7618-40e3-88d0-365e75132882", "AQAAAAEAACcQAAAAEBL2/iXrsHrafv4vhu2s0jZgM0ffFri5VoUntHLA7fk2EUG5R0LTV3X2LmVr4Ns8SQ==", "" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000003"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "cd77211c-cd35-4c4f-9827-c720673b2e19", "AQAAAAEAACcQAAAAECLrU/jq9FgS46ztwbz0/01HvuTkEvsjwNr//Qj5B1nVTp7Nk+R1UQQhfFx95lSv0g==", "" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000004"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "5b3beb7d-76a9-4d2a-be3b-726fb434c646", "AQAAAAEAACcQAAAAEJeDZRnRyKbaeCG+3iJfuh0STChNTYcU1fQQQ2RA/ZnjZrYmo8z7HQ527zez4dEXkA==", "" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CacheDatas");

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "Users",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2020, 12, 27, 9, 58, 32, 802, DateTimeKind.Local).AddTicks(3569),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2021, 1, 3, 15, 30, 29, 245, DateTimeKind.Local).AddTicks(4409));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "User_Like_Song",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2020, 12, 27, 9, 58, 32, 838, DateTimeKind.Local).AddTicks(4118),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2021, 1, 3, 15, 30, 29, 283, DateTimeKind.Local).AddTicks(1616));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "User_Cmt_Song",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2020, 12, 27, 9, 58, 32, 840, DateTimeKind.Local).AddTicks(8372),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2021, 1, 3, 15, 30, 29, 285, DateTimeKind.Local).AddTicks(9177));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "Songs",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2020, 12, 27, 9, 58, 32, 824, DateTimeKind.Local).AddTicks(5654),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2021, 1, 3, 15, 30, 29, 267, DateTimeKind.Local).AddTicks(371));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "PlayList",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2020, 12, 27, 9, 58, 32, 828, DateTimeKind.Local).AddTicks(1790),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2021, 1, 3, 15, 30, 29, 272, DateTimeKind.Local).AddTicks(823));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateSendRequest",
                table: "Friends",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2020, 12, 27, 9, 58, 32, 842, DateTimeKind.Local).AddTicks(9877),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2021, 1, 3, 15, 30, 29, 288, DateTimeKind.Local).AddTicks(3546));

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000001"),
                column: "ConcurrencyStamp",
                value: "dbc5cdf4-396c-402c-b9fc-3850f92b16f7");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000002"),
                column: "ConcurrencyStamp",
                value: "a9efa616-8d51-4624-9348-5277295fcac1");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000003"),
                column: "ConcurrencyStamp",
                value: "0c7eb8ac-826f-46a8-8180-465594f8e74f");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000004"),
                column: "ConcurrencyStamp",
                value: "2d32fb20-65a2-49f1-9690-0ec6a0387691");

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateSendRequest",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(9969));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateSendRequest",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 914, DateTimeKind.Local).AddTicks(824));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateSendRequest",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 914, DateTimeKind.Local).AddTicks(840));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateSendRequest",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 914, DateTimeKind.Local).AddTicks(842));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateSendRequest",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 914, DateTimeKind.Local).AddTicks(843));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(2414));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(6931));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(7026));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(7030));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(7033));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(7036));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(7038));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(7041));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(7043));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(7046));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(7048));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(7051));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(7053));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(7056));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000001"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "7eaaa837-b7be-497c-9ab0-2bf6307747cf", "AQAAAAEAACcQAAAAEL0hHuUVvY5o1UgtV2Kxo8H8X3pCZO5rxichILJyMiNDqJnyd67AG169Ys26AjyFpA==", "" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000002"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "15b79dc4-ad3f-446b-8fa5-3a9bd4593aa6", "AQAAAAEAACcQAAAAEOsFDuDT3Nic+hmtMbmeqSitZfx4qHOewPj3vKFJLDunrxVrI1lJXM+M61+EDdFffg==", "" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000003"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "84fcf361-0764-4bf2-bbb6-3323bc1d97b1", "AQAAAAEAACcQAAAAEDawYAbv2ph7FW59OB7DVd3MHzph1ruxK5PBd3QPsa3tsCHN4/bnFGFPL2XYghVMYw==", "" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000004"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "cb7dfb41-6ab1-4104-be1c-f65ea9ea012b", "AQAAAAEAACcQAAAAEOS5MSAvKCIe3jmorWsGcLKwCrOCgps+Atw1bUmaKL78r6lF/Uy0Lsrckw0sfgpr+A==", "" });
        }
    }
}
