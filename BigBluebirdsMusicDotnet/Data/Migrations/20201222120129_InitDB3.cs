﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class InitDB3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "SecurityStamp",
                table: "Users",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreate",
                table: "Users",
                nullable: false,
                defaultValue: new DateTime(2020, 12, 22, 19, 1, 28, 879, DateTimeKind.Local).AddTicks(376));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "User_Like_Song",
                nullable: false,
                defaultValue: new DateTime(2020, 12, 22, 19, 1, 28, 911, DateTimeKind.Local).AddTicks(5381),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2020, 11, 22, 20, 58, 13, 219, DateTimeKind.Local).AddTicks(2335));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "User_Cmt_Song",
                nullable: false,
                defaultValue: new DateTime(2020, 12, 22, 19, 1, 28, 913, DateTimeKind.Local).AddTicks(8063),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2020, 11, 22, 20, 58, 13, 221, DateTimeKind.Local).AddTicks(7662));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "Songs",
                nullable: false,
                defaultValue: new DateTime(2020, 12, 22, 19, 1, 28, 897, DateTimeKind.Local).AddTicks(9560),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2020, 11, 22, 20, 58, 13, 204, DateTimeKind.Local).AddTicks(6409));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "PlayList",
                nullable: false,
                defaultValue: new DateTime(2020, 12, 22, 19, 1, 28, 901, DateTimeKind.Local).AddTicks(1952),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2020, 11, 22, 20, 58, 13, 208, DateTimeKind.Local).AddTicks(9446));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateSendRequest",
                table: "Friends",
                nullable: false,
                defaultValue: new DateTime(2020, 12, 22, 19, 1, 28, 916, DateTimeKind.Local).AddTicks(32),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2020, 11, 22, 20, 58, 13, 223, DateTimeKind.Local).AddTicks(9337));

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000001"),
                column: "ConcurrencyStamp",
                value: "0069754f-3c54-4134-a302-45ede63cb65b");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000002"),
                column: "ConcurrencyStamp",
                value: "6666304c-2651-4358-8b7a-df0d7259550a");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000003"),
                column: "ConcurrencyStamp",
                value: "881053fc-e1b8-4cbe-82fe-e90fff101e8e");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000004"),
                column: "ConcurrencyStamp",
                value: "a0ae0bb2-439e-4b76-8a56-ed43977fe935");

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateSendRequest",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 982, DateTimeKind.Local).AddTicks(972));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateSendRequest",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 982, DateTimeKind.Local).AddTicks(1543));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateSendRequest",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 982, DateTimeKind.Local).AddTicks(1556));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateSendRequest",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 982, DateTimeKind.Local).AddTicks(1558));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateSendRequest",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 982, DateTimeKind.Local).AddTicks(1560));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(4180));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(8122));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(8225));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(8229));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(8232));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(8236));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(8239));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(8241));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(8245));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(8248));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(8251));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(8254));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(8258));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(8261));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000001"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "9a065fc0-57a7-4291-8abf-9b1d507152f5", "AQAAAAEAACcQAAAAEB8e5dmXPNWpNN6eNxd9Ktx2yXn77xZ7dUjCfN/2ewD46dVRqxu/WDnYvN6LJfK0Uw==", "" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000002"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "567be988-1f9d-424a-a0f5-9e83b4372637", "AQAAAAEAACcQAAAAECD3SnKnLtjiDF5gOBTD5fmUbDrn+0c+asicfN5jTfX2ZpufN095mke0XHtPjwaD4g==", "" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000003"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "8fa4f530-6452-4529-9276-5e9a05fa9607", "AQAAAAEAACcQAAAAED7/BMXCg+1Ta0X7LE4uI4q1skQhRD/7st6x+KQZqWj5mf8UrsNPfrqoQGADMqAucA==", "" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000004"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "fb3d8122-80f0-444d-abe4-5765fdb175dc", "AQAAAAEAACcQAAAAEC+FJ4+cQfvQs2lqanI35C01kaRMWD+WdSVyPiy+rejl4U+1lOFBGSN0BzZyUeRdfQ==", "" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateCreate",
                table: "Users");

            migrationBuilder.AlterColumn<string>(
                name: "SecurityStamp",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldDefaultValue: "");

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "User_Like_Song",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2020, 11, 22, 20, 58, 13, 219, DateTimeKind.Local).AddTicks(2335),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2020, 12, 22, 19, 1, 28, 911, DateTimeKind.Local).AddTicks(5381));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "User_Cmt_Song",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2020, 11, 22, 20, 58, 13, 221, DateTimeKind.Local).AddTicks(7662),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2020, 12, 22, 19, 1, 28, 913, DateTimeKind.Local).AddTicks(8063));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "Songs",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2020, 11, 22, 20, 58, 13, 204, DateTimeKind.Local).AddTicks(6409),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2020, 12, 22, 19, 1, 28, 897, DateTimeKind.Local).AddTicks(9560));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "PlayList",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2020, 11, 22, 20, 58, 13, 208, DateTimeKind.Local).AddTicks(9446),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2020, 12, 22, 19, 1, 28, 901, DateTimeKind.Local).AddTicks(1952));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateSendRequest",
                table: "Friends",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2020, 11, 22, 20, 58, 13, 223, DateTimeKind.Local).AddTicks(9337),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2020, 12, 22, 19, 1, 28, 916, DateTimeKind.Local).AddTicks(32));

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000001"),
                column: "ConcurrencyStamp",
                value: "3beea34f-1cac-4f0d-8919-539ff684ea9f");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000002"),
                column: "ConcurrencyStamp",
                value: "ebe2d3e0-d8d8-45d6-bbf6-517c732f5a18");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000003"),
                column: "ConcurrencyStamp",
                value: "bc515002-0d02-44d7-9ece-7f71f6b59f7d");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000004"),
                column: "ConcurrencyStamp",
                value: "31f47ed9-4d5a-44b1-aa8f-da40799a8cdc");

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateSendRequest",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(5620));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateSendRequest",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(6182));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateSendRequest",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(6194));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateSendRequest",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(6196));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateSendRequest",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(6197));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 285, DateTimeKind.Local).AddTicks(8699));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(2807));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(2892));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(2896));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(2899));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(2903));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(2906));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(2909));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(2911));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(2915));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(2918));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(2920));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(2923));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreate",
                value: new DateTime(2020, 11, 22, 20, 58, 13, 286, DateTimeKind.Local).AddTicks(2926));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000001"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "6b9b2d3f-706d-4e33-988c-6da1000fc7c2", "AQAAAAEAACcQAAAAEModV2bCk1tw1t1TPzgs4ec6LdE4MlG1YV6M6hG4TIjfjL8r9vmSHQQ8Tr+4HQ5Oag==" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000002"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "3f9f53a5-d775-4daf-b596-bb1f77fdbc93", "AQAAAAEAACcQAAAAEHaPcEnUf4q+xcRSFnEOHnXvSkA5vnaC/PlY0uiMmFxnJc3eKUhnn2cr/t936DRmuQ==" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000003"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "7e3ac422-f7f4-41d9-a47d-ad4091123434", "AQAAAAEAACcQAAAAEKr8TXclEhC6mzrRGQ5weNrazT4MduIiXNXdjFQfNjciUblDU2KlYXScAdMaWuq3kg==" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000004"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "b122f61d-0d2c-4c20-8e2c-601af66555bb", "AQAAAAEAACcQAAAAEF6Je8EmBgWSba7VCJIMcYu88zQ4oo3uymmbcd224y6YZxdfNDwV/ixPcKn2GLs1Xg==" });
        }
    }
}
