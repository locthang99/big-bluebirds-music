﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class Song_Owner
    {
        public int SongId { get; set; }
        public Song Song { get; set; }

        public Guid OwnerId { get; set; }
        public User Owner { get; set; }
    }
}
