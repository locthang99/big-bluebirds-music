﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class CacheData
    {
        public int Id { get; set; }
        public string OwnerId { get; set; }
        public string Data { get; set; }
        public string Type { get; set; }
        public int TimeExpire { get; set; }
        public DateTime DateCreate { get; set; }
    }
}
