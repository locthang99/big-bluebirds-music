﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class User_Cmt_Song
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }

        public int SongId { get; set; }
        public Song Song { get; set; }
        public DateTime DateCreate { get; set; }
        public string Content { get; set; }
    }
}
