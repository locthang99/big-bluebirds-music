﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class Tag
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<Song_Tag> Song_Tags { get; set; }
    }
}
