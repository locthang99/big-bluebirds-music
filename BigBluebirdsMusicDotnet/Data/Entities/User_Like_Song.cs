﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class User_Like_Song
    {
        public Guid UserId { get; set; }
        public User User { get; set; }

        public int SongId { get; set; }
        public Song Song { get; set; }
        public DateTime DateCreate { get; set; }
    }
}
