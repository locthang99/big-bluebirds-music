﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class Song_PlayList
    {
        public int SongId { get; set; }
        public Song Song { get; set; }

        public int PlayListId { get; set; }
        public PlayList PlayList { get; set; }

    }
}
