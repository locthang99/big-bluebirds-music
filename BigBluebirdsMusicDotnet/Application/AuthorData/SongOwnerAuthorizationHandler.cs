﻿
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Identity;
using Models.Catalog.Song;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Application.AuthorData
{
    public class SongOwnerAuthorizationHandler : AuthorizationHandler<OperationAuthorizationRequirement, SongModel>
    {

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,OperationAuthorizationRequirement requirement,SongModel resource)
        {
            //if (context.User == null || resource == null)
            //{
            //    return Task.CompletedTask;
            //}

            //// If not asking for CRUD permission, return.

            ////if (requirement.Name != Constants.CreateOperationName &&
            ////    requirement.Name != Constants.ReadOperationName &&
            ////    requirement.Name != Constants.UpdateOperationName &&
            ////    requirement.Name != Constants.DeleteOperationName)
            ////{
            ////    return Task.CompletedTask;
            ////}
            if(context.User.IsInRole("admin"))
            {
                context.Succeed(requirement);
                return Task.CompletedTask;

            }
            resource.Owners.ForEach(x =>
            {
                if (x.OwnerId.ToString() == context.User.FindFirst(ClaimTypes.NameIdentifier).Value)
                {
                    context.Succeed(requirement);
                }
            });
            return Task.CompletedTask;
        }
    }
    public class SameAuthorRequirement : IAuthorizationRequirement { }
}
