﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Application.AuthorData
{
    public class UserAuthorizationHandler : AuthorizationHandler<OperationAuthorizationRequirement, Guid>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, OperationAuthorizationRequirement requirement, Guid resource)
        {          
                if (resource.ToString() == context.User.FindFirst(ClaimTypes.NameIdentifier).Value)
                {
                    context.Succeed(requirement);
                }
            return Task.CompletedTask;
        }
    }
}
