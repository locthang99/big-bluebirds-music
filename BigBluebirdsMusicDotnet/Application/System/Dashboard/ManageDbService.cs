﻿using Models.System.Dashboard;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Application.Base;
using Data.EF;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Application.System.Dashboard
{
    public class ManageDbService : IManageDbService
    {
        private readonly BigBluebirdsDbContext _context;
        private readonly IApplicationServiceBase _applicationServiceBase;
        //private readonly UserManager<Data.Entities.User> _userManager;
        //private readonly RoleManager<AppRole> _roleManager;
        public ManageDbService(BigBluebirdsDbContext context, IApplicationServiceBase applicationServiceBase)
        {
            _context = context;
            _applicationServiceBase = applicationServiceBase;

        }
        public async Task<List<DashboardModel>> GetNumberSong(string type)
        {
            // var data = await _context.Songs.GroupBy(x => x.DateCreate.Year).ToListAsync();
            if (type == "month")
            {
                var data = from song in _context.Songs
                           group song by new { song.DateCreate.Month, song.DateCreate.Year } into g
                           select new
                           {
                               M = g.Key.Month,
                               Y = g.Key.Year,
                               Title = g.Key.Month.ToString() + "/" + g.Key.Year.ToString(),
                               Total = g.Count()
                           };
                return await data.Select(x => new DashboardModel
                {
                    M = x.M,
                    Y = x.Y,
                    Title = x.Title,
                    Value = x.Total.ToString()
                }).OrderBy(x => x.Y).ThenBy(x => x.M).ToListAsync();
            }
            else
            {
                var data = from song in _context.Songs
                           group song by new { song.DateCreate.Year } into g
                           select new
                           {
                               Y = g.Key.Year,
                               Title = g.Key.Year.ToString(),
                               Total = g.Count()
                           };
                return await data.Select(x => new DashboardModel
                {
                    Y = x.Y,
                    Title = x.Title,
                    Value = x.Total.ToString()
                }).OrderBy(x => x.Y).ToListAsync();
            }
        }
        public async Task<List<DashboardModel>> GetNumberPlaylist(string type)
        {
            // var data = await _context.Songs.GroupBy(x => x.DateCreate.Year).ToListAsync();
            if (type == "month")
            {
                var data = from pl in _context.PlayLists
                           group pl by new { pl.DateCreate.Month, pl.DateCreate.Year } into g
                           select new
                           {
                               M = g.Key.Month,
                               Y = g.Key.Year,
                               Title = g.Key.Month.ToString() + "/" + g.Key.Year.ToString(),
                               Total = g.Count()
                           };
                return await data.Select(x => new DashboardModel
                {
                    M = x.M,
                    Y = x.Y,
                    Title = x.Title,
                    Value = x.Total.ToString()
                }).OrderBy(x => x.Y).ThenBy(x => x.M).ToListAsync();
            }
            else
            {
                var data = from pl in _context.PlayLists
                           group pl by new { pl.DateCreate.Year } into g
                           select new
                           {
                               Y = g.Key.Year,
                               Title = g.Key.Year.ToString(),
                               Total = g.Count()
                           };
                return await data.Select(x => new DashboardModel
                {
                    Y = x.Y,
                    Title = x.Title,
                    Value = x.Total.ToString()
                }).OrderBy(x => x.Y).ToListAsync();
            }
        }
        public async Task<List<DashboardModel>> GetTotalPlaylist()
        {
            // var data = await _context.Songs.GroupBy(x => x.DateCreate.Year).ToListAsync();
            var now = DateTime.Now;
            var week = await _context.PlayLists.CountAsync(x => DateTime.Now.AddDays(-7).CompareTo(x.DateCreate) == -1);
            var month = await _context.PlayLists.CountAsync(x => DateTime.Now.AddDays(-30).CompareTo(x.DateCreate) == -1);
            var year = await _context.PlayLists.CountAsync(x => DateTime.Now.AddDays(-365).CompareTo(x.DateCreate) == -1);
            var total = await _context.PlayLists.CountAsync();

            var data = new List<DashboardModel>();
            data.Add(new DashboardModel() { Title = "Tuần qua", Value = week.ToString() });
            data.Add(new DashboardModel() { Title = "Tháng qua", Value = month.ToString() });
            data.Add(new DashboardModel() { Title = "Năm qua", Value = year.ToString() });
            data.Add(new DashboardModel() { Title = "Tổng số playlist", Value = total.ToString() });
            return data;

        }

        public async Task<List<DashboardModelVIP>> GetAll(string type)
        {
            if (type == "month")
            {
                var data = from song in _context.Songs
                           group song by new { song.DateCreate.Month, song.DateCreate.Year } into g
                           select new
                           {
                               M = g.Key.Month,
                               Y = g.Key.Year,
                               Title = g.Key.Month.ToString() + "/" + g.Key.Year.ToString(),
                               Like = g.Sum(x=> Convert.ToInt64(x.TotalLike)),
                               Cmt = g.Sum(x => Convert.ToInt64(x.TotalCmt)),
                               Listen = g.Sum(x => Convert.ToInt64(x.TotalListen)),
                               Download = g.Sum(x => Convert.ToInt64(x.TotalDownload)),

                           };
                return await data.Select(x => new DashboardModelVIP
                {
                    M = x.M,
                    Y = x.Y,
                    Title = x.Title,
                    Like=x.Like,
                    Cmt=x.Cmt,
                    Listen=x.Listen,
                    Download=x.Download
                }).OrderBy(x => x.Y).ThenBy(x => x.M).ToListAsync();
            }
            else
            {
                var data = from song in _context.Songs
                           group song by new {song.DateCreate.Year } into g
                           select new
                           {
                               
                               Y = g.Key.Year,
                               Title = g.Key.Year.ToString(),
                               Like = g.Sum(x => Convert.ToInt64(x.TotalLike)),
                               Cmt = g.Sum(x => Convert.ToInt64(x.TotalCmt)),
                               Listen = g.Sum(x => Convert.ToInt64(x.TotalListen)),
                               Download = g.Sum(x => Convert.ToInt64(x.TotalDownload)),

                           };
                return await data.Select(x => new DashboardModelVIP
                {
                    
                    Y = x.Y,
                    Title = x.Title,
                    Like = x.Like,
                    Cmt = x.Cmt,
                    Listen = x.Listen,
                    Download = x.Download
                }).OrderBy(x => x.Y).ToListAsync();
            }
        }
        public async Task<List<DashboardModel>> GetAge(string type)
        {
            var data = from u in _context.Users
                       group u by new { u.Dob.Year } into g
                       select new
                       {

                           Y = g.Key.Year,
                           Title =  g.Key.Year.ToString(),
                           Total = g.Count()
                       };
            return await data.Select(x => new DashboardModel
            {
                Y = x.Y,
                Title = x.Title,
                Value = x.Total.ToString()
            }).OrderBy(x => x.Y).ToListAsync();
        }

        public async Task<List<DashboardModel>> GetCreateUser(string type)
        {
            
            if(type=="month")
            {
                var data = from u in _context.Users
                           group u by new { u.DateCreate.Month,u.DateCreate.Year } into g
                           select new
                           {
                               M=g.Key.Year,
                               Y = g.Key.Year,
                               Title = g.Key.Month.ToString() +"/"+g.Key.Year.ToString(),
                               Total = g.Count()
                           };
                return await data.Select(x => new DashboardModel
                {
                    M=x.M,
                    Y = x.Y,
                    Title = x.Title,
                    Value = x.Total.ToString()
                }).OrderBy(x => x.Y).ThenBy(x=>x.M).ToListAsync();
            }
            else
            {
                var data = from u in _context.Users
                           group u by new { u.DateCreate.Year } into g
                           select new
                           {
                               Y = g.Key.Year,
                               Title = g.Key.Year.ToString(),
                               Total = g.Count()
                           };
                return await data.Select(x => new DashboardModel
                {
                    Y = x.Y,
                    Title = x.Title,
                    Value = x.Total.ToString()
                }).OrderBy(x => x.Y).ToListAsync();
            }
        }
    }
}
