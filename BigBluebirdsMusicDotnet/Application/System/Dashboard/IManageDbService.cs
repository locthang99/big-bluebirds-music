﻿
using Models.System.Dashboard;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.System.Dashboard
{
    public interface IManageDbService
    {
        


        Task<List<DashboardModel>> GetNumberSong(string type);
        Task<List<DashboardModel>> GetNumberPlaylist(string type);
        Task<List<DashboardModel>> GetTotalPlaylist();
        Task<List<DashboardModelVIP>> GetAll(string type);
        Task<List<DashboardModel>> GetAge(string type);
        Task<List<DashboardModel>> GetCreateUser(string type);
    }

}
