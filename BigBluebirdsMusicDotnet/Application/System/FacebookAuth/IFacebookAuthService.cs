﻿using Models.System.FacebookAuth;
using Models.System.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.System.FacebookAuth
{
    public interface IFacebookAuthService
    {
        Task<FBValidateTokenModel> ValidateAccessToken(string Token);
        Task<FBUserInforModel> GetUserInfor(string Token);
    }
}
