﻿using Models.Page;
using Models.System.User;
using Models.System.User.UserRequest;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Application.System.User
{
    public interface IManageUserService
    {
        Task<TokenResponse> Login(UserLoginRequest rq);
        Task<TokenResponse> LoginWithFacebook(string Token);
        Task<TokenResponse> Register(UserRegisterRequest rq);
        Task<UserModel> GetCurrentUser();        
        Task<List<UserModel>> GetByUsername(string rqUser);
        Task<UserModel> GetById(Guid Id);
        Task<List<UserModel>> GetAllUser(PagingRequest pg);
        Task<List<UserModel>> GetUserByPhone(string Phones,PagingRequest pg);
        Task<int> Update( Guid Id, UserUpdateRequest rq);
        Task<int> Delete( Guid Id);
        Task<int> SetRole(Guid Id,string RoleName);
        Task<List<FriendModel>> GetAllStatusFiend();
        Task<List<FirendAcceptedModel>> GetAllFiend();
        Task<int> SendFriendRequest(Guid ReceiverId);
        Task<int> AcceptFriendRequest(Guid SenderID);
        Task<int> DenyFriendRequest(Guid SenderID);

    }
}
