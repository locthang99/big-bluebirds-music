﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Data.Entities;
using Models.System.User;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Models.System.User.UserRequest;
using Models.Page;
using Data.EF;
using Microsoft.AspNetCore.Http;
using Application.Common;
using System.Net.Http.Headers;
using System.IO;
using Application.System.FacebookAuth;
using Models.Enum;
using Application.Base;

namespace Application.System.User
{
    public class ManageUserService : IManageUserService
    {
        private readonly BigBluebirdsDbContext _context;
        private readonly IApplicationServiceBase _applicationServiceBase;
        private readonly UserManager<Data.Entities.User> _userManager;
        private readonly SignInManager<Data.Entities.User> _signInManager;
        private readonly RoleManager<AppRole> _roleManager;
        private readonly IConfiguration _config;
        private readonly IStorageService _storageService;
        private readonly IFacebookAuthService _facebookAuthService;
        public ManageUserService(BigBluebirdsDbContext context,
            IApplicationServiceBase applicationServiceBase,
            UserManager<Data.Entities.User> userManager, SignInManager<Data.Entities.User> signInManager,
            RoleManager<AppRole> roleManager, IConfiguration config, IStorageService storageService, IFacebookAuthService facebookAuthService)
        {
            _context = context;
            _userManager = userManager;
            _applicationServiceBase = applicationServiceBase;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _config = config;
            _storageService = storageService;
            _facebookAuthService = facebookAuthService;
        }
        #region Login Register
        public async Task<TokenResponse> Login(UserLoginRequest rq)
        {
            var user = await _userManager.FindByNameAsync(rq.User);
            if (user == null)
            {
                return null;
                //throw new ShopException("Cannot find User!");
            }
            var result = await _signInManager.PasswordSignInAsync(user, rq.Password, rq.RememberMe, true);
            if (!result.Succeeded)
                return null;


            var rs = new TokenResponse()
            {
                UserId = user.Id.ToString(),
                Username = user.UserName,
                Token = await GenerateToken(user)
            };
            return rs;
        }

        private async Task<string> GenerateToken(Data.Entities.User user)
        {
            var roles = await _userManager.GetRolesAsync(user);
            var claims = new[]
            {
                new Claim(ClaimTypes.Email,user.Email),
                new Claim(ClaimTypes.GivenName,user.Id.ToString()),
                new Claim(ClaimTypes.Role, string.Join(";",roles)),
                new Claim(ClaimTypes.Name,(user.FirstName+" "+user.LastName)),
                new Claim(ClaimTypes.NameIdentifier,user.Id.ToString())
            };
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Tokens:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_config["Tokens:Issuer"],
                _config["Tokens:Issuer"],
                claims,
                expires: DateTime.Now.AddHours(3),
                signingCredentials: creds);
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
        public async Task<TokenResponse> LoginWithFacebook(string Token)
        {
            var validateRS = await _facebookAuthService.ValidateAccessToken(Token);
            if (!validateRS.Data.IsValid)
            {
                return new TokenResponse()
                {
                    Token = "",
                    UserId = "",
                    Username = ""
                };
            }

            var userFbInfor = await _facebookAuthService.GetUserInfor(Token);
            var user = await _userManager.FindByEmailAsync(userFbInfor.Email);
            if (user != null)
            {
                return new TokenResponse()
                {
                    Token = await GenerateToken(user),
                    UserId = user.Id.ToString(),
                    Username = user.Email
                };
            }

            if (user == null)
            {
                user = new Data.Entities.User()
                {
                    Id = new Guid(),
                    Email = userFbInfor.Email,
                    FirstName = userFbInfor.FirstName,
                    LastName = userFbInfor.LastName,
                    UserName = userFbInfor.Email,
                    AccountType = AccountType.FACEBOOK.ToString(),
                    Thumbnail = userFbInfor.Picture.Data.Url.ToString()

                };
                //user.FileImages = new List<FileImage>();
                //user.FileImages.Add(new FileImage()
                //{

                //    Description = "Thumbnail",
                //    Type = ImageType.INTERNET.ToString(),
                //    Path = userFbInfor.Picture.Data.Url.ToString(),
                //});
                var rs = await _userManager.CreateAsync(user);
                if (!rs.Succeeded)
                    return new TokenResponse();
            }
            var rsRole = await _context.UserRoles.AddAsync(new IdentityUserRole<Guid>()
            {
                UserId = user.Id,
                RoleId = StaticID.RoleUserId
            });
            await _context.SaveChangesAsync();
            return new TokenResponse()
            {
                Token = await GenerateToken(user),
                UserId = user.Id.ToString(),
                Username = user.Email

            };


        }
        public async Task<TokenResponse> Register(UserRegisterRequest rq)
        {
            var user = new Data.Entities.User()
            {
                Id = new Guid(),
                Dob = rq.Dob,
                Email = rq.Email,
                FirstName = rq.FirstName,
                LastName = rq.LastName,
                UserName = rq.User,
                PhoneNumber = rq.PhoneNumber,
                AccountType = AccountType.SYSTEM.ToString(),
                Thumbnail = await _storageService.SaveFile(rq.Thumbnail, 0),
            };
            //Add Image
            //user.FileImages = new List<FileImage>();
            //if (rq.Thumbnail != null)
            //{
            //    user.FileImages.Add(new FileImage()
            //    {
            //        Description = "Thumbnail",
            //        Type = rq.Thumbnail.ContentType,
            //        FileSize = rq.Thumbnail.Length,
            //        Path = await _storageService.SaveFile(rq.Thumbnail, 0),
            //    });
            //}
            var result = await _userManager.CreateAsync(user, rq.Password);

            if (result.Succeeded)
            {
                var rsRole = await _context.UserRoles.AddAsync(new IdentityUserRole<Guid>()
                {
                    UserId = user.Id,
                    RoleId = StaticID.RoleUserId
                });
                await _context.SaveChangesAsync();
                return new TokenResponse()
                {
                    Token = await GenerateToken(user),
                    UserId = user.Id.ToString(),
                    Username = user.Email

                };

            }
            return null;
        }
        #endregion

        #region CRUD User
        private UserModel MapUser(Data.Entities.User user)
        {
            if (user == null)
                return null;
            var data = new UserModel()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                PhoneNumber = user.PhoneNumber,
                UserName = user.UserName,
                Dob=user.Dob,
                Email = user.Email,
                Role = GetRole(user.Id),
                Thumbnail = user.Thumbnail

        };
            //var srcImg = _context.FileImages.Where(z => z.IdUser == user.Id && z.Description == "Thumbnail").FirstOrDefault();

            if (!user.Thumbnail.Contains("http")&&user.Thumbnail!="")
                data.Thumbnail = _config["File:Image"] + user.Thumbnail;
                

            return data;
        }
        private string GetRole(Guid UserId)
        {
            var RoleId = _context.UserRoles.Where(x => x.UserId == UserId).Select(x => x.RoleId).FirstOrDefault();
            if (RoleId == StaticID.RoleIdNull)
                return null;
            var rs = _context.Roles.Find(RoleId).Name;
            return rs;
        }
        public async Task<UserModel> GetCurrentUser()
        {
            //var Id = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var Id = _applicationServiceBase.GetCurrentUserId();
            var user = await _userManager.FindByIdAsync(Id);
            if (user != null)
                return MapUser(user);
            else
                return null;
        }
        public async Task<UserModel> GetById(Guid Id)
        {

            var user = await _userManager.FindByIdAsync(Id.ToString());
            if (user != null)
                return MapUser(user);
            else
                return null;

        }
        public async Task<List<UserModel>> GetAllUser(PagingRequest pg)
        {
            var query = await _userManager.Users.Skip((pg.Index - 1) * pg.PageSize).Take(pg.PageSize).ToListAsync();
            var data = query.Select(x => MapUser(x)).ToList();
            return data;
        }
        public async Task<List<UserModel>> GetUserByPhone(string Phone, PagingRequest pg)
        {
            var query = await _context.Users.Where(u=>Phone==u.PhoneNumber).Skip((pg.Index - 1) * pg.PageSize).Take(pg.PageSize).ToListAsync();
            var data = query.Select(x => MapUser(x)).ToList();
            return data;
        }
        public async Task<List<UserModel>> GetByUsername(string rqUser)
        {
            var data = await _userManager.Users.Where(x => x.UserName.Contains(rqUser)|| rqUser.Contains(x.UserName)).ToListAsync();
            var rs = data.Select(u => MapUser(u)).ToList();
            return rs;
        }
        public async Task<int> Update(Guid Id, UserUpdateRequest rq)
        {
            if (!_applicationServiceBase.CheckAuthentication())
                return -401;
            if (!(_applicationServiceBase.GetCurrentUserId() == Id.ToString() || _applicationServiceBase.CheckRole("admin")))
                return -403;
            var user = await _userManager.FindByIdAsync(Id.ToString());
            if (user == null)
                return -404;
            else
            {
                //if (user.AccountType != AccountType.SYSTEM.ToString())
                //{
                //    user.Email = rq.Email;
                //    user.FirstName = rq.FirstName;
                //    user.LastName = rq.LastName;
                //}
                user.Email = rq.Email;
                user.FirstName = rq.FirstName;
                user.LastName = rq.LastName;
                user.Dob = rq.Dob;
                user.PhoneNumber = rq.PhoneNumber;
                if (rq.Thumbnail != null)
                {
                    user.Thumbnail = await _storageService.SaveFile(rq.Thumbnail, 0);
                }
            }

            //if (rq.Thumbnail != null)
            //{
            //    var ThumnailUpdate = _context.FileImages.Where(x => x.User.ToString() == Id.ToString() && x.Description == "Thumbnail").FirstOrDefault();
            //    if (ThumnailUpdate == null)
            //    {
            //        //ThumnailUpdate = new FileImage();
            //        //ThumnailUpdate.Description = "Thumbail";
            //        //_context.FileImages.Add(ThumnailUpdate);
            //    }

            //    ThumnailUpdate.Type = ImageType.SYSTEM.ToString();
            //    ThumnailUpdate.FileSize = rq.Thumbnail.Length;
            //    ThumnailUpdate.Path = await _storageService.SaveFile(rq.Thumbnail, 0);
            //    _context.Update(ThumnailUpdate);
            //}

            _context.Users.Update(user);
            var rs = await _context.SaveChangesAsync();
            return rs; 
        }
        public async Task<int> Delete(Guid Id)
        {

            if (!_applicationServiceBase.CheckAuthentication())
                return -401;
            if (_applicationServiceBase.CheckRole("admin"))
                return -403;
            var user = await _userManager.FindByIdAsync(Id.ToString());
            if (user == null)
                return -404;
            var rs = await _userManager.DeleteAsync(user);
            if (rs.Succeeded)
                return 1;
            else
                return 0;
        }

        public async Task<int> SetRole(Guid Id, string RoleName) 
        {

            if (!_applicationServiceBase.CheckAuthentication())
                return -401;
            if (!_applicationServiceBase.CheckRole("admin"))
                return -403;
            var user = await _userManager.FindByIdAsync(Id.ToString());
            if (user == null)
                return -404;
            var currentRole = await _userManager.GetRolesAsync(user);
            
            if (currentRole.Count > 0)
            {
               await _userManager.RemoveFromRolesAsync(user, currentRole);
            }
            var rsForce = await _userManager.AddToRoleAsync(user, RoleName);
            if (rsForce.Succeeded)
                return 1;
            else
                return 0;
        }
        #endregion
        #region Friend
        public async Task<List<FriendModel>> GetAllStatusFiend()
        {
            var Id = _applicationServiceBase.GetCurrentUserId();
            Guid UserId = new Guid(Id);
            var user = await _userManager.FindByIdAsync(UserId.ToString());
            if (user == null)
                return null;
            var l1 = await _context.Friends.Where(u => u.SenderId == UserId).Select(l => new FriendModel()
            {
                SenderId = l.SenderId,
                SenderName = l.Sender.FirstName + " " + l.Sender.LastName,
                ReceiverId = l.ReceiverId,
                ReceiverName = l.Receiver.FirstName + " " + l.Receiver.LastName,
                Status = ExacRq(l.Status),
                DateSendRequest = l.DateSendRequest,
                DateUpdateRequest = l.DateUpdateRequest,
            }
            ).ToListAsync();

            var l2 = await _context.Friends.Where(u => u.ReceiverId == UserId).Select(l => new FriendModel()
            {
                SenderId = l.SenderId,
                SenderName = l.Sender.FirstName + " " + l.Sender.LastName,
                ReceiverId = l.ReceiverId,
                ReceiverName = l.Receiver.FirstName + " " + l.Receiver.LastName,
                Status = ExacRq(l.Status),
                DateSendRequest = l.DateSendRequest,
                DateUpdateRequest = l.DateUpdateRequest,
            }
            ).ToListAsync();
            return l1.Concat(l2).ToList();
        }
        public async Task<List<FirendAcceptedModel>> GetAllFiend()
        {
            var Id = _applicationServiceBase.GetCurrentUserId();
            Guid UserId = new Guid(Id);
            var user = await _userManager.FindByIdAsync(UserId.ToString());
            if (user == null)
                return null;
            var l1 = await _context.Friends.Where(u => u.SenderId == UserId && u.Status == 1).Select(l => new FirendAcceptedModel()
            {
                FriendId = l.ReceiverId,
                FriendName = l.Receiver.FirstName + " " + l.Sender.LastName,
                Status = ExacRq(l.Status),
                DateSendRequest = l.DateSendRequest,
                DateAcceptedRequest = l.DateUpdateRequest,
            }
            ).ToListAsync();
            var l2 = await _context.Friends.Where(u => u.ReceiverId == UserId && u.Status == 1).Select(l => new FirendAcceptedModel()
            {
                FriendId = l.SenderId,
                FriendName = l.Sender.FirstName + " " + l.Sender.LastName,
                Status = ExacRq(l.Status),
                DateSendRequest = l.DateSendRequest,
                DateAcceptedRequest = l.DateUpdateRequest,
            }
            ).ToListAsync();

            return l1.ToList();
        }
        public async Task<int> SendFriendRequest(Guid ReceiverId)
        {
            if (!_applicationServiceBase.CheckAuthentication())
                return -401;
            var SenderId = new Guid(_applicationServiceBase.GetCurrentUserId());
            if (SenderId == ReceiverId)
                return -2;
            var Sender = await _userManager.FindByIdAsync(SenderId.ToString());
            var Receiver = await _userManager.FindByIdAsync(ReceiverId.ToString());
            if (Sender == null || Receiver == null)
                return -404;// ko ton tai user
            else
            {
                var l1 = await _context.Friends.Where(u => u.SenderId == SenderId && u.ReceiverId == ReceiverId).ToListAsync();
                if (l1.Count != 0)
                {
                    if (l1.First().Status == 1)
                        return -1;
                    l1.First().Status = 0;
                    l1.First().DateSendRequest = DateTime.Now;
                    l1.First().DateUpdateRequest = DateTime.Now;
                    _context.Friends.Update(l1.First());
                    await _context.SaveChangesAsync();
                    return 0;//da gui loi moi ket ban
                }

                else
                {
                    var l2 = await _context.Friends.Where(u => u.SenderId == ReceiverId && u.ReceiverId == SenderId).ToListAsync();
                    if (l2.Count != 0)
                    {
                        l2.First().Status = 1;//nguoi ta da gui loi moi ket ban truoc do thi accept
                        _context.Friends.Update(l2.First());
                        await _context.SaveChangesAsync();
                        return 1;
                    }
                    else
                    {
                        var FriendRq = new Friend()
                        {
                            SenderId = SenderId,
                            Sender = Sender,
                            ReceiverId = ReceiverId,
                            Receiver = Receiver,
                            Status = 0,//waiting
                            DateSendRequest = DateTime.Now
                        };//tao moi yeu cau ket ban
                        _context.Friends.Add(FriendRq);
                        await _context.SaveChangesAsync();
                        return 2;
                    }
                }
            }
        }
        public static string ExacRq(int x)
        {
            switch (x)
            {
                case -1:
                    return "Rejected";
                case 0:
                    return "Waiting";
                case 1:
                    return "Accpeted";
                default:
                    return "";
            }
        }
        public async Task<int> AcceptFriendRequest(Guid SenderId)
        {
            if (!_applicationServiceBase.CheckAuthentication())
                return -401;
            Guid ReceiverId = new Guid(_applicationServiceBase.GetCurrentUserId());
            var rq = await _context.Friends.Where(u => u.SenderId == SenderId && u.ReceiverId == ReceiverId).ToListAsync();
            if (rq.Count != 0)
            {
                if (rq.First().Status != 0)//status 0:wating thi co the accept
                    return 0;
                rq.First().Status = 1;
                rq.First().DateSendRequest = DateTime.Now;
                rq.First().DateUpdateRequest = DateTime.Now;
                _context.Friends.Update(rq.First());
                await _context.SaveChangesAsync();
                return 1;
            }
            else
                return -404;//khong ton tai loi moi ket ban
        }
        public async Task<int> DenyFriendRequest(Guid SenderId)
        {
            if (!_applicationServiceBase.CheckAuthentication())
                return -401;
            Guid ReceiverId = new Guid(_applicationServiceBase.GetCurrentUserId());
            var rq = await _context.Friends.Where(u => u.SenderId == SenderId && u.ReceiverId == ReceiverId).ToListAsync();
            if (rq.Count != 0)
            {
                if (rq.First().Status != 0)//status 0:wating thi co the deny
                    return 0;
                rq.First().Status = -1;
                rq.First().DateSendRequest = DateTime.Now;
                rq.First().DateUpdateRequest = DateTime.Now;
                _context.Friends.Update(rq.First());
                await _context.SaveChangesAsync();
                return 1;
            }
            else
                return -404;//khong ton tai loi moi ket ban
        }


        #endregion
    }
}
