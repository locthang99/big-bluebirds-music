﻿using Data.EF;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Application.Cache
{
    public class CacheService : ICacheService
    {
        private readonly BigBluebirdsDbContext _context;
        public CacheService(BigBluebirdsDbContext context)
        {
            _context = context;
        }
        public async Task<int> AddCache(CacheData rq)
        {
            var cached = _context.CacheDatas.FirstOrDefault(c => c.OwnerId == rq.OwnerId && c.Type == rq.Type);
            if(cached!=null)
            {
                cached.Data = rq.Data;
                cached.DateCreate = DateTime.Now;
                _context.CacheDatas.Update(cached);
                await _context.SaveChangesAsync();
                return cached.Id;
            }
            else
            {
                rq.DateCreate = DateTime.Now;
                _context.CacheDatas.Add(rq);
                await _context.SaveChangesAsync();
                return rq.Id;
            }
        }

        public Task<int> DeleteCahe()
        {
            throw new NotImplementedException();
        }

        public async Task<string> GetCache(string OwnerId, string Type)
        {
            var cached =await _context.CacheDatas.FirstOrDefaultAsync(c => c.OwnerId == OwnerId && c.Type == Type);

            if (cached != null)
            {
                if (cached.DateCreate.AddHours(cached.TimeExpire).CompareTo(DateTime.Now) < 0)
                    return null;
                else
                    return cached.Data;
            }
            else
                return null;

        }
    }
}
