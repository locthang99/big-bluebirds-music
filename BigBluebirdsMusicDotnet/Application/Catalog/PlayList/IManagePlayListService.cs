﻿using Models.Catalog.Comment;
using Models.Catalog.PlayList;
using Models.Catalog.PlayList.PlayListRequest;
using Models.Catalog.Song;
using Models.Page;
using Models.System.User.UserRequest;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Application.Catalog.PlayList
{
    public interface IManagePlayListService
    {
        Task<PlayListModel> GetPlayListById(int Id);
        Task<List<PlayListModel>> GetPlayListByName(string Name, PagingRequest pg);
        Task<List<PlayListModel>> GetAllPlayList(PagingRequest pg);
        Task<List<PlayListModel>> GetPlayListPrivate(PagingRequest pg);
        Task<List<PlayListModel>> GetPlaylistByListId(List<int> ListId);
        Task<int> PlayListCreate(PlayListCreateRequest rq);
        Task<int> PlayListDelete(int Id);
        Task<int> PlayListUpdate(int Id, PlayListUpdateRequest rq);
        Task<List<SongModel>> GetAllSongFromPlayList(int Id, PagingRequest pg);
        Task<int> PushSongToPlayList(int IdPlayList,  int IdSong);
        Task<int> RemoveSongFromPlayList(int IdPlayList, int IdSong);
        //Task<int> PlayListGetSong(int Id);
        Task<List<CommentModel>> GetAllComment(int PlaylistId, PagingRequest pg);
        Task<int> LikePlaylist(int Id);
        Task<int> CheckLikePlaylist(int Id);
        Task<int> UnLikePlaylist(int Id);
        Task<int> CmtPlaylist(UserCmtRequest rq);

    }
}
