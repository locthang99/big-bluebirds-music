﻿using Application.Catalog.Song;
using Data.EF;
using Microsoft.EntityFrameworkCore;
using Models.Catalog.Song;
using Models.Catalog.Tag;
using Models.Catalog.Tag.TagRequest;
using Models.Page;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Catalog.Tag
{
    public class ManageTagService : IManageTagService
    {
        private readonly BigBluebirdsDbContext _context;
        private readonly IManageSongService _manageSongService;
        public ManageTagService(BigBluebirdsDbContext context, IManageSongService manageSongService)
        {
            _context = context;
            _manageSongService = manageSongService;
        }
        public async Task<List<TagModel>> GetAllTag(PagingRequest pg)
        {
            var query = from sp in _context.Tags
                        select new { sp };
            var data = await query.Select(x => new TagModel()
            {
                Id = x.sp.Id,
                Name = x.sp.Name,
                Description = x.sp.Description,
            })
                .Skip((pg.Index - 1) * pg.PageSize).Take(pg.PageSize)
                .ToListAsync();
            return data;
        }
        public async Task<TagModel> GetTagById(int Id)
        {

            var sp = await _context.Tags.FindAsync(Id);
            if (sp == null)
            {
                return null;
            }
            var data = new TagModel()
            {
                Id = sp.Id,
                Name = sp.Name,
                Description = sp.Description,
            };

            return data;
        }
        public async Task<List<TagModel>> GetTagByName(string Name, PagingRequest pg)
        {
            var query = from sp in _context.Tags
                        select new { sp };
            var data = await query.Select(x => new TagModel()
            {
                Id = x.sp.Id,
                Name = x.sp.Name,
                Description = x.sp.Description,
            }).Where(x => Name.Contains(x.Name) || x.Name.Contains(Name))
            .Skip((pg.Index - 1) * pg.PageSize).Take(pg.PageSize)
            .ToListAsync();
            return data;
        }
        public async Task<int> TagCreate(TagCreateRequest rq)
        {
            var checkName = _context.Tags.FirstOrDefault(t => t.Name == rq.Name);
            if (checkName != null)
                return -1;
            var t = new Data.Entities.Tag()
            {
                Name = rq.Name,
                Description = rq.Description
            };
            _context.Tags.Add(t);
            await _context.SaveChangesAsync();
            return t.Id;

        }
        public async Task<int> TagDelete(int Id)
        {
            var t = await _context.Tags.FindAsync(Id);
            if (t == null)
            {
                return -404;
            }
            _context.Tags.Remove(t);
            await _context.SaveChangesAsync();
            return t.Id;
        }
        public async Task<int> TagUpdate(int Id, TagUpdateRequest rq)
        {
            var t = await _context.Tags.FindAsync(Id);
            if (t == null)
            {
                return -404;
            }
            if (t.Name != rq.Name)
            {
                var checkName = _context.Tags.FirstOrDefault(t => t.Name == rq.Name);
                if (checkName != null)
                    return -1;
            }
            t.Name = rq.Name;
            t.Description = rq.Description;
            _context.Tags.Update(t);
            await _context.SaveChangesAsync();
            return t.Id;
        }
        public async Task<List<SongModel>> GetSongFromTag(int Id, PagingRequest pg)
        {
            var ListId = await _context.Song_Tags.Where(x => x.TagId == Id).Select(x => x.SongId)
                .Skip((pg.Index - 1) * pg.PageSize).Take(pg.PageSize)
                .ToListAsync();
            return await _manageSongService.GetSongByListId(ListId);
        }
    }
}
