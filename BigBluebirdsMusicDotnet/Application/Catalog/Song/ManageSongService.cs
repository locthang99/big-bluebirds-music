﻿using Application.Common;
using Data.EF;
using Models.Catalog.Song;
using Models.Catalog.Song.SongRequest;
using Models.Page;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Data.Entities;
using Application.Catalog.SongType;
using Microsoft.AspNetCore.Http;
using System.Net.Http.Headers;
using System.IO;
using Models.Catalog.Comment;
using Models.Catalog.Owner;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Application.AuthorData;
using Models.System.User.UserRequest;
using Application.Catalog.History;
using Models.Catalog.Tag;
using Models.Catalog.SongType;
using Abp.Domain.Repositories;
using Models.Enum;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Identity;
using Application.Catalog.PlayList;
using Application.Base;
using Models.Catalog.History.Request;

namespace Application.Catalog.Song
{
    public class ManageSongService : IManageSongService
    {
        private readonly BigBluebirdsDbContext _context;
        private readonly IApplicationServiceBase _applicationServiceBase;
        private readonly IStorageService _storageService;
        private readonly IAuthorizationService _authorizationService;
        private readonly IConfiguration _config;
        private readonly UserManager<Data.Entities.User> _userManager;
        private readonly IManageHistoryService _manageHistoryService;
        //private readonly IManageSongTypeService _manageSongTypeService;
        public ManageSongService(BigBluebirdsDbContext context,
            IApplicationServiceBase applicationServiceBase,
            IStorageService storageService,
            IAuthorizationService authorizationService,
            IManageHistoryService manageHistoryService,
            IConfiguration config,
            UserManager<Data.Entities.User> userManager)
        {
            _context = context;
            _applicationServiceBase = applicationServiceBase;
            _storageService = storageService;
            _authorizationService = authorizationService;
            _config = config;
            _userManager = userManager;
            _manageHistoryService = manageHistoryService;
            //_manageHistoryService = manageHistoryService;
            //_manageSongTypeService = manageSongTypeService;
        }

        #region CRUD Song
        private SongModel MapSong(Data.Entities.Song song)
        {
            if (song == null)
                return null;
            var data = new SongModel()
            {
                Id = song.Id,
                Name = song.Name,
                Description = song.Description,
                DateCreate = song.DateCreate,
                TotalListen = song.TotalListen,
                TotalLike = song.TotalLike,
                TotalCmt = song.TotalCmt,
                TotalDownload = song.TotalDownload,
                Lyric = song.Lyric,
                Duration = song.Duration,
                Types = _context.Song_SongTypes.Where(z => z.SongId == song.Id).Select(c => new SongTypeModel() { Id = c.SongTypeId, Name = c.SongType.Name }).ToList(),
                Owners = _context.Song_Owers.Where(z => z.SongId == song.Id).Select(c => new OwnerModel()
                {
                    OwnerId = c.OwnerId,
                    NameOwner = c.Owner.FirstName + " " + c.Owner.LastName,
                    Thumbnail = c.Owner.Thumbnail
                }).ToList(),
                Tags = _context.Song_Tags.Where(z => z.SongId == song.Id).Select(c => new TagModel()
                {
                    Id = c.TagId,
                    Name = c.Tag.Name
                }).ToList()
            };
            if (!song.Thumbnail.Contains("http") && song.Thumbnail != "")
                data.Thumbnail = _config["File:Image"] + song.Thumbnail;
            else
                data.Thumbnail = song.Thumbnail;
            if (!song.FileMusic.Contains("http") && song.FileMusic != "")
                data.FileMusic = _config["File:Music"] + song.FileMusic;
            else
                data.FileMusic = song.FileMusic;
            return data;
        }
        public async Task<List<SongModel>> GetAllSong(PagingRequest pg)
        {
            var data = await _context.Songs.Where(s=>s.IsPublic).Skip((pg.Index - 1) * pg.PageSize).Take(pg.PageSize).ToListAsync();
            var rs = data.Select(x => MapSong(x)).ToList();
            return rs;
        }
        public async Task<SongModel> GetSongById(int Id)
        {
            var data = await _context.Songs.FindAsync(Id);
            if (data == null)
                return null;
            else
                return MapSong(data);
        }
        public async Task<List<SongModel>> GetSongByName(string Name, PagingRequest pg)
        {
            var data = await _context.Songs.Where(s => (Name.Contains(s.Name) || s.Name.Contains(Name))&& s.IsPublic)
                .Skip((pg.Index - 1) * pg.PageSize).Take(pg.PageSize)
                .ToListAsync();
            var rs = data.Select(x => MapSong(x)).ToList();
            return rs;
        }

        public async Task<List<SongModel>> GetPrivateSong(PagingRequest pg)
        {
            var userID = _applicationServiceBase.GetCurrentUserId();
            if (userID == null)
                return new List<SongModel>();
            var listSongID = await _context.Song_Owers.Where(so => so.OwnerId.ToString() == userID).Select(so=>so.SongId).ToListAsync();
            var data = await _context.Songs.Where(s=>listSongID.Contains(s.Id)).Skip((pg.Index - 1) * pg.PageSize).Take(pg.PageSize).ToListAsync();
            var rs = data.Select(x => MapSong(x)).ToList();
            return rs;
        }
        public async Task<int> SongCreate(SongCreateRequest rq)
        {
            if (!_applicationServiceBase.CheckAuthentication())
            {
                return -401;
            }
            var s = new Data.Entities.Song()
            {
                Name = rq.Name,
                DateCreate = DateTime.Now,
                Description = rq.Description,
                Lyric = rq.Lyric,
                Duration = rq.Duration,
                Thumbnail = await _storageService.SaveFile(rq.Thumbnail, 0),
                FileMusic = await _storageService.SaveFile(rq.FileMusic, 1)
            };
            //Add Image
            //s.FileImages = new List<FileImage>();
            //if (rq.Thumbnail != null)
            //{
            //    s.FileImages.Add(new FileImage()
            //    {
            //        Description = "Thumbnail",
            //        Type = rq.Thumbnail.ContentType,
            //        FileSize = rq.Thumbnail.Length,
            //        Path = await _storageService.SaveFile(rq.Thumbnail, 0),
            //    });
            //}

            //if (rq.FileMusic != null)
            //{
            //    s.FileMusic = new FileMusic()
            //    {
            //        Description = "Audio",
            //        Type = rq.FileMusic.ContentType,
            //        FileSize = rq.FileMusic.Length,
            //        Path = await _storageService.SaveFile(rq.FileMusic, 1)
            //    };
            //}
            //Add Owner
            s.Song_Owners = new List<Song_Owner>();
            Song_Owner so = new Song_Owner()
            {
                SongId = s.Id,
                OwnerId = new Guid(_applicationServiceBase.GetCurrentUserId())
            };
            s.Song_Owners.Add(so);
            _context.Songs.Add(s);

            //Add type
            if (rq.SongTypes != null)
            {
                s.Song_SongTypes = new List<Song_SongType>();
                foreach (int SongTypeId in rq.SongTypes)
                    s.Song_SongTypes.Add(new Song_SongType() { SongId = s.Id, SongTypeId = SongTypeId });
            }
            //Add tag
            if (rq.Tags != null)
            {
                s.Song_Tags = new List<Song_Tag>();
                foreach (int TagId in rq.Tags)
                    s.Song_Tags.Add(new Song_Tag() { SongId = s.Id, TagId = TagId });
            }
            await _context.SaveChangesAsync();
            return s.Id;

        }
        public async Task<int> SongDelete( int Id)
        {
            if (!_applicationServiceBase.CheckAuthentication())
            {
                return -401;
            }
            var song = await _context.Songs.FindAsync(Id);
            var SongModel = await GetSongById(Id);
            if (song == null || SongModel == null)
            {
                return -404;
            }
            if (!(await _authorizationService.AuthorizeAsync(_applicationServiceBase.GetClaimsPrincipalUser(), SongModel, Operations.Delete)).Succeeded)
                return -403;

            _context.Songs.Remove(song);
            return await _context.SaveChangesAsync();
        }
        public async Task<int> SongUpdate( int Id, SongUpdateRequest rq)
        {
            if (!_applicationServiceBase.CheckAuthentication())
            {
                return -401;
            }
            var song = await _context.Songs.FindAsync(Id);
            var SongModel = await GetSongById(Id);
            if (song == null || SongModel == null)
            {
                return -404;
            }
            if (!(await _authorizationService.AuthorizeAsync(_applicationServiceBase.GetClaimsPrincipalUser(), SongModel, Operations.Update)).Succeeded)
                return -403;
            if (rq.Name != null)
                song.Name = rq.Name;
            if (rq.Description != null)
                song.Description = rq.Description;
            if (rq.Duration != 0)
                song.Duration = rq.Duration;
            if (rq.Lyric != null)
                song.Lyric = rq.Lyric;
            //Add Image      
            if (rq.Thumbnail != null)
            {
                song.Thumbnail = await _storageService.SaveFile(rq.Thumbnail, 0);
                //var ThumnailUpdate = _context.FileImages.Where(x => x.IdSong == Id && x.Description == "Thumbnail").FirstOrDefault();
                //if (ThumnailUpdate == null)
                //{
                //    ThumnailUpdate = new FileImage();
                //    ThumnailUpdate.Description = "Thumbail";
                //    song.FileImages = new List<FileImage>();
                //    song.FileImages.Add(ThumnailUpdate);
                //}

                //ThumnailUpdate.Type = ImageType.SYSTEM.ToString();
                //ThumnailUpdate.FileSize = rq.Thumbnail.Length;
                //ThumnailUpdate.Path = await _storageService.SaveFile(rq.Thumbnail, 0);
                //_context.Update(ThumnailUpdate);
            }

            if (rq.FileMusic != null)
            {
                song.FileMusic = await _storageService.SaveFile(rq.FileMusic, 1);
                //var AudioUpdate = _context.FileMusics.Where(x => x.IdSong == Id && x.Description == "Audio").FirstOrDefault();
                //if (AudioUpdate == null)
                //{
                //    //AudioUpdate = new FileMusic();
                //    //AudioUpdate.Description = "Audio";
                //    //_context.FileMusics.Add(AudioUpdate);
                //}

                //AudioUpdate.Type = rq.FileMusic.ContentType;
                //AudioUpdate.FileSize = rq.FileMusic.Length;
                //AudioUpdate.Path = await _storageService.SaveFile(rq.FileMusic, 1);
                //_context.Update(AudioUpdate);

            }

            //Update type
            var ListSongTypeId = _context.Song_SongTypes.Where(x => x.SongId == Id).Select(x => x.SongTypeId).ToList();
            if (rq.SongTypes == null)
                rq.SongTypes = new List<int>();

            foreach (int SongTypeId in rq.SongTypes)
            {
                if (!ListSongTypeId.Contains(SongTypeId))
                    _context.Song_SongTypes.Add(new Song_SongType() { SongId = song.Id, SongTypeId = SongTypeId });
            }
            foreach (int SongTypeId in ListSongTypeId)
            {
                if (!rq.SongTypes.Contains(SongTypeId))
                {
                    var TypeDelete = await _context.Song_SongTypes.FindAsync(Id, SongTypeId);
                    _context.Song_SongTypes.Remove(TypeDelete);
                }

            }
            //Update tag
            var ListTagId = _context.Song_Tags.Where(x => x.SongId == Id).Select(x => x.TagId).ToList();
            if (rq.Tags == null)
                rq.Tags = new List<int>();
            foreach (int TagId in rq.Tags)
            {
                if (!ListSongTypeId.Contains(TagId))
                    _context.Song_Tags.Add(new Song_Tag() { SongId = song.Id, TagId = TagId });
            }
            foreach (int TagId in ListSongTypeId)
            {
                if (!rq.Tags.Contains(TagId))
                {
                    var TagDelete = await _context.Song_Tags.FindAsync(Id, TagId);
                    _context.Song_Tags.Remove(TagDelete);
                }
            }

            _context.Songs.Update(song);
            return await _context.SaveChangesAsync();
        }
        public async Task<List<SongModel>> GetSongByListId(List<int> ListId)
        {

            var data = await _context.Songs
                .Where(x => ListId.Contains(x.Id)).ToListAsync();
            var rs = data.Select(x => MapSong(x)).ToList();
            return rs;
        }
        #endregion
        public async Task<List<CommentModel>> GetAllComment(int SongId, PagingRequest pg)
        {
            return await _context.User_Cmt_Songs.Where(x => x.SongId == SongId).Select(x => new CommentModel
            {
                UserId = x.UserId,
                FirstName = x.User.FirstName,
                LastName = x.User.LastName,
                Content = x.Content,
                DateTime = x.DateCreate
            }).Skip((pg.Index - 1) * pg.PageSize).Take(pg.PageSize)
                .ToListAsync();

        }
        public async Task<int> LikeSong( int IdSong)
        {
            if (!_applicationServiceBase.CheckAuthentication())
                return -401;
            var Id = _applicationServiceBase.GetCurrentUserId();
            Guid UserId = new Guid();
            if (Id != null)
            {
                try
                {
                    UserId = new Guid(Id);
                }
                catch
                {
                    return 0;
                }
            }
            var UserLike = await _context.Users.FindAsync(UserId);
            var Song = await _context.Songs.FindAsync(IdSong);
            if (Song == null)
                return -404;
            var uls = new User_Like_Song()
            {
                UserId = UserId,
                User = UserLike,
                SongId = IdSong,
                Song = Song,
                DateCreate = DateTime.Now
            };
            var rs = await _context.User_Like_Songs.FindAsync(UserId, IdSong);
            if (rs != null)
                return -1;//Bi trung -- bai hat da duoc like
            _context.User_Like_Songs.Add(uls);
            Song.TotalLike++;
            _context.Songs.Update(Song);
            return await _context.SaveChangesAsync();
        }
        public async Task<int> CheckLikeSong(int IdSong)
        {
            if (!_applicationServiceBase.CheckAuthentication())
                return -401;
            var Id = _applicationServiceBase.GetCurrentUserId();
            Guid UserId = new Guid();
            if (Id != null)
            {
                try
                {
                    UserId = new Guid(Id);
                }
                catch
                {
                    return 0;
                }
            }
            var UserLike = await _context.Users.FindAsync(UserId);
            var Song = await _context.Songs.FindAsync(IdSong);
            if (Song == null)
                return -404;
            var rs = await _context.User_Like_Songs.FindAsync(UserId, IdSong);
            if (rs != null)
                return 1;
            else
                return 2;
            
        }
        public async Task<int> UnLikeSong( int IdSong)
        {
            if (!_applicationServiceBase.CheckAuthentication())
                return -401;
            var Id = _applicationServiceBase.GetCurrentUserId();
            Guid UserId = new Guid();
            if (Id != null)
            {
                try
                {
                    UserId = new Guid(Id);
                }
                catch
                {
                    return 0;
                }
            }
            var UserLike = await _context.Users.FindAsync(UserId);
            var Song = await _context.Songs.FindAsync(IdSong);
            if (Song == null)
                return -404;
            var rs = await _context.User_Like_Songs.FindAsync(UserId, IdSong);
            if (rs == null)
                return -1;//Bi trung -- bai hat da duoc like
            _context.User_Like_Songs.Remove(rs);
            Song.TotalLike--;
            _context.Songs.Update(Song);
            return await _context.SaveChangesAsync();
        }
        public async Task<int> CmtSong( UserCmtRequest rq)
        {
            if (!_applicationServiceBase.CheckAuthentication())
                return -401;
            var Id = _applicationServiceBase.GetCurrentUserId();
            Guid UserId = new Guid();
            if (Id != null)
            {
                try
                {
                    UserId = new Guid(Id);
                }
                catch
                {
                    return 0;
                }
            }
            var UserCmt = await _context.Users.FindAsync(UserId);
            var Song = await _context.Songs.FindAsync(rq.ObjId);
            if (Song == null)
                return -404;
            var ucs = new User_Cmt_Song()
            {
                UserId = UserId,
                User = UserCmt,
                SongId = rq.ObjId,
                Song = Song,
                DateCreate = DateTime.Now,
                Content = rq.Content
            };
            //var rs = await _context.User_Cmt_Songs.FindAsync(rq.UserId, rq.ObjId);
            //if (rs != null)
            //    return -1;//Bi trung -- bai hat da duoc cmt
            _context.User_Cmt_Songs.Add(ucs);
            Song.TotalCmt++;
            _context.Songs.Update(Song);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> Listen(int Id)
        {
            var song = await _context.Songs.FindAsync(Id);
            if (song == null)
                return -404;
            await _manageHistoryService.RecordHistory(new CreateHistoryRequest()
            {
                ActionType = Models.Enum.ActionType.LISTEN,
                ObjectType = Models.Enum.ObjectType.SONG,
                ObjectId = Id.ToString()
            });
            song.TotalListen++;
            _context.Songs.Update(song);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> Download(int Id)
        {
            var song = await _context.Songs.FindAsync(Id);
            if (song == null)
                return -404;

            song.TotalDownload++;
            _context.Songs.Update(song);
            return await _context.SaveChangesAsync();
        }

        static Dictionary<string,Guid> listSinger = new Dictionary<string, Guid>();
        public async Task<int> SongClone(List<SongCloneRequest> rq,int PlaylistId)
        {
            List<Data.Entities.Song> data = new List<Data.Entities.Song>();
            List<Guid> listIdUser = new List<Guid>();
            var hasher = new PasswordHasher<User>();
            //foreach(var item in rq)
            //{
            //    var checkSong = _context.Songs.FirstOrDefault(check => check.Description == item.Description);
            //}
            rq.ForEach(x =>
            {
                List<Guid> listIdUser = new List<Guid>();
                var checkSong=_context.Songs.FirstOrDefault(check => check.FileMusic == x.FileMusic);
                if (checkSong == null)
                {
                    var s = new Data.Entities.Song()
                    {
                        Name = x.Name,
                        DateCreate = DateTime.Now,
                        Description = x.Description,
                        Lyric = x.Lyric,
                        Duration = x.Duration,
                        Thumbnail = x.Thumbnail,
                        FileMusic = x.FileMusic,
                        TotalListen=x.TotalListen,
                    };
                    if (x.singerClones.Count > 0)
                    {
                        x.singerClones.ForEach(sc =>
                        {
                            var user = new Data.Entities.User()
                            {
                                Id = new Guid(),
                                FirstName = "",
                                LastName = sc.Name,
                                UserName = sc.Username,
                                Email = sc.Username+"@gmail.com",
                                EmailConfirmed = true,
                                PasswordHash = hasher.HashPassword(null, "Abcd1234$"),
                                SecurityStamp = string.Empty,
                                AccountType = AccountType.SYSTEM.ToString(),
                                Thumbnail = sc.Thumbnail,
                            };
                            //var checkuser = _context.Users.FirstOrDefault(u => u.UserName == sc.Username);
                            //var checkuser = await _userManager.FindByNameAsync(user.UserName);
                            var checkuser = listSinger.ContainsKey(user.UserName);
                            if (!checkuser)
                            {
                                 _context.Users.Add(user);
                                
                                //var result = _context.SaveChanges();
                                //if (result > 0)
                                listIdUser.Add(user.Id);
                                listSinger.Add(user.UserName,user.Id);
                            }
                            else
                            {
                                Console.WriteLine("Duplicate");
                                listIdUser.Add(listSinger[user.UserName]);
                                
                            }
                        });

                    }
                    s.Song_Owners = new List<Song_Owner>();
                    if (listIdUser.Count > 0)
                    {
                        listIdUser.ForEach(x =>
                        {
                            Song_Owner so = new Song_Owner()
                            {
                                SongId = s.Id,
                                OwnerId = x,
                            };
                            s.Song_Owners.Add(so);
                        });
                    }

                    data.Add(s);
                }
                else
                {
                    _context.Song_PlayLists.Add(new Song_PlayList() { SongId = checkSong.Id, PlayListId = PlaylistId });
                }
            });

             _context.Songs.AddRange(data);
            await _context.SaveChangesAsync();
            data.ForEach(s =>
            {
               // Console.WriteLine("idddd: " + s.Id);
                _context.Song_PlayLists.Add(new Song_PlayList() { SongId = s.Id, PlayListId = PlaylistId });
            });
            var pl = _context.PlayLists.Find(PlaylistId);
            pl.TotalSong = rq.Count;
            _context.Update(pl);
            return await _context.SaveChangesAsync();
        }

        
        public async Task<List<SongModel>> GetTopSong(string Type)
        {
            if (Type == null)
            {
                return null;
            }
            List<Data.Entities.Song> dataTopSong =new List<Data.Entities.Song>();
            switch (Type)
            {
                
                case "LISTEN":
                    dataTopSong = (from s in _context.Songs
                                orderby s.TotalListen descending
                                select s ).Take(10).ToList();
                    break;
                case "DOWNLOAD":
                    dataTopSong = (from s in _context.Songs
                            orderby s.TotalDownload descending
                            select s).Take(10).ToList();
                    break;
                case "COMMENT":
                    dataTopSong = (from s in _context.Songs 
                            orderby s.TotalCmt descending
                            select s).Take(10).ToList();
                    break;
                case "LIKE":
                    dataTopSong = (from s in _context.Songs
                            orderby s.TotalLike descending
                            select s).Take(10).ToList();
                    break;
            }
            var rs = dataTopSong.Select(x => MapSong(x)).ToList();
            return rs;
        }
    }
}