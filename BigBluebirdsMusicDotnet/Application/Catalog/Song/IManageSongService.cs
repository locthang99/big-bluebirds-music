﻿using Models.Catalog.Comment;
using Models.Catalog.Song;
using Models.Catalog.Song.SongRequest;
using Models.Page;
using Models.System.User.UserRequest;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Application.Catalog.Song
{
    public interface IManageSongService
    {
        Task<SongModel> GetSongById(int Id);
        Task<List<SongModel>> GetSongByListId(List<int> ListId);
        Task<List<SongModel>> GetSongByName(string Name, PagingRequest pg);
        Task<List<SongModel>> GetAllSong(PagingRequest pg);
        Task<List<SongModel>> GetPrivateSong(PagingRequest pg);
        Task<int> SongCreate(SongCreateRequest rq);
        Task<int> SongDelete( int Id);
        Task<int> SongUpdate(int Id, SongUpdateRequest rq);
        Task<List<CommentModel>> GetAllComment(int SongId, PagingRequest pg);
        Task<int> LikeSong(int Id);
        Task<int> CheckLikeSong(int Id);
        Task<int> UnLikeSong( int Id);
        Task<int> CmtSong( UserCmtRequest rq );
        Task<int> Listen(int Id);
        Task<int> Download(int Id);
        Task<int> SongClone(List<SongCloneRequest> rq,int PlaylistId);
        Task<List<SongModel>> GetTopSong(string Type);

    }
}
