﻿using Data.EF;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Models.Catalog.Song;
using Application.Catalog.Song;
using Models.Page;
using Application.Cache;
using Data.Entities;
using Application.Catalog.PlayList;
using Models.Catalog.PlayList;

namespace Application.Catalog.Recommendation
{
    public class RecommendService : IRecommendService
    {
        private readonly BigBluebirdsDbContext _context;
        private readonly IManageSongService _manageSongService;
        private readonly IManagePlayListService _managePlaylistService;     
        private readonly ICacheService _cacheService;
        public RecommendService(BigBluebirdsDbContext context,IManageSongService manageSongService, ICacheService cacheService, IManagePlayListService managePlaylistService)
        {
            _context = context;
            _manageSongService = manageSongService;
            _cacheService = cacheService;
            _managePlaylistService = managePlaylistService;
        }

        public async Task<List<string>> GetListRawSimilarUsersID(Guid userID, int NumberUser, string ActionType, string ObjectType)
        {
            var query = from h in _context.Histories
                        let listObjId = (from h2 in _context.Histories
                                          where h2.UserId == userID
                                          && h2.ActionType == ActionType && h2.ObjectType == ObjectType
                                          orderby h2.Id descending
                                          select h2.ObjectId).Take(10)
                        where h.ObjectType == ObjectType && h.ActionType == ActionType
                        && listObjId.Contains(h.ObjectId) && h.UserId != userID
                        select h.UserId.ToString();
            return await query.Distinct().Take(NumberUser).ToListAsync();
        }

        public async Task<List<int>> GetListLikeObjectsOfAUser(Guid userID,int NumberObj, string ActionType, string ObjectType)
        {
            var query = from h in _context.Histories
                        where h.ActionType == ActionType && h.ObjectType == ObjectType && h.UserId == userID
                        orderby h.Id descending
                        select Int32.Parse(h.ObjectId);
            return await query.Distinct().Take(NumberObj).ToListAsync();

        }

        public async Task<List<string>> GetListUsersLikeObject(int objId, string ActionType, string ObjectType)
        {
            var query = from h in _context.Histories
                        where h.ActionType == ActionType && h.ObjectType == ObjectType && h.ObjectId == objId.ToString()
                        select h.UserId.ToString();
            return await query.Distinct().ToListAsync();
        }
        public async Task<List<int>> GetListRawSimilarObjectsID(int objId,string ActionType, string ObjectType)
        {
            var query = from s in _context.Songs
                        let listSong = from h in _context.Histories
                                       where h.ActionType == ActionType && h.ObjectType == ObjectType
                                       select h.ObjectId
                        where s.Id != objId && listSong.Contains(s.Id.ToString())
                        select s.Id;
            return await query.ToListAsync();

        }

        public async Task<List<int>> UserBasedCollaborativeFilter(Guid userID, int NumberUser,int NumberObj, string ActionType, string ObjectType,PagingRequest pg)
        {
            
            List<string> lstSimilarUsers = await GetListRawSimilarUsersID(userID,NumberUser,ActionType,ObjectType);

            //Lay danh sách các bài hát được like bởi user
            List<int> lstUserLikeObjs =await GetListLikeObjectsOfAUser(userID,NumberObj, ActionType, ObjectType);
            if (lstUserLikeObjs.Count == 0)
                return null;

            // Chon ra cac user tuong tu nhat (Khoang 10 user)
            // Dung thuat toan jaccard similarity
            Dictionary<string, double> lstUserAndPoint = new Dictionary<string, double>();
            Dictionary<string, List<int>> lstUserAndLikeObjs = new Dictionary<string, List<int>>();
            int numberUserOfList = 10; // Lay 10 user tuong tu nhat thoi
            foreach (string id2 in lstSimilarUsers)
            {
                Guid uuid2 = new Guid(id2);
                // Lay danh sach cac bai hat duoc like cua moi user
                List<int> lstLikeObjs2 = await GetListLikeObjectsOfAUser(uuid2, NumberObj, ActionType, ObjectType);
                if (lstLikeObjs2.Count == 0)
                    continue;
                // Tinh point
                double point = lstUserLikeObjs.Intersect(lstLikeObjs2).Count() * 1.0 / lstUserLikeObjs.Union(lstLikeObjs2).Count() * 1.0;
                if (lstUserAndPoint.Count < numberUserOfList)
                {
                    lstUserAndPoint.Add(id2, point);
                    lstUserAndLikeObjs.Add(id2, lstLikeObjs2);
                }
                else
                {
                    // Thêm vào danh sách user tương tự nhất (Loại mấy user ít tương tự ra)
                    string minKey = GetMinKeyDictionary(lstUserAndPoint);
                    if (point > lstUserAndPoint[minKey])
                    {
                        lstUserAndPoint.Remove(minKey);
                        lstUserAndPoint.Add(id2, point);
                        lstUserAndLikeObjs.Remove(minKey);
                        lstUserAndLikeObjs.Add(id2, lstLikeObjs2);
                    }
                }
            }

            // Từ danh sách các user tương tự nhất, tổ chức vote
            // Bài toán tìm phần tử được lặp lại nhiều nhất trong các mảng
            // Cách tối ưu nhất là lặp lặp từng phần tử, đếm và lưu nó vào dictionary (độ phức tạp O(n))
            // Key là id của bài hát, Value là tần suất xuất hiện
            Dictionary<int, int> frequentDic = new Dictionary<int, int>();
            foreach (var userAndLikeSongs in lstUserAndLikeObjs)
            {
                foreach (var song in userAndLikeSongs.Value)
                {
                    if (!frequentDic.ContainsKey(song))
                    {
                        frequentDic.Add(song, 1);
                    }
                    else
                    {
                        frequentDic[song]++;
                    }
                }
            }
            //Lấy bài hát được xuất hiện nhiều nhất
            //int recommendSongID = GetMaxKeyDictionary(frequentDic);
            var listSongIdRecommend =frequentDic.OrderByDescending(x => x.Value).Select(x => x.Key)
                .Skip((pg.Index - 1) * pg.PageSize)
                .Take(pg.PageSize).ToList(); ;
            var cacheSongId = "";
            foreach (var entry in listSongIdRecommend)
                cacheSongId += entry + ",";
            var newCahe = new CacheData()
            {
                Data = cacheSongId.Substring(0, cacheSongId.Length - 1),
                OwnerId = userID.ToString(),
                Type = "Recommend"+ObjectType+"IdUserBase"
            };
            await _cacheService.AddCache(newCahe);
            return listSongIdRecommend;
            //return await _manageSongService.GetSongByListId(listSongIdRecommend);

        }

        public async Task<List<int>> ItemBasedCollaborativeFilter(int objId,string ActionType,string ObjectType,PagingRequest pg)
        {
           
            List<int> lstRawSimilarSongs = await GetListRawSimilarObjectsID(objId,ActionType,ObjectType);

            //Lấy danh sách các user like bài hát này
            List<string> lstUserLikeObj = await GetListUsersLikeObject(objId, ActionType, ObjectType);

            // Loop qua từng bài hát trên danh sách
            // Chọn ra các bài hát tương tự nhất (Có độ similarity cao nhất)
            // Dùng thuật toán jaccard similarity
            //int recommendSong = -1;
            //double maxPoint = 0;
            Dictionary<int, double> frequentDic = new Dictionary<int, double>();
            foreach (int obj in lstRawSimilarSongs)
            {
                List<string> lstUserLikeObj2 = await GetListUsersLikeObject(objId, ActionType, ObjectType);
                double point = lstUserLikeObj.Intersect(lstUserLikeObj2).Count() * 1.0 / lstUserLikeObj.Union(lstUserLikeObj2).Count() * 1.0;
                //if (point > maxPoint)
                //{
                //    maxPoint = point;
                //    recommendSong = song2;
                //}
                frequentDic.Add(obj, point);
            }
            var listobjIdRecommend = frequentDic.OrderByDescending(x => x.Value).Select(x => x.Key)
                .Skip((pg.Index - 1) * pg.PageSize)
                .Take(pg.PageSize).ToList();
            var cacheobjId = "";
            foreach (var entry in listobjIdRecommend)
                cacheobjId += entry + ",";
            var newCahe = new CacheData()
            {
                Data = cacheobjId.Substring(0,cacheobjId.Length-1),
                OwnerId = objId.ToString(),
                Type = "Recommend"+ObjectType+"IdItemBase"
            };
            await _cacheService.AddCache(newCahe);
            //Console.WriteLine("[{0} {1}]", entry, frequentDic[entry]);
            return listobjIdRecommend;
        }



        public async Task<List<SongModel>> UserBasedCollaborativeFilterSong(Guid userID, int NumberUser, int NumberObj, string ActionType, string ObjectType, PagingRequest pg)
        {
            var cachedData = await _cacheService.GetCache(userID.ToString(), "RecommendSONGIdUserBase");
            if (cachedData != null)
            {
                var listSongId = cachedData.Split(',').Select(x => Int32.Parse(x)).ToList();
                return await _manageSongService.GetSongByListId(listSongId);
            }
            else
            {
                var listId = await UserBasedCollaborativeFilter(userID, NumberUser, NumberObj, ActionType, ObjectType, pg);
                return await _manageSongService.GetSongByListId(listId);
            }
        }
        public async Task<List<PlayListModel>> UserBasedCollaborativeFilterPlaylist(Guid userID, int NumberUser, int NumberObj, string ActionType, string ObjectType, PagingRequest pg)
        {
            var cachedData = await _cacheService.GetCache(userID.ToString(), "RecommendPLAYLISTIdUserBase");
            if (cachedData != null)
            {
                var listPlaylistId = cachedData.Split(',').Select(x => Int32.Parse(x)).ToList();
                return await _managePlaylistService.GetPlaylistByListId(listPlaylistId);
            }
            else
            {
                var listPlaylistId = await UserBasedCollaborativeFilter(userID, NumberUser, NumberObj, ActionType, ObjectType, pg);
                return await _managePlaylistService.GetPlaylistByListId(listPlaylistId);
            }
        }


        public async Task<List<SongModel>> ItemBasedCollaborativeFilterSong(int ObjId, string ActionType, string ObjectType, PagingRequest pg)
        {
            var cachedData = await _cacheService.GetCache(ObjId.ToString(), "RecommendSONGIdItemBase");
            if (cachedData != null)
            {
                var listSongId = cachedData.Split(',').Select(x => Int32.Parse(x)).ToList();
                return await _manageSongService.GetSongByListId(listSongId);
            }
            else
            {
                var listId = await ItemBasedCollaborativeFilter(ObjId, ActionType, ObjectType, pg);
                return await _manageSongService.GetSongByListId(listId);
            }
        }
        public async Task<List<PlayListModel>> ItemBasedCollaborativeFilterPlaylist(int ObjId, string ActionType, string ObjectType, PagingRequest pg)
        {
            var cachedData = await _cacheService.GetCache(ObjId.ToString(), "RecommendPLAYLISTIdItemBase");
            if (cachedData != null)
            {
                var listPlaylistId = cachedData.Split(',').Select(x => Int32.Parse(x)).ToList();
                return await _managePlaylistService.GetPlaylistByListId(listPlaylistId);
            }
            else
            {
                var listPlaylistId = await ItemBasedCollaborativeFilter(ObjId, ActionType, ObjectType, pg);
                return await _managePlaylistService.GetPlaylistByListId(listPlaylistId);
            }
        }


        public static string GetMinKeyDictionary(Dictionary<string, double> lstUser)
        {
            string minKey = null;
            double minValue = 10;
            foreach (var data in lstUser)
            {
                if (data.Value < minValue)
                {
                    minValue = data.Value;
                    minKey = data.Key;
                }
            }
            return minKey;
        }

        public static int GetMaxKeyDictionary(Dictionary<int, int> frequentDic)
        {
            int maxKey = -1;
            int maxValue = 0;
            foreach (var data in frequentDic)
            {
                if (data.Value >= maxValue)
                {
                    maxValue = data.Value;
                    maxKey = data.Key;
                }
            }
            return maxKey;
        }


    }
}
