﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Application.Base
{
    public class ApplicationServiceBase : IApplicationServiceBase
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public ApplicationServiceBase(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }       
        public string GetCurrentUserId()
        {
            if (_httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier) != null)
                return _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            else
                return null;
        }
        public bool CheckAuthentication()
        {
            if (_httpContextAccessor.HttpContext.User.Identity.IsAuthenticated)
                return true;
            else
                return false;
        }

        public bool CheckRole(string RoleName)
        {
            if (_httpContextAccessor.HttpContext.User.IsInRole(RoleName))
                return true;
            else
                return false;
        }

        public ClaimsPrincipal GetClaimsPrincipalUser()
        {
            return _httpContextAccessor.HttpContext.User;
        }
    }
}
