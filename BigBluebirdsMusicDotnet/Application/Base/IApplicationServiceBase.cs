﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Application.Base
{
    public interface IApplicationServiceBase
    {
        string GetCurrentUserId();
        bool CheckAuthentication();
        bool CheckRole(string RoleName);
        ClaimsPrincipal GetClaimsPrincipalUser();

    }
}
