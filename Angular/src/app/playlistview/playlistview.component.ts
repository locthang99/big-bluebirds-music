import { Component, OnInit } from '@angular/core';
import { Song, PlayList } from 'share/model';
import { PlayListsServiceProxy } from 'share/services'
import { PlayerService } from '../services/player.service'
import { Router, ActivatedRoute, Params } from "@angular/router";
@Component({
  selector: 'app-playlistview',
  templateUrl: './playlistview.component.html',
  styleUrls: ['./playlistview.component.css', '../../styles.css']
})
export class PlaylistviewComponent implements OnInit {
  id: number;
  index: number = 1;
  pagsize: number = 20;
  xemthem: boolean = false;
  playlist: PlayList;
  plIsLoaded: boolean = false;
  files: Array<any> = [
  ];
  constructor(
    public playlistservice: PlayListsServiceProxy, public player: PlayerService,
    public activatedRouteService: ActivatedRoute) {
    this.activatedRouteService.params.subscribe((data: Params) => {
      this.id = data['id'];
    });

    this.pagsize = 20;

  }
  ngOnInit(): void {
    this.load();
  }
  load() {
    this.playlistservice.byId(this.id).subscribe((data: any) => {
      this.playlist = data.data.data;
      this.plIsLoaded = true;
    });
    this.playlistservice.listSong(this.id, this.index, this.pagsize).subscribe((data: any) => {
      this.files = [];
      for (let index = 0; index < data['data']['data'].length; index++) {
        const element = data['data']['data'][index];
        let baihat = Song.fromJS(element);
        this.files.push(baihat);
      }
    });

  }
  seemore() {
    this.pagsize += 20;
    if (this.pagsize > 100)
      this.pagsize = 100;
    this.load();

  }
  play(file: any, index: any) {

    if (JSON.stringify(this.files[this.files.length - 1]) === JSON.stringify(this.player.files[this.player.files.length - 1])) {
      //Tức là 1 danh sách bằng nhau
      this.player.openFile(file, index);
    }
    else {

      this.player.setList(this.files);
      this.player.openFile(file, index);

    }
  }
}
