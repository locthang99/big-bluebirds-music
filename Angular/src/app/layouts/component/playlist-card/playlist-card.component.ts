import { Component, OnInit, Input } from '@angular/core';
import { PlayList } from 'share/model';
import { PlayListsServiceProxy } from 'share/services';

@Component({
  selector: 'app-playlist-card',
  templateUrl: './playlist-card.component.html',
  styleUrls: ['./playlist-card.component.css']
})
export class PlaylistCardComponent implements OnInit {
  @Input('id') id: number;

  playlist: PlayList;
  constructor(public playlistservice: PlayListsServiceProxy) {

  }

  ngOnInit(): void {
    this.playlistservice.byId(this.id).subscribe((data: any) => {
      this.playlist = data.data.data;
      console.log("Ảnh", this.playlist.name);

    });
  }

}
