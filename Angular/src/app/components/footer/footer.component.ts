import { Component, OnInit } from '@angular/core';
import { PlayerService } from '../../services/player.service'
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  files: Array<any> = [
  ];

  constructor(
    public player: PlayerService
  ) {
  }
  ngOnInit() {
  }
}
