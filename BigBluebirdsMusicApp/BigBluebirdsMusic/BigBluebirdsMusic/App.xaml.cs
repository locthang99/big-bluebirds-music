﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using BigBluebirdsMusic.Views;
using BigBluebirdsMusic.Services;
using System.Threading.Tasks;

namespace BigBluebirdsMusic
{
    public partial class App : Xamarin.Forms.Application
    {
        public App()
        {
            InitializeComponent();
            LocalDatabaseService db = new LocalDatabaseService();
            db.CreateDatabase();
            MainPage = new NavigationPage(new MainPage());
        }
        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
