﻿using BigBluebirdsMusic.Models;
using BigBluebirdsMusic.Services;
using BigBluebirdsMusic.ViewModels;
using Plugin.ContactService.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BigBluebirdsMusic.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfilePage : ContentPage
    {
        public ProfilePage()
        {
            InitializeComponent();
            user = User.GetInstance();
            vmProfile = ProfileViewModel.GetInstance();
            // vmProfileFriend = new ProfileViewModel(); 
            //lv_friend.BindingContext = vmProfileFriend;
            //lv_friend.SetBinding(ListView.ItemsSourceProperty, "friends");

            img_avatar.BindingContext = vmProfile;
            img_avatar.SetBinding(Image.SourceProperty, "avatar");
            this.BindingContext = vmProfile;
        }
        public static User user { get; set; }
        public ProfileViewModel vmProfile;
        // public ProfileViewModel vmProfileFriend;
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            user = User.GetInstance();
            if (!user.IsLoggedIn)
            {
                User.GetLocalInstanceInfo();
                user = User.GetInstance();
            }

            CheckLogin();
            await SetUserInforAsync();
        }
        public async void CheckLogin()
        {
            user = await DataService.CheckUserIsLoggedinAsync(user);
            User.SetInstance(user);
        }
        public void Logout()
        {
            user = User.LogoutLocalUser();
        }
        public async Task SetUserInforAsync()
        {
            user = User.GetInstance();
            if (user.IsLoggedIn)
            {
                //lb_name.Text = user.FirstName + " " + user.LastName;
                vmProfile.SetUserInfoCommand.Execute(user);
                lb_role.Text = user.Role!=null?user.Role:"VIP: 0";
                btn_edit.IsEnabled = true;
                btn_sync.IsEnabled = true;
                tbx_name.Text = user.FirstName + " " + user.LastName;
                tbx_email.Text = user.Email;
                tbx_phone.Text = user.PhoneNumber;
                tbx_dob.Date = user.DayOfBirdth;
                vmProfile.SetAvatar(user.Thumbnail);
                img_avatar.BindingContext = vmProfile;
                img_avatar.SetBinding(Image.SourceProperty, "avatar");


                //lv_friend.BindingContext = vmProfileFriend;
                //lv_friend.SetBinding(ListView.ItemsSourceProperty, "friends");
                await LoadFriendAsync();

            }
            else
            {
                //lb_name.Text = "Chưa đăng nhập";
                vmProfile.OfflineUserInfoCommand.Execute(null);
                btn_edit.IsEnabled = false;
                btn_sync.IsEnabled = false;
                tbx_name.Text = "";
                tbx_email.Text = "";
                tbx_phone.Text = "";
                tbx_dob.Date = new DateTime();
                //foreach (var user in vmProfile.friends)
                //    vmProfileFriend.RemoveFriend(user);
                //lv_friend.BindingContext = vmProfileFriend;
                //lv_friend.SetBinding(ListView.ItemsSourceProperty, "friends");
                vmProfile.ClearFriendCommand.Execute(null);

            }

        }
        public async Task LoadFriendAsync()
        {
            //lv_friend.IsRefreshing = true;
            vmProfile.LoadFriendAsync(lv_friend);
            //lv_friend.HeightRequest = 6 * 60 + 20;
            //lv_friend.IsRefreshing = false;
        }
        private void btn_login_Clicked(object sender, EventArgs e)
        {
            Logout();
            Navigation.PushModalAsync(new LoginPage());
        }

        private void btn_logout_Clicked(object sender, EventArgs e)
        {
            Logout();
            Navigation.PushModalAsync(new LoginPage());
        }

        private void btn_edit_Clicked(object sender, EventArgs e)
        {
            tbx_name.IsEnabled = true;
            if(tbx_email.Text=="")
                tbx_email.IsEnabled = true;
            tbx_phone.IsEnabled = true;
            tbx_dob.IsEnabled = true;
            btn_edit.IsVisible = false;
            btn_cancel.IsVisible = true;
            btn_save.IsVisible = true;
        }

        private async void btn_save_Clicked(object sender, EventArgs e)
        {
            tbx_name.IsEnabled = false;
            tbx_email.IsEnabled = false;
            tbx_phone.IsEnabled = false;
            tbx_dob.IsEnabled = false;
            btn_edit.IsVisible = true;
            btn_cancel.IsVisible = false;
            btn_save.IsVisible = false;
            string[] splName = tbx_name.Text.Trim().Split(' ');
            string lastname = splName[splName.Length - 1];
            string firstname = tbx_name.Text.Remove(
                tbx_name.Text.Trim().Length - lastname.Length -1, lastname.Length+1);
            User new_info = new User()
            {
                ID = user.ID,
                Role = user.Role,
                Thumbnail = user.Thumbnail,
                Token = user.Token,
                UserName = user.UserName,
                IsLoggedIn = user.IsLoggedIn,
                DayOfBirdth = tbx_dob.Date,
                Email = tbx_email.Text,
                PhoneNumber = tbx_phone.Text,
                FirstName = firstname,
                LastName = lastname
            };
            User res = await DataService.UpdateUserInfo(new_info);
            if (res == null)
            {
                string message = "Sửa thông tin thất bại"; // That bai
                Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Short).Show();

            }
            else
            {
                user = res;
                User.SetInstance(res);
                string message = "Sửa thông tin thành công"; // That bai
                Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Short).Show();

            }
        }

        private void btn_cancel_Clicked(object sender, EventArgs e)
        {
            tbx_name.IsEnabled = false;
            tbx_email.IsEnabled = false;
            tbx_phone.IsEnabled = false;
            tbx_dob.IsEnabled = false;
            btn_edit.IsVisible = true;
            btn_cancel.IsVisible = false;
            btn_save.IsVisible = false;
            SetUserInforAsync();
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            bool answer = await DisplayAlert("Xác nhận", "Quá trình đồng bộ có thể tốn nhiều thời gian, bạn có muốn tiếp tục?", "Vâng", "Lúc khác");
            if (!answer)
                return;
            try
            {
                lv_friend.IsRefreshing = true;
                // Device may request user permission to get contacts access.
                var contacts = await Plugin.ContactService.CrossContactService.Current.GetContactListAsync();
                foreach (Contact contact in contacts)
                {
                    DataService.SendFolowRequest(contact.Number);
                }
                LoadFriendAsync();
                lv_friend.IsRefreshing = false;
            }
            catch
            {
                string message = "Đồng bộ thất bại"; // That bai
                Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Short).Show();
                //Chua doi quyen
            }

        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            if (frame_Info.IsVisible)
            {
                frame_Info.IsVisible = false;
                frameFriend.IsVisible = true;
            }
            else
            {
                frame_Info.IsVisible = true;
                frameFriend.IsVisible = false;
            }
        }

        private void btn_return_Clicked(object sender, EventArgs e)
        {
            frame_Info.IsVisible = false;
            frameFriend.IsVisible = true;
        }

        private void RefreshView_Refreshing(object sender, EventArgs e)
        {
            LoadFriendAsync();
        }
    }
}