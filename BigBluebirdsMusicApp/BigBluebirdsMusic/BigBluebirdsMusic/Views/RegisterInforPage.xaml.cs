﻿using BigBluebirdsMusic.Services;
using Plugin.FilePicker.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BigBluebirdsMusic.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterInforPage : ContentPage
    {
        private string username;
        private string password;
        private bool isValidName;
        private bool isValidEmail;
        private bool isValidPhone;
        public RegisterInforPage(string username, string password)
        {
            InitializeComponent();
            this.username = username;
            this.password = password;

        }

        private void txt_name_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidateNameInput();
        }

        private void txt_email_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidateEmailInput();
        }

        private void txt_sdt_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidatePhoneInput();
        }

        private async void btn_register_Clicked(object sender, EventArgs e)
        {
            var res = await DataService.RegisterAsync(username, password, txt_name.Text,
                txt_email.Text, txt_sdt.Text, pk_birth.Date, image);

            if (res)
            {
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    "Đăng ký thành công!",
                    Android.Widget.ToastLength.Short).Show();
                await Navigation.PopModalAsync();
                await Navigation.PopModalAsync();
            }
            else
            {
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    "Đăng ký thất bại!",
                    Android.Widget.ToastLength.Short).Show();
            }

        }

        private void TapGestureRecognizer_Tapped_1(object sender, EventArgs e)
        {
            Navigation.PopModalAsync();
        }

        private void ValidateNameInput() 
        {
            Regex regex = new Regex(@"[1-9~!@#$%^&*()_+<>{}]");
            Match match = regex.Match(txt_name.Text);
            string error_mesage = "";
            if (txt_name.Text.Length < 5)
                error_mesage = "Họ tên tối thiểu 5 ký tự";
            if (match.Success)
            {
                error_mesage = "Họ tên chứa ký tự không hợp lệ!";
            }
            
            if (error_mesage != "")
            {
                lb_name_failed.Text = error_mesage;
                lb_name_failed.IsVisible = true;
                isValidName = false;
            }
            else
            {
                lb_name_failed.IsVisible = false;
                isValidName = true;
            }
                
        }
        private void ValidateEmailInput() 
        {
            string error_mesage = "";
            if (txt_email.Text.Length < 10)
                error_mesage = "Email tối thiểu 10 ký tự";
            if (txt_email.Text.IndexOf("@") <= 0)
            {
                error_mesage = "Email không hợp lệ!";
            }

            if (error_mesage != "")
            {
                lb_email_failed.Text = error_mesage;
                lb_email_failed.IsVisible = true;
                isValidEmail = false;
            }
            else
            {
                lb_email_failed.IsVisible = false; isValidEmail = true;
            }
                
        }
        private void ValidatePhoneInput() 
        {
            Regex regex = new Regex(@"^\+?\d+(-\d+)*$");
            Match match = regex.Match(txt_sdt.Text);
            string error_mesage = "";
            if (txt_sdt.Text.Length < 10)
                error_mesage = "Số điện thoại không hợp lệ";
            if (!match.Success)
            {
                error_mesage = "Số điện thoại không hợp lệ";
            }

            if (error_mesage != "")
            {
                lb_sdt_failed.Text = error_mesage;
                lb_sdt_failed.IsVisible = true;
                isValidPhone = false;
            }
            else { 
                lb_sdt_failed.IsVisible = false; 
                isValidPhone = true;
            }
        }

        FileData image = null;
        private async void btn_selectImage_Clicked(object sender, EventArgs e)
        {
            FileData data = await FileRW.PickImageFile();
            lb_avatar.Text = data.FileName;
            image = data;
        }
    }
}