﻿using Android.Media;
using BigBluebirdsMusic.Models;
using BigBluebirdsMusic.ViewModels;
using MediaManager;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BigBluebirdsMusic.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LocalMusicPage : ContentPage
    {
        // List<Song> listMySongs;
        ListSongsViewModel vmListSong;
        public LocalMusicPage()
        {
            InitializeComponent();
            //Java.Lang.Thread thr = new Java.Lang.Thread(InitListMySongs);
            //thr.Start();
            
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            InitListMySongs();
        }
        public void InitListMySongs()
        {
            lvListMySongs.IsRefreshing = true;
            vmListSong = new ListSongsViewModel();
            LoadLocalSongs();
            vmListSong = new ListSongsViewModel(vmListSong.songs.OrderBy(x => x.Name).ToList());
            lvListMySongs.BindingContext = vmListSong;
            lvListMySongs.SetBinding(ListView.ItemsSourceProperty, "songs");
            // listMySongs = new List<Song>();

            // listMySongs = listMySongs.OrderBy(x => x.Name).ToList();
            // lvListMySongs.HeightRequest = 60 * vmListSong.songs.Count + 10;
            
            lvListMySongs.IsRefreshing = false;
        }

        public void LoadLocalSongs()
        {
            //var directory = Android.OS.Environment.ExternalStorageDirectory;
            string ExMusicDirectoryPath = Android.OS.Environment.GetExternalStoragePublicDirectory(
                Android.OS.Environment.DirectoryMusic).ToString();
            string myAppMusicDirectoryPath = Android.App.Application.Context.GetExternalFilesDir("").AbsolutePath;
            Console.WriteLine("ATDEMO: " + myAppMusicDirectoryPath);

            LoadSongsFromDirec(myAppMusicDirectoryPath);

            //Folder Music
            LoadSongsFromDirec(ExMusicDirectoryPath);

            //Tim zing mp3
            string zingMp3Path = ExMusicDirectoryPath.Replace("Music", "Zing MP3");
            LoadSongsFromDirec(zingMp3Path);

            //Tim NCT
            string nCTPath = ExMusicDirectoryPath.Replace("Music", "NCT");
            LoadSongsFromDirec(nCTPath);

            string bbbPath = ExMusicDirectoryPath.Replace("Music", "BBBMusic");
            LoadSongsFromDirec(bbbPath);
        }
        public void LoadSongsFromDirec(string path)
        {
            try
            {
                foreach (string musicFilePath in Directory.EnumerateFiles(path, "*", SearchOption.AllDirectories))
                {
                    if (musicFilePath.EndsWith(".mp3"))
                    {
                        // Console.WriteLine("ATDEMO: " + musicFilePath);
                        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                        mmr.SetDataSource(musicFilePath);
                        string[] name_spliter = musicFilePath.Split((char)47);
                        string name = name_spliter[name_spliter.Length - 1].Replace(".mp3", "");
                        string title = mmr.ExtractMetadata(Android.Media.MetadataKey.Title);
                        if (title != null && title != "")
                        {
                            name = title;
                        }
                        string artist = mmr.ExtractMetadata(Android.Media.MetadataKey.Artist);
                        int duration = Int32.Parse(mmr.ExtractMetadata(Android.Media.MetadataKey.Duration)) / 1000;
                        string strDuration = Song.GetDurationString(duration);
                        name = name.Length >= 20 ? name.Substring(0, 20) + "..." : name;
                        Song song = new Song
                        {
                            ID = 0,
                            IsLocal = true,
                            Name = name,
                            Singer = artist,
                            Duration = duration,
                            DurationString = strDuration,
                            ImageLink = "ni_music_note.png",
                            LinkMp3 = musicFilePath

                        };
                        if (File.Exists(musicFilePath.Replace(".mp3", ".lyric")))
                        {
                            song.LinkLyric = musicFilePath.Replace(".mp3", ".lyric");
                        }
                        vmListSong.AddSong(song);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }


        private void lvListMySongs_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Song song = (Song)e.Item;
            PlayingPage.GetInstance().PlaySong(song);
        }

        private void refresh_view_Refreshing(object sender, EventArgs e)
        {
            Refresh();
            refresh_view.IsRefreshing = false;
        }

        [Obsolete]
        private void Refresh()
        {
            InitListMySongs();
        }

        private void MenuItem_AddPlayingList_Clicked(object sender, EventArgs e)
        {
            var menuItem = ((MenuItem)sender);
            Song song = (Song)menuItem.CommandParameter;
            try
            {
                MediaManager.Library.MediaItem mediaItem = new MediaManager.Library.MediaItem(song.LinkMp3);
                CrossMediaManager.Current.Queue.Add(mediaItem);
                CrossMediaManager.Current.Queue.MediaItems.Add(mediaItem);
                PlayingPage.GetInstance().vmPlayinglist.AddSong(song);
                string message = "Đã thêm bài hát vào danh sách đang phát!";
                Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Short).Show();
            }
            catch
            {
                string message = "Thêm bài hát vào danh sách đang phát thất bại!";
                Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Short).Show();
            }
        }

        private void MenuItem_AddPlaylist_Clicked(object sender, EventArgs e)
        {
            var menuItem = ((MenuItem)sender);
            Song addingSong = (Song)menuItem.CommandParameter;
            Navigation.PushAsync(new LocalPlaylistPage(true, addingSong), true);
        }
    }
}