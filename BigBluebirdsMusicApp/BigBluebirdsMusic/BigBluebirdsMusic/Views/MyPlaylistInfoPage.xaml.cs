﻿using BigBluebirdsMusic.Models;
using BigBluebirdsMusic.Services;
using BigBluebirdsMusic.ViewModels;
using MediaManager;
using Plugin.Share;
using Plugin.Share.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BigBluebirdsMusic.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyPlaylistInfoPage : ContentPage
    {
        public Playlist Playlist { get; set; }
        public ListSongsViewModel vmListSongs;
        public MyPlaylistInfoPage(Playlist playlist)
        {
            InitializeComponent();
            this.Playlist = playlist;

            imgPlaylist.Source = playlist.ImageLink;
            lbPlaylistName.Text = playlist.Name;
            lbPlaylistNumSong.Text = playlist.NumberSong.ToString() + " bài hát";
            lbPlaylistDescription.Text = playlist.Description;
            lb_totalListen.Text = Song.TotalIntToString(playlist.TotalListen);
            lb_totalLike.Text = Song.TotalIntToString(playlist.TotalLike);
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            GetSong();
        }
        private async void GetSong()
        {
            lvListSong.IsRefreshing = true;
            if (!Playlist.IsLocal)
            {
                Playlist tmp = await DataService.GetPlaylistInforAsync(Playlist.ID.ToString());
                if (tmp == null)
                    Playlist.Songs = new List<Song>();
                else if (tmp.Songs == null)
                    Playlist.Songs = new List<Song>();
                else
                    Playlist.Songs = tmp.Songs;
            }
            else // local playlist
            {
                Playlist.Songs = new LocalDatabaseService().GetListSongsOfLocalPlaylist(Playlist);
                Playlist.NumberSong = Playlist.Songs.Count;
                lbPlaylistNumSong.Text = Playlist.NumberSong.ToString() + " bài hát";
            }
            vmListSongs = new ListSongsViewModel(Playlist.Songs);
            lvListSong.BindingContext = vmListSongs;
            lvListSong.SetBinding(ListView.ItemsSourceProperty, "songs");
            //lvListSong.ItemsSource = Playlist.Songs;
            lvListSong.IsRefreshing = false;
        }
        private async void lvListSong_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            PlayingPage.GetInstance().PlaySong(Playlist.Songs[e.ItemIndex]);
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {

        }

        [Obsolete]
        private async void MenuItem_Download_Clicked(object sender, EventArgs e)
        {
            var menuItem = ((MenuItem)sender);
            Song song = (Song)menuItem.CommandParameter;
            string message = "";
            if (song.IsLocal)
            {
                message = "Bài hát đã được tải xuống.";
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    message,
                    Android.Widget.ToastLength.Short).Show();
                return;
            }

            try
            {
                message = "Đang tải xuống bài hát " + song.Name;
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    message,
                    Android.Widget.ToastLength.Short).Show();
                await new Downloader().SaveFilePublicStorageAsync(song.Name + "-" + song.ID + ".mp3", song.LinkMp3);
            }
            catch
            {
                message = "Tải xuống bài hát thất bại!";
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    message,
                    Android.Widget.ToastLength.Short).Show();
            }
            try
            {
                await new Downloader().SaveFilePublicStorageAsync(song.Name + "-" + song.ID + ".lyric", song.LinkLyric);
            }
            catch { }

            message = "Đã tải xuống bài hát " + song.Name;
            Android.Widget.Toast.MakeText(
                Android.App.Application.Context,
                message,
                Android.Widget.ToastLength.Short).Show();
        }

        private async void MenuItem_Remove_Clicked(object sender, EventArgs e)
        {
            bool answer = await DisplayAlert("Xóa", "Bạn có muốn xóa bài hát khỏi danh sách?", "Vâng", "Lúc khác");
            if (!answer)
            {
                return;
            }
            var menuItem = ((MenuItem)sender);
            Song song = (Song)menuItem.CommandParameter;
            bool res = false;
            if (this.Playlist.IsLocal)
            {
                res = new LocalDatabaseService().RemoveSongFromPlaylist(song, this.Playlist);
            }
            else
            {
                res = await DataService.RemoveSongFromMyPlaylist(song, this.Playlist);
            }
            if (res)
            {
                vmListSongs.RemoveSong(song);
                //this.Playlist.NumberSong -= 1;
                lbPlaylistNumSong.Text = this.Playlist.NumberSong.ToString() + " bài hát";
            }
            else
            {
                Android.Widget.Toast.MakeText(
                        Android.App.Application.Context,
                        "Xóa bài hát khỏi danh sách phát thất bại!",
                        Android.Widget.ToastLength.Short).Show();
            }
            //FileRW.ReadListPlaylist();
        }

        private bool isLiked = false;
        private async void btn_like_Pressed(object sender, EventArgs e)
        {
            if (Playlist.IsLocal)
            {
                // Toast bai hat offline
                string message = "Danh sách phát offline!";
                Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Short).Show();
                return;
            }
            if (!User.GetInstance().IsLoggedIn)
            {
                //Show hoi dang nhap
                bool answer = await DisplayAlert("Chưa đăng nhập", "Bạn có muốn đăng nhập?", "Vâng", "Lúc khác");
                if (answer)
                {
                    //Ensure Logout beforelogin
                    User.LogoutLocalUser();
                    await Navigation.PushModalAsync(new LoginPage());
                }
                return;
            }
            
            if (isLiked)
            {
                isLiked = false;
                btn_like.Source = "ni_heart_off.png";
            }
            else
            {
                isLiked = true;
                btn_like.Source = "ni_heart_fill.png";
            }
            //Call api like dislike
            Playlist new_info = await DataService.LikeOrDisLikeAsync(isLiked, Playlist.ID);
            if (new_info != null)
            {
                //Set label like, listen
                lb_totalLike.Text = Song.TotalIntToString(Int32.Parse(new_info.TotalLike.ToString()));
                lb_totalListen.Text = Song.TotalIntToString(Int32.Parse(new_info.TotalListen.ToString()));
            }
        }

        private void btn_playAll_Clicked(object sender, EventArgs e)
        {
            if(Playlist.NumberSong!=0)
                PlayingPage.GetInstance().PlayPlaylist(Playlist, 0);
        }
        private void MenuItem_AddPlayingList_Clicked(object sender, EventArgs e)
        {
            var menuItem = ((MenuItem)sender);
            Song song = (Song)menuItem.CommandParameter;
            try
            {
                MediaManager.Library.MediaItem mediaItem = new MediaManager.Library.MediaItem(song.LinkMp3);
                CrossMediaManager.Current.Queue.Add(mediaItem);
                CrossMediaManager.Current.Queue.MediaItems.Add(mediaItem);
                PlayingPage.GetInstance().vmPlayinglist.AddSong(song);
                string message = "Đã thêm bài hát vào danh sách đang phát!";
                Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Short).Show();
            }
            catch
            {
                string message = "Thêm bài hát vào danh sách đang phát thất bại!";
                Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Short).Show();
            }

        }

        private void btn_edit_Tapped(object sender, EventArgs e)
        {
            info_playlist.Opacity = 0.5;
            info_playlist.IsEnabled = false;
            context_edit_menu.IsVisible = true;

            //For edit page
            txt_playlist_name.Text = this.Playlist.Name;
            txt_playlist_description.Text = this.Playlist.Description;
        }

        private async void btn_delete_Tapped(object sender, EventArgs e)
        {
            bool answer = await DisplayAlert("Xóa", "Bạn có muốn xóa danh sách phát này?", "Vâng", "Lúc khác");
            if (answer)
            {
                //Xóa
                if (Playlist.IsLocal)
                    new LocalDatabaseService().DeletePlayList(this.Playlist);
                else // Online
                {
                    if (await DataService.DeletePlaylist(this.Playlist))
                    {
                        Android.Widget.Toast.MakeText(Android.App.Application.Context, 
                            "Xóa danh sách phát thành công!", Android.Widget.ToastLength.Short).Show();
                    }
                    else
                    {
                        Android.Widget.Toast.MakeText(Android.App.Application.Context,
                            "Xóa danh sách phát thất bại!", Android.Widget.ToastLength.Short).Show();
                    }    

                }
                //Quay về
                await Navigation.PopAsync();
            }
        }

        private async void btn_share_Tapped(object sender, EventArgs e)
        {
            if (Playlist.IsLocal)
            {
                string message = "Không thể chia sẻ danh sách phát offline!";
                Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Short).Show();
                return;
            }
            string link = "http://bigbluebirds.vn/playlists/" + (Playlist.ID);
            var msg = new ShareMessage { Text = link };
            await CrossShare.Current.Share(msg);
        }

        private void btn_cancel_Clicked(object sender, EventArgs e)
        {
            info_playlist.Opacity = 1;
            info_playlist.IsEnabled = true;
            context_edit_menu.IsVisible = false;
        }

        private async void btn_save_Clicked(object sender, EventArgs e)
        {
            //save 
            if (this.Playlist.IsLocal)
            {
                if(txt_playlist_name.Text!= "")
                {
                    this.Playlist.Name = txt_playlist_name.Text;
                    this.Playlist.Description = txt_playlist_description.Text;
                    new LocalDatabaseService().UpdatePlayList(this.Playlist);
                    lbPlaylistName.Text = this.Playlist.Name;
                    lbPlaylistDescription.Text = this.Playlist.Description;
                }
            }
            else // Update infor online
            {
                Playlist pll = Playlist;
                pll.Name = txt_playlist_name.Text;
                pll.Description = txt_playlist_description.Text;
                if (await DataService.UpdatePlaylist(pll))
                {
                    Android.Widget.Toast.MakeText(
                        Android.App.Application.Context,
                        "Cập nhật thông tin thành công!",
                        Android.Widget.ToastLength.Short).Show();
                    lbPlaylistName.Text = pll.Name;
                    lbPlaylistDescription.Text = pll.Description;
                }
                else
                {
                    Android.Widget.Toast.MakeText(
                        Android.App.Application.Context,
                        "Cập nhật thông tin thất bại!",
                        Android.Widget.ToastLength.Short).Show();
                }
            }
            //exit
            btn_cancel_Clicked(sender, e);
        }

        private void rf_lstSong_Refreshing(object sender, EventArgs e)
        {
            GetSong();
            rf_lstSong.IsRefreshing = false;
        }
    }

}