﻿using BigBluebirdsMusic.Models;
using BigBluebirdsMusic.Services;
using Plugin.FacebookClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BigBluebirdsMusic.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new RegisterPage(), true);
        }

        // LB return
        private async void TapGestureRecognizer_Tapped_1(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

        private async void btn_login_Clicked(object sender, EventArgs e)
        {
            User user = await DataService.LoginAsync(txt_username.Text, txt_pass.Text);
            if(user == null)
            {
                lb_failed.IsVisible = true;
                return;
            }
            // Login success
            User.SetInstance(user);
            user.SaveInfoToFile();
            ProfilePage.user = user;
            await Navigation.PopModalAsync();
        }

        private void txt_username_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(lb_failed.IsVisible)
                lb_failed.IsVisible = false;
        }

        private async void btn_fbLogin_Clicked(object sender, EventArgs e)
        {
            if (CrossFacebookClient.Current.IsLoggedIn)
            {
                CrossFacebookClient.Current.Logout();
            }
            var fbLoginResponse = await CrossFacebookClient.Current.LoginAsync(new string[]{"email"});
            if (fbLoginResponse.Status == FacebookActionStatus.Completed)
            {
                var fbUserAccessToken = CrossFacebookClient.Current.ActiveToken;
                // login use the fb access token
                User user = await DataService.LoginFacebookAsync(fbUserAccessToken);
                if (user == null)
                {
                    string message = "Đăng nhập thất bại!";
                    Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Short).Show();
                    return;
                }
                // Login success
                User.SetInstance(user);
                user.SaveInfoToFile();
                ProfilePage.user = user;
                var vmProfile = BigBluebirdsMusic.ViewModels.ProfileViewModel.GetInstance();
                vmProfile.SetUserInfoCommand.Execute(user);
                await Navigation.PopModalAsync();
            }
            else
            {
                string message = "Đăng nhập thất bại!";
                Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Short).Show();
            }
        }

        private void btn_ggLogin_Clicked(object sender, EventArgs e)
        {
            string message = "Tính năng chưa được hỗ trợ!";
            Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Short).Show();
        }

        private void TapGestureRecognizer_Tapped_2(object sender, EventArgs e)
        {
            
        }
    }
}