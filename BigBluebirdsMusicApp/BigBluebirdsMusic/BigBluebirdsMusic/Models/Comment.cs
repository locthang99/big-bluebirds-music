﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BigBluebirdsMusic.Models
{
    public class Comment
    {
        public int ID { get; set; }
        public string UserName { get; set; }
        public string ImageLink { get; set; }
        public string Time { get; set; }
        public string Content { get; set; }
        public Comment()
        {
            ID = 1;
            UserName = "Tan Nguyen";
            ImageLink = "m_musicicon.png";
            Time = "29/10/2020-22:22";
            Content = "Hat hay qua!!!";
        }

    }
}
