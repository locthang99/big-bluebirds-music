﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BigBluebirdsMusic.Models
{
    public enum PlayingPageStatus
    {
        NON,
        COMMENT,
        LYRIC,
        PLAYLIST,
        INFO
    }
    public enum LOOP_STATUS 
    { 
        LOOP_ALL,
        LOOP_ONE,
        LOOP_OFF,
    }
}
