﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace BigBluebirdsMusic.Models
{
    public class Playlist
    {
        // Su dung de luu vao localDB
        [PrimaryKey, AutoIncrement]
        public int LocalID { get; set; }

        // ID online
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageLink { get; set; }
        public string Type { get; set; } //online or offline
        public string DateCreate { get; set; }
        public int TotalLike { get; set; }
        public int TotalListen { get; set; }

        [Ignore] // Ignore list Songs because it cant be store
        public List<Song> Songs { get; set; }
        public int NumberSong { get; set; }
        public bool IsLocal { get; set; }
        public Playlist()
        {
            ID = 0;
            Name = "BigBlueBirds";
            Description = "Nghe ngay những bài nhạc cực chất ngay hôm nay!";
            ImageLink = "music_playlist.jpg";
            IsLocal = true;
            Songs = new List<Song>();
            //Songs.Add(new Song());
            //Songs.Add(new Song());
            //Songs.Add(new Song());
            //Songs.Add(new Song());
            //Songs.Add(new Song());
            NumberSong = Songs.Count;
        }

        public string GenerateJson()
        {
            string songstr = "";
            foreach(Song song in this.Songs)
            {
                songstr += song.GenerateJson() + ",";
            }
            return "{ID:" + this.ID
                + ", Name:\"" + this.Name
                + "\", ImageLink:\"" + this.ImageLink
                + "\", NumberSong:\"" + this.NumberSong
                + "\", IsLocal:\"" + this.IsLocal
                + "\", Songs:[" + songstr + "]}";
        }
    }
}
