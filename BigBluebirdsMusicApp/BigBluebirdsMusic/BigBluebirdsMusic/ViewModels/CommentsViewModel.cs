﻿using BigBluebirdsMusic.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace BigBluebirdsMusic.ViewModels
{
    public class CommentsViewModel
    {
        public ObservableCollection<Comment> comments { get; set; }
        public ICommand AddCommentCommand => new Command(AddComment);
        public ICommand RemoveCommentCommand => new Command(RemoveComment);
        public ICommand ClearCommentsCommand => new Command(ClearComments);
        public ICommand SetCommentsCommand => new Command(SetComments);
        public void AddComment(object obj)
        {
            Comment comment = (Comment)obj;
            comments.Add(comment);
        }
        public void RemoveComment(object obj)
        {
            Comment comment = (Comment)obj;
            comments.Remove(comment);
        }
        public void ClearComments()
        {
            comments.Clear();
        }
        public void SetComments(object obj)
        {
            List<Comment> lstComments = (List<Comment>)obj;
            comments.Clear();
            foreach(Comment item in lstComments)
            {
                comments.Add(item);
            }
        }

        public CommentsViewModel()
        {
            comments = new ObservableCollection<Comment>(new List<Comment>());
        }
        public CommentsViewModel(List<Comment> lstcomments)
        {
            comments = new ObservableCollection<Comment>(lstcomments);
        }
    }
}
