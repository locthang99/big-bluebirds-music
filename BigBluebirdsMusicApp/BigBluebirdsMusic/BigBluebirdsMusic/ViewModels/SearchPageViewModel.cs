﻿using BigBluebirdsMusic.Models;
using BigBluebirdsMusic.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace BigBluebirdsMusic.ViewModels
{
    class SearchPageViewModel: System.ComponentModel.INotifyPropertyChanged
    {
        private static SearchPageViewModel instance = null;

        public event PropertyChangedEventHandler PropertyChanged;

        public static SearchPageViewModel GetInstance()
        {
            if (instance == null)
            {
                instance = new SearchPageViewModel();
            }
            return instance;
        }

        private ObservableCollection<Song> resultSongs { get; set; }
        public ObservableCollection<Song> ResultSongs
        {
            get { return resultSongs; }
            set
            {
                resultSongs = value;
            }
        }

        private ObservableCollection<Playlist> resultPlaylists { get; set; }
        public ObservableCollection<Playlist> ResultPlaylists
        {
            get { return resultPlaylists; }
            set
            {
                resultPlaylists = value;
            }
        }
    
        private bool lbResBaihatVisible { get; set; }
        public bool LbResBaihatVisible
        {
            get { return lbResBaihatVisible; }
            set
            {
                lbResBaihatVisible = value;
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs("LbResBaihatVisible"));
                }
            }
        }
        private bool resPlaylistVisible { get; set; }
        public bool ResPlaylistVisible
        {
            get { return resPlaylistVisible; }
            set
            {
                resPlaylistVisible = value;
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs("ResPlaylistVisible"));
                }
            }
        }

        private int playlistHeight { get; set; }
        public int PlaylistHeight 
        {
            get { return playlistHeight; }
            set
            {
                playlistHeight = value;
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs("PlaylistHeight"));
                }
            }
        }
        public SearchPageViewModel()
        {
            resultSongs = new ObservableCollection<Song>();
            resultPlaylists = new ObservableCollection<Playlist>();
            LbResBaihatVisible = true;
            ResPlaylistVisible = false;
        }

        public ICommand GetSearchResultCommand => new Command(GetSearchResult);
        public ICommand ResetSearchResultCommand => new Command(ResetSearchResult);
        public async void GetSearchResult(object txt)
        {
            LbResBaihatVisible = false;
            ResPlaylistVisible = true;
            string searchTxt = (string)txt;
            int count = resultSongs.Count;
            for(int i = 0; i < count; i++)
            {
                resultSongs.Remove(resultSongs[0]);
            }
            foreach (var item in await DataService.GetSearchSongsResultsAsync(searchTxt))
            {
                resultSongs.Add(item);
            }

            // Get search playlist
            count = resultPlaylists.Count;
            for (int i = 0; i < count; i++)
            {
                resultPlaylists.Remove(resultPlaylists[0]);
            }
            foreach (var item in await DataService.GetSearchPlaylists(searchTxt))
            {
                resultPlaylists.Add(item);
            }

            if (resultSongs.Count == 0)
                LbResBaihatVisible = true;
            else
                LbResBaihatVisible = false;
            if (resultPlaylists.Count == 0)
            {
                ResPlaylistVisible = false;
                PlaylistHeight = 10;
            }
            else
            {
                ResPlaylistVisible = true;
                PlaylistHeight = (1 / 2 + 1) * 220 + 30;
            }
                
        }

        public void ResetSearchResult(object obj)
        {
            int count = resultSongs.Count;
            for (int i = 0; i < count; i++)
            {
                resultSongs.Remove(resultSongs[0]);
            }
            count = resultPlaylists.Count;
            for (int i = 0; i < count; i++)
            {
                resultPlaylists.Remove(resultPlaylists[0]);
            }
            LbResBaihatVisible = true;
            ResPlaylistVisible = false;
        }
    }
}
