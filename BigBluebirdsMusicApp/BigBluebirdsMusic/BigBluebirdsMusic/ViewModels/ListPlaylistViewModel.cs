﻿using BigBluebirdsMusic.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace BigBluebirdsMusic.ViewModels
{
    public class ListPlaylistViewModel
    {
        // This field can be filter
        public ObservableCollection<Playlist> playlists { get; set; }
        //AllPlaylists use for backup all playlist loaded
        public ObservableCollection<Playlist> allPlaylists { get; set; }
        public ICommand AddPlaylistCommand => new Command(AddPlaylist);
        public ICommand RemovePlaylistCommand => new Command(RemovePlaylist);

        public void AddPlaylist(object obj)
        {
            Playlist playlist = (Playlist)obj;
            playlists.Add(playlist);
            allPlaylists.Add(playlist);
        }
        public void RemovePlaylist(object obj)
        {
            Playlist playlist = (Playlist)obj;
            playlists.Remove(playlist);
            allPlaylists.Remove(playlist);
        }

        public ListPlaylistViewModel()
        {
            List<Playlist> lstPlaylists = new List<Playlist>();
            playlists = new ObservableCollection<Playlist>(lstPlaylists);
            allPlaylists = new ObservableCollection<Playlist>(lstPlaylists);
        }
        public ListPlaylistViewModel(List<Playlist> lstplaylists)
        {
            playlists = new ObservableCollection<Playlist>(lstplaylists);
            allPlaylists = new ObservableCollection<Playlist>(lstplaylists);
        }
        public ListPlaylistViewModel(List<Playlist> lstplaylists, List<Playlist> lstallplaylists)
        {
            playlists = new ObservableCollection<Playlist>(lstplaylists);
            allPlaylists = new ObservableCollection<Playlist>(lstallplaylists);
        }
    }
}
