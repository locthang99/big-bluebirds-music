﻿using Android.Util;
using BigBluebirdsMusic.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.FilePicker;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BigBluebirdsMusic.Services
{
    class FileRW
    {
        public static List<string> ReadListPlaylist()
        {
            try
            {
                var backingFile = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "playlist");

                if (backingFile == null || !File.Exists(backingFile))
                {
                    return null;
                }
                List<string> FileData = new List<string>();
                using (var reader = new StreamReader(backingFile, true))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        FileData.Add(line);
                        // object a=JsonConvert.DeserializeObject(line);
                    }
                }

                return FileData;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static async Task SavePlaylist(Playlist playlist)
        {
            try
            {
                var backingFile = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "playlist");
                using (var writer = File.CreateText(backingFile))
                {
                    await writer.WriteLineAsync(playlist.GenerateJson());
                }
            }
            catch (Exception ex)
            {
            }

        }

        public static List<LyricRow> ReadLyric(string backingFile)
        {
            try
            {
                if (backingFile == null || !File.Exists(backingFile))
                {
                    return null;
                }
                List<LyricRow> rows = new List<LyricRow>();
                using (var reader = new StreamReader(backingFile, true))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        string m = line.Substring(1, 5).Split(':')[0];
                        string s = line.Substring(1, 5).Split(':')[1];
                        int iminute = 0;
                        int isecond = 0;
                        Int32.TryParse(m, out iminute);
                        Int32.TryParse(s, out isecond);
                        int time = iminute * 60 + isecond;
                        LyricRow lyricRow = new LyricRow() { Content = line.Substring(10), Time = time };
                        rows.Add(lyricRow);
                        // object a=JsonConvert.DeserializeObject(line);
                    }
                }

                return rows;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static async Task SaveRecentSongs(Song song)
        {
            try
            {
                var backingFile = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "recent_songs.sbbb");
                List<string> newList = new List<string>();
                newList.Add(song.GenerateJson());
                if (File.Exists(backingFile))
                {
                    string[] allLine = File.ReadAllLines(backingFile);
                    if (allLine.Length > 0)
                    {
                        if(allLine[0] != newList[0])
                            newList.Add(allLine[0]);
                    }
                        
                    if (allLine.Length > 1)
                    {
                        if (allLine[1] != newList[0])
                            newList.Add(allLine[1]);
                    }
                    if (newList.Count < 3 && allLine.Length>2)
                    {
                        if (allLine[2] != newList[0])
                            newList.Add(allLine[1]);
                    }
                }
                File.WriteAllLines(backingFile, newList);
                //File.AppendAllText(backingFile, song.GenerateJson());
                //using (var writer = File.CreateText(backingFile))
                //{
                //    await writer.WriteLineAsync(song.GenerateJson());
                //}
            }
            catch (Exception ex)
            {
            }
        }
    
        public static List<Song> ReadListRecentSongs()
        {
            try
            {
                var backingFile = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "recent_songs.sbbb");

                if (backingFile == null || !File.Exists(backingFile))
                {
                    return new List<Song>();
                }
                List<Song> lstSongs = new List<Song>();
                using (var reader = new StreamReader(backingFile, true))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        var songJson = JObject.Parse(line);

                        Song song = new Song
                        {
                            ID = Int32.Parse(songJson["ID"].ToString()),
                            Name = songJson["Name"].ToString(),
                            ImageLink = songJson["ImageLink"].ToString(),
                            Singer = songJson["Singer"].ToString(),
                            DurationString = songJson["DurationString"].ToString(),
                            Duration = Int32.Parse(songJson["Duration"].ToString()),
                            LinkMp3 = songJson["Link"].ToString(),
                            IsLocal = songJson["IsLocal"].ToString().ToLower() == "true",
                        };
                        lstSongs.Add(song);
                    }
                }
                return lstSongs;
            }
            catch (Exception ex)
            {
                return new List<Song>();
            }
        }

        public static async void SaveUserInfor(User user)
        {
            try
            {
                var backingFile = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "user.info");

                using (var writer = File.CreateText(backingFile))
                {
                    await writer.WriteLineAsync(user.GenerateJson());
                    await writer.WriteLineAsync(user.Token);
                }
            }
            catch (Exception ex)
            {
            }
        }

        public static User ReadUserInfor()
        {
            try
            {
                var backingFile = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "user.info");

                if (backingFile == null || !File.Exists(backingFile))
                {
                    return null;
                }
                using (var reader = new StreamReader(backingFile, true))
                {
                    string line;
                    line = reader.ReadLine();
                    var userJson = JObject.Parse(line);
                    User user = new User() {
                        ID = userJson["UserID"].ToString(),
                        FirstName = userJson["FirstName"].ToString(),
                        LastName = userJson["LastName"].ToString(),
                        PhoneNumber = userJson["PhoneNumber"].ToString(),
                        UserName = userJson["UserName"].ToString(),
                        Email = userJson["Email"].ToString(),
                        Role = userJson["Role"].ToString(),
                        Thumbnail = userJson["Thumbnail"].ToString(),
                        IsLoggedIn = userJson["IsLoggedIn"].ToString().ToLower() == "true",
                        DayOfBirdth = DateTime.Parse(userJson["DayOfBirdth"].ToString())
                    };
                    while ((line = reader.ReadLine()) != null)
                    {
                        user.Token = line;
                    }
                    return user;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static async Task<Plugin.FilePicker.Abstractions.FileData> PickImageFile()
        {
            var file = await CrossFilePicker.Current.PickFile();
            if (file.FileName.EndsWith(".png") || file.FileName.EndsWith(".jpeg")
                || file.FileName.EndsWith(".jpg"))
            {
                return file;
            }
            else
            {
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    "Vui lòng chọn file hình ảnh!",
                    Android.Widget.ToastLength.Short).Show();
                return null;
            }
        }
        public static async Task<Plugin.FilePicker.Abstractions.FileData> PickMusicFile()
        {
            var file = await CrossFilePicker.Current.PickFile();
            if (file.FileName.EndsWith(".mp3"))
            {
                return file;
            }
            else
            {
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    "Vui lòng chọn file mp3!",
                    Android.Widget.ToastLength.Short).Show();
                return null;
            }
        }
    }

    public class Downloader
    {
        [Obsolete]
        public async Task SaveFilePublicStorageAsync(string filename, string fileUrl)
        {
            string musicDirectoryPath = Android.OS.Environment.GetExternalStoragePublicDirectory(
                Android.OS.Environment.DirectoryMusic).ToString();
            //musicDirectoryPath = Android.App.Application.Context.GetExternalFilesDir("").AbsolutePath;
            var filePath = Path.Combine(musicDirectoryPath, filename);
            await new WebClient().DownloadFileTaskAsync(
                new Uri(fileUrl), filePath);
            
        }
    }
}
