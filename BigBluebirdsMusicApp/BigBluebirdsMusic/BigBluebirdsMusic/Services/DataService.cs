﻿using BigBluebirdsMusic.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.FilePicker.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BigBluebirdsMusic.Services
{
    public class DataService
    {
        public static string host = @"http://[2405:4802:911a:6c70::10]:5005/";
        //public static string host = @"http://192.168.43.166:5000/";
        public static async Task<string> ApiStringData(string url)
        {
            var client = new HttpClient()
            {
                //BaseAddress = new Uri(string.Format(url)),
                Timeout = TimeSpan.FromMilliseconds(1000)
            };
            try
            {
                var response = await client.GetAsync(string.Format(url));
                var responseString = await response.Content.ReadAsStringAsync();
                return responseString;
            }
            catch {
                return "{\"status\": 404}";
            }
            
        }

        public static async Task<string> GetApiAsync(string url)
        {
            var task = DataService.ApiStringData(url); ;
            if (await Task.WhenAny(task, Task.Delay(10000)) == task)
            {
                return await DataService.ApiStringData(url);
            }
            else
            {
                return "{\"status\": 404}";
            }
        }

        public static async Task<List<Song>> GetSearchSongsResultsAsync(string searchText)
        {
            //Get song list
            string lstSongUrl = host + @"api/Songs/ByName?Name="+searchText+"&Index=1&PageSize=8";
            var client = new HttpClient();
            var response = await client.GetAsync(lstSongUrl);
            string result = response.Content.ReadAsStringAsync().Result;
            var lstSongJson = JObject.Parse(result);
            int status = Int32.Parse(lstSongJson["status"].ToString());
            if (status != 200)
                return new List<Song>();

            var data = lstSongJson["data"];
            List<Song> lstSong = new List<Song>();
            if (data != null)
            {
                foreach (var item in data)
                {
                    try
                    {
                        Song song = new Song()
                        {
                            ID = Int32.Parse(item["id"].ToString()),
                            Name = item["name"].ToString(),
                            DateCreate = item["dateCreate"].ToString(),
                            Duration = Int32.Parse(item["duration"].ToString()),
                            DurationString = Song.GetDurationString(Int32.Parse(item["duration"].ToString())),
                            ImageLink = item["thumbnail"].ToString(),
                            LinkLyric = item["lyric"].ToString(),
                            LinkMp3 = item["fileMusic"].ToString(),
                            TotalLike = Int32.Parse(item["totalLike"].ToString()),
                            TotalListen = Int32.Parse(item["totalListen"].ToString()),
                            Singer = item["owners"][0]["nameOwner"].ToString().Trim(),
                            IsLocal = false,
                        };
                        lstSong.Add(song);
                    }
                    catch { }                    
                    
                }
            }
            return lstSong;
            // KhoiTaoDuLieuGia(searchText);
        }
        public static List<Playlist> GetSearchPlaylistResults(string searchText)
        {
            List<Playlist> listPlayLists;
            listPlayLists = new List<Playlist>();
            for (int i = 0; i < searchText.Length; i++)
                listPlayLists.Add(new Playlist());
            return listPlayLists;
        }
        public static List<Song> KhoiTaoDuLieuGia(string searchText)
        {
            List<Song> listSongs = new List<Song>();
            for(int i=0; i < searchText.Length; i++)
            {
                listSongs.Add(new Song());
            }
            
            return listSongs;
        }

        public static async Task<Playlist> GetPlaylistInforAsync(string id)
        {
            try
            {
                // get playlist infor
                string url = host + @"api/PlayLists/ById?Id=" + id;
                string strRes = await GetApiAsync(url);
                var json = JObject.Parse(strRes);
                int status = Int32.Parse(json["status"].ToString());
                if (status != 200)
                    return null;

                //Get song list
                string lstSongUrl = host + @"api/PlayLists/ListSong?Id=" + id + "&Index=1&PageSize=100";
                var client = new HttpClient();
                var response = await client.GetAsync(lstSongUrl);
                string result = response.Content.ReadAsStringAsync().Result;
                var lstSongJson = JObject.Parse(result);
                status = Int32.Parse(lstSongJson["status"].ToString());
                if (status != 200)
                    return null;

                var data = lstSongJson["data"];
                List<Song> lstSong = new List<Song>();
                if (data != null)
                {
                    foreach (var item in data)
                    {
                        Song song = new Song()
                        {
                            ID = Int32.Parse(item["id"].ToString()),
                            Name = item["name"].ToString(),
                            DateCreate = item["dateCreate"].ToString(),
                            Duration = Int32.Parse(item["duration"].ToString()),
                            DurationString = Song.GetDurationString(Int32.Parse(item["duration"].ToString())),
                            ImageLink = item["thumbnail"].ToString(),
                            LinkLyric = item["lyric"].ToString(),
                            LinkMp3 = item["fileMusic"].ToString(),
                            TotalLike = Int32.Parse(item["totalLike"].ToString()),
                            TotalListen = Int32.Parse(item["totalListen"].ToString()),
                            Singer = item["owners"][0]["nameOwner"].ToString().Trim(),
                            IsLocal = false,
                        };
                        lstSong.Add(song);
                    }
                }

                // Playlist infor
                var pldata = json["data"];
                Playlist playlist = new Playlist()
                {
                    ID = Int32.Parse(pldata["id"].ToString()),
                    Name = pldata["name"].ToString(),
                    Description = pldata["description"].ToString(),
                    DateCreate = pldata["dateCreate"].ToString(),
                    NumberSong = Int32.Parse(pldata["totalSong"].ToString()),
                    TotalListen = Int32.Parse(pldata["totalListen"].ToString()),
                    TotalLike = Int32.Parse(pldata["totalLike"].ToString()),
                    ImageLink = pldata["thumbnail"].ToString(),
                    IsLocal = false,
                    Songs = lstSong
                };
                return playlist;
            }
            catch
            {
                return null;
            }
        }

        public static async Task<Playlist> GetPlaylistInforOnlyAsync(string id)
        {
            try
            {
                // get playlist infor
                string url = host + @"api/PlayLists/ById?Id=" + id;
                string strRes = await GetApiAsync(url);
                var json = JObject.Parse(strRes);
                int status = Int32.Parse(json["status"].ToString());
                if (status != 200)
                    return null;
                // Playlist infor
                var pldata = json["data"];
                Playlist playlist = new Playlist()
                {
                    ID = Int32.Parse(pldata["id"].ToString()),
                    Name = pldata["name"].ToString(),
                    Description = pldata["description"].ToString(),
                    DateCreate = pldata["dateCreate"].ToString(),
                    NumberSong = Int32.Parse(pldata["totalSong"].ToString()),
                    TotalListen = Int32.Parse(pldata["totalListen"].ToString()),
                    TotalLike = Int32.Parse(pldata["totalLike"].ToString()),
                    ImageLink = pldata["thumbnail"].ToString(),
                    IsLocal = false,
                };
                return playlist;
            }
            catch
            {
                return null;
            }
        }


        public static async Task<List<Song>> GetListSongsRecommendUserBase()
        {
            if (!User.GetInstance().IsLoggedIn)
            {
                int id1 = new Random().Next(1, 50);
                int id2 = new Random().Next(51, 100);
                List<Song> lstSongs = new List<Song>();
                lstSongs.Add(await GetSongByIDAsync(id1));
                lstSongs.Add(await GetSongByIDAsync(id2));
                return lstSongs;
            }

            string url = host + @"api/Recommendations/UserBase/Song?userID="+User.GetInstance().ID
                +"&ActionType=LIKE&ObjectType=SONG&Index=1&PageSize=3";
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + User.GetInstance().Token);
            var response = await client.GetAsync(url);
            string result = response.Content.ReadAsStringAsync().Result;
            var lstSongJson = JObject.Parse(result);
            int status = Int32.Parse(lstSongJson["status"].ToString());
            if (status != 200)
                return new List<Song>();

            var data = lstSongJson["data"];
            List<Song> lstSong = new List<Song>();
            if (data != null)
            {
                foreach (var item in data)
                {
                    try
                    {
                        Song song = new Song()
                        {
                            ID = Int32.Parse(item["id"].ToString()),
                            Name = item["name"].ToString(),
                            DateCreate = item["dateCreate"].ToString(),
                            Duration = Int32.Parse(item["duration"].ToString()),
                            DurationString = Song.GetDurationString(Int32.Parse(item["duration"].ToString())),
                            ImageLink = item["thumbnail"].ToString(),
                            LinkLyric = item["lyric"].ToString(),
                            LinkMp3 = item["fileMusic"].ToString(),
                            TotalLike = Int32.Parse(item["totalLike"].ToString()),
                            TotalListen = Int32.Parse(item["totalListen"].ToString()),
                            Singer = item["owners"][0]["nameOwner"].ToString().Trim(),
                            IsLocal = false,
                        };
                        lstSong.Add(song);
                    }
                    catch { }

                }
            }
            return lstSong;
        }

        public static async Task<List<Song>> GetListSongsRecommendItemBase(int songID)
        {
            string url = host + @"api/Recommendations/ItemBase/Song?songID=" + songID
                + "&ActionType=LIKE&ObjectType=SONG&Index=1&PageSize=3";
            var client = new HttpClient();
            var response = await client.GetAsync(url);
            string result = response.Content.ReadAsStringAsync().Result;
            var lstSongJson = JObject.Parse(result);
            int status = Int32.Parse(lstSongJson["status"].ToString());
            if (status != 200)
                return new List<Song>();

            var data = lstSongJson["data"];
            List<Song> lstSong = new List<Song>();
            if (data != null)
            {
                foreach (var item in data)
                {
                    try
                    {
                        Song song = new Song()
                        {
                            ID = Int32.Parse(item["id"].ToString()),
                            Name = item["name"].ToString(),
                            DateCreate = item["dateCreate"].ToString(),
                            Duration = Int32.Parse(item["duration"].ToString()),
                            DurationString = Song.GetDurationString(Int32.Parse(item["duration"].ToString())),
                            ImageLink = item["thumbnail"].ToString(),
                            LinkLyric = item["lyric"].ToString(),
                            LinkMp3 = item["fileMusic"].ToString(),
                            TotalLike = Int32.Parse(item["totalLike"].ToString()),
                            TotalListen = Int32.Parse(item["totalListen"].ToString()),
                            Singer = item["owners"][0]["nameOwner"].ToString().Trim(),
                            IsLocal = false,
                        };
                        lstSong.Add(song);
                    }
                    catch { }

                }
            }
            return lstSong;
        }

        public static async Task<List<Playlist>> GetRecommendedPlaylists()
        {
            // GET RECOMMEND PLAYLIST USER BASE
            List<Playlist> lstRecommendUserBase = new List<Playlist>();
            try
            {
                // get list playlist
                string url = host + @"api/Recommendations/UserBase/Playlist?userID="+User.GetInstance().ID+"&ActionType=LIKE&Index=1&PageSize=4";
                string strRes = await GetApiAsync(url);
                var json = JObject.Parse(strRes);
                int status = Int32.Parse(json["status"].ToString());
                if (status != 200)
                    throw new Exception();

                //Get data playlist
                var data = json["data"];
                if (data != null)
                {
                    foreach (var item in data)
                    {
                        Playlist playlist = new Playlist()
                        {
                            ID = Int32.Parse(item["id"].ToString()),
                            Name = item["name"].ToString(),
                            Description = item["description"].ToString(),
                            DateCreate = item["dateCreate"].ToString(),
                            NumberSong = Int32.Parse(item["totalSong"].ToString()),
                            TotalListen = Int32.Parse(item["totalListen"].ToString()),
                            TotalLike = Int32.Parse(item["totalLike"].ToString()),
                            ImageLink = item["thumbnail"].ToString(),
                            IsLocal = false,
                            Songs = new List<Song>()
                        };
                        if (playlist.ImageLink == null || playlist.ImageLink == "" || playlist.ImageLink == "null")
                        {
                            playlist.ImageLink = "https://i.ibb.co/QJXTF0g/music-playlist.jpg";
                        }
                        lstRecommendUserBase.Add(playlist);
                    }
                }
            }
            catch {}

            // IF NOT ENOUGH GET MORE
            List<Playlist> lstRecommendMore = new List<Playlist>();
            try
            {
                // get list playlist
                string url = host + @"api/PlayLists?Index=1&PageSize=8";
                string strRes = await GetApiAsync(url);
                var json = JObject.Parse(strRes);
                int status = Int32.Parse(json["status"].ToString());
                if (status != 200)
                    throw new Exception();

                //Get data playlist
                var data = json["data"];
                if (data != null)
                {
                    foreach (var item in data)
                    {
                        Playlist playlist = new Playlist()
                        {
                            ID = Int32.Parse(item["id"].ToString()),
                            Name = item["name"].ToString(),
                            Description = item["description"].ToString(),
                            DateCreate = item["dateCreate"].ToString(),
                            NumberSong = Int32.Parse(item["totalSong"].ToString()),
                            TotalListen = Int32.Parse(item["totalListen"].ToString()),
                            TotalLike = Int32.Parse(item["totalLike"].ToString()),
                            ImageLink = item["thumbnail"].ToString(),
                            IsLocal = false,
                            Songs = new List<Song>()
                        };
                        if (playlist.ImageLink == null || playlist.ImageLink == "" || playlist.ImageLink == "null")
                        {
                            playlist.ImageLink = "https://i.ibb.co/QJXTF0g/music-playlist.jpg";
                        }
                        lstRecommendMore.Add(playlist);
                    }
                }
            }
            catch { }
            
            foreach(var item in lstRecommendMore)
            {
                if (lstRecommendUserBase.Count >= 8)
                    break;
                bool isExist = false;
                // Kiem tra no co ton tai chua
                foreach(var item2 in lstRecommendUserBase)
                {
                    if (item.ID == item2.ID)
                    {
                        isExist = true;
                        break;
                    }
                }
                if (!isExist)
                {
                    lstRecommendUserBase.Add(item);
                }
            }
            return lstRecommendUserBase;
        }

        public static async Task<List<Playlist>> GetListPlayListInit()
        {
            List<Playlist> lstRecommendMore = new List<Playlist>();
            try
            {
                // get list playlist
                string url = host + @"api/PlayLists?Index=1&PageSize=8";
                string strRes = await GetApiAsync(url);
                var json = JObject.Parse(strRes);
                int status = Int32.Parse(json["status"].ToString());
                if (status != 200)
                    return new List<Playlist>();

                //Get data playlist
                var data = json["data"];
                if (data != null)
                {
                    foreach (var item in data)
                    {
                        Playlist playlist = new Playlist()
                        {
                            ID = Int32.Parse(item["id"].ToString()),
                            Name = item["name"].ToString(),
                            Description = item["description"].ToString(),
                            DateCreate = item["dateCreate"].ToString(),
                            NumberSong = Int32.Parse(item["totalSong"].ToString()),
                            TotalListen = Int32.Parse(item["totalListen"].ToString()),
                            TotalLike = Int32.Parse(item["totalLike"].ToString()),
                            ImageLink = item["thumbnail"].ToString(),
                            IsLocal = false,
                            Songs = new List<Song>()
                        };
                        if (playlist.ImageLink == null || playlist.ImageLink == "" || playlist.ImageLink == "null")
                        {
                            playlist.ImageLink = "https://i.ibb.co/QJXTF0g/music-playlist.jpg";
                        }
                        lstRecommendMore.Add(playlist);
                    }
                }
                return lstRecommendMore;
            }
            catch {
                return new List<Playlist>();
            }
        }

        public static async Task<List<Song>> GetTopSongs()
        {
            try
            {
                //Get song list
                string lstSongUrl = host + @"api/Songs/Top10?Type=LISTEN";
                var client = new HttpClient();
                var response = await client.GetAsync(lstSongUrl);
                string result = response.Content.ReadAsStringAsync().Result;
                var lstSongJson = JObject.Parse(result);
                int status = Int32.Parse(lstSongJson["status"].ToString());
                if (status != 200)
                    return new List<Song>();

                var data = lstSongJson["data"];
                List<Song> lstSong = new List<Song>();
                if (data != null)
                {
                    foreach (var item in data)
                    {
                        Song song = new Song()
                        {
                            ID = Int32.Parse(item["id"].ToString()),
                            Name = item["name"].ToString(),
                            DateCreate = item["dateCreate"].ToString(),
                            Duration = Int32.Parse(item["duration"].ToString()),
                            DurationString = Song.GetDurationString(Int32.Parse(item["duration"].ToString())),
                            ImageLink = item["thumbnail"].ToString(),
                            LinkLyric = item["lyric"].ToString(),
                            LinkMp3 = item["fileMusic"].ToString(),
                            TotalLike = Int32.Parse(item["totalLike"].ToString()),
                            TotalListen = Int32.Parse(item["totalListen"].ToString()),
                            Singer = item["owners"][0]["nameOwner"].ToString().Trim(),
                            IsLocal = false,
                        };
                        lstSong.Add(song);
                    }
                }
                return lstSong;
            }
            catch
            {
                return new List<Song>();
            }
        }


        public static async Task<List<Playlist>> GetSearchPlaylists(string text)
        {
            // get list playlist
            string url = host + @"api/PlayLists/ByName?Name="+text+"&Index=1&PageSize=8";
            string strRes = await GetApiAsync(url);
            var json = JObject.Parse(strRes);
            int status = Int32.Parse(json["status"].ToString());
            if (status != 200)
                return new List<Playlist>();

            //Get data playlist
            var data = json["data"];
            List<Playlist> lstPlaylist = new List<Playlist>();
            if (data != null)
            {
                foreach (var item in data)
                {
                    try
                    {
                        Playlist playlist = new Playlist()
                        {
                            ID = Int32.Parse(item["id"].ToString()),
                            Name = item["name"].ToString(),
                            Description = item["description"].ToString(),
                            DateCreate = item["dateCreate"].ToString(),
                            NumberSong = Int32.Parse(item["totalSong"].ToString()),
                            TotalListen = Int32.Parse(item["totalListen"].ToString()),
                            TotalLike = Int32.Parse(item["totalLike"].ToString()),
                            ImageLink = item["thumbnail"].ToString(),
                            IsLocal = false,
                            Songs = new List<Song>()
                        };
                        if (playlist.ImageLink == null || playlist.ImageLink == "" || playlist.ImageLink == "null")
                        {
                            playlist.ImageLink = "https://i.ibb.co/QJXTF0g/music-playlist.jpg";
                        }
                        lstPlaylist.Add(playlist);
                    }
                    catch { }
                }
            }
            return lstPlaylist;
        }

        public static async Task<List<Song>> GetOnlineSongsOfUser()
        {
            string url = host + @"api/Songs/MySong?Index=1&PageSize=1000";
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + User.GetInstance().Token);
            var response = await client.GetAsync(url);
            string result = response.Content.ReadAsStringAsync().Result;
            var lstSongJson = JObject.Parse(result);
            int status = Int32.Parse(lstSongJson["status"].ToString());
            if (status != 200)
                return new List<Song>();

            var data = lstSongJson["data"];
            List<Song> lstSong = new List<Song>();
            if (data != null)
            {
                foreach (var item in data)
                {
                    try
                    {
                        Song song = new Song()
                        {
                            ID = Int32.Parse(item["id"].ToString()),
                            Name = item["name"].ToString(),
                            DateCreate = item["dateCreate"].ToString(),
                            Duration = Int32.Parse(item["duration"].ToString()),
                            DurationString = Song.GetDurationString(Int32.Parse(item["duration"].ToString())),
                            ImageLink = item["thumbnail"].ToString(),
                            LinkLyric = item["lyric"].ToString(),
                            LinkMp3 = item["fileMusic"].ToString(),
                            TotalLike = Int32.Parse(item["totalLike"].ToString()),
                            TotalListen = Int32.Parse(item["totalListen"].ToString()),
                            Singer = item["owners"][0]["nameOwner"].ToString().Trim(),
                            IsLocal = false,
                        };
                        lstSong.Add(song);
                    }
                    catch { }

                }
            }
            return lstSong;
        }

        public static async Task<bool> CreateSong(
            string name, string description, FileData thumbnailData, FileData fileMusicData)
        {
            try
            {
                var content = new MultipartFormDataContent();

                // Add the image:
                var thumbnailContent = new StreamContent(thumbnailData.GetStream());
                if (thumbnailData.FileName.EndsWith("png"))
                    thumbnailContent.Headers.ContentType = new MediaTypeHeaderValue("image/png");
                else if (thumbnailData.FileName.EndsWith("jpeg") || thumbnailData.FileName.EndsWith("jpg"))
                    thumbnailContent.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");
                content.Add(thumbnailContent, "Thumbnail", thumbnailData.FileName);

                //Add the mp3
                var fileMusicContent = new StreamContent(fileMusicData.GetStream());
                if (fileMusicData.FileName.EndsWith("mp3"))
                    fileMusicContent.Headers.ContentType = new MediaTypeHeaderValue("audio/mpeg");
                content.Add(fileMusicContent, "FileMusic", fileMusicData.FileName);

                //Get duraion
                Android.Media.MediaMetadataRetriever mmr = new Android.Media.MediaMetadataRetriever();
                mmr.SetDataSource(fileMusicData.FilePath);
                int duration = Int32.Parse(mmr.ExtractMetadata(Android.Media.MetadataKey.Duration)) / 1000;

                //// Add another parameter:
                content.Add(new StringContent(name), "Name");
                content.Add(new StringContent(description), "Description");
                content.Add(new StringContent(duration.ToString()), "Duration");
                
                var url = host + "api/Songs";
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + BigBluebirdsMusic.Models.User.GetInstance().Token);
                var response = await client.PostAsync(url, content);
                string result = response.Content.ReadAsStringAsync().Result;
                var jsonRes = JObject.Parse(result);
                int status = Int32.Parse(jsonRes["status"].ToString());
                if (status != 200)
                    return false;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static async Task<bool> UpdateSong(
            int id, string name, string description, FileData thumbnailData)
        {
            try
            {
                var content = new MultipartFormDataContent();

                // Add the image:
                if (thumbnailData != null)
                {
                    var thumbnailContent = new StreamContent(thumbnailData.GetStream());
                    if (thumbnailData.FileName.EndsWith("png"))
                        thumbnailContent.Headers.ContentType = new MediaTypeHeaderValue("image/png");
                    else if (thumbnailData.FileName.EndsWith("jpeg") || thumbnailData.FileName.EndsWith("jpg"))
                        thumbnailContent.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");
                    content.Add(thumbnailContent, "Thumbnail", thumbnailData.FileName);
                }

                //// Add another parameter:
                content.Add(new StringContent(name), "Name");
                content.Add(new StringContent(description), "Description");

                var url = host + "api/Songs?Id="+id;
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + BigBluebirdsMusic.Models.User.GetInstance().Token);
                var response = await client.PutAsync(url, content);
                string result = response.Content.ReadAsStringAsync().Result;
                var jsonRes = JObject.Parse(result);
                int status = Int32.Parse(jsonRes["status"].ToString());
                if (status != 200)
                    return false;
                return true;
            }
            catch
            {
                return false;
            }
        }


        public static async Task<bool> DeleteSong(int id)
        {
            User user = User.GetInstance();
            if (!user.IsLoggedIn)
            {
                return false;
            }
            // User da login
            var url = host + "api/Songs?Id=" + id;
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + user.Token);
                var response = await client.DeleteAsync(url);
                string result = response.Content.ReadAsStringAsync().Result;
                var jsonRes = JObject.Parse(result);
                int status = Int32.Parse(jsonRes["status"].ToString());
                if (status != 200)
                    return false;
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static async Task<List<Playlist>> GetOnlinePlaylistsOfUser(string filter="")
        {
            // Check login
            User user = User.GetInstance();
            if (!user.IsLoggedIn)
            {
                return new List<Playlist>(); // return empty list
            }
            // User da login
            //Init to call api
            var url = host + "api/PlayLists/PlaylistPrivate?Index=1&PageSize=100";
            // Call api
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + user.Token);
            var response = await client.GetAsync(url);
            string result = response.Content.ReadAsStringAsync().Result;
            var json = JObject.Parse(result);
            int status = Int32.Parse(json["status"].ToString());
            if (status != 200)
                return new List<Playlist>();

            //Get data playlist
            var data = json["data"];
            List<Playlist> lstPlaylist = new List<Playlist>();
            if (data != null)
            {
                foreach (var item in data)
                {
                    try
                    {
                        Playlist playlist = new Playlist()
                        {
                            ID = Int32.Parse(item["id"].ToString()),
                            Name = item["name"].ToString(),
                            Description = item["description"].ToString(),
                            DateCreate = item["dateCreate"].ToString(),
                            NumberSong = Int32.Parse(item["totalSong"].ToString()),
                            TotalListen = Int32.Parse(item["totalListen"].ToString()),
                            TotalLike = Int32.Parse(item["totalLike"].ToString()),
                            ImageLink = item["thumbnail"].ToString(),
                            IsLocal = false,
                            Songs = new List<Song>()
                        };
                        if (playlist.ImageLink == null || playlist.ImageLink == "" || playlist.ImageLink == "null")
                        {
                            playlist.ImageLink = "https://i.ibb.co/QJXTF0g/music-playlist.jpg";
                        }
                        if (filter == "" || playlist.Name.Contains(filter))
                            lstPlaylist.Add(playlist);

                    }
                    catch { }
                }
            }
            return lstPlaylist;
        }

        public static async Task<bool> CreatePlaylist(Playlist playlist)
        {
            User user = User.GetInstance();
            if (!user.IsLoggedIn)
            {
                return false; //Like failed
            }
            // User da login
            List<KeyValuePair<string, string>> lst = new List<KeyValuePair<string, string>>();
            lst.Add(new KeyValuePair<string, string>("Name", playlist.Name));
            lst.Add(new KeyValuePair<string, string>("Description", playlist.Description));
            var content = new FormUrlEncodedContent(lst);
            //var content = new StringContent(
            //    JsonConvert.SerializeObject(new { 
            //        Name=playlist.Name,
            //        Description=playlist.Description
            //    }),
            //    Encoding.UTF8,
            //    "application/json"
            //);;
            var url = host + "api/PlayLists";
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + user.Token);
                var response = await client.PostAsync(url, content);
                string result = response.Content.ReadAsStringAsync().Result;
                var jsonRes = JObject.Parse(result);
                int status = Int32.Parse(jsonRes["status"].ToString());
                if (status != 200)
                    return false;
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        public static async Task<bool> DeletePlaylist(Playlist playlist)
        {
            User user = User.GetInstance();
            if (!user.IsLoggedIn || playlist.IsLocal)
            {
                return false; //Like failed
            }
            // User da login
            var url = host + "api/PlayLists?Id="+playlist.ID;
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + user.Token);
                var response = await client.DeleteAsync(url);
                string result = response.Content.ReadAsStringAsync().Result;
                var jsonRes = JObject.Parse(result);
                int status = Int32.Parse(jsonRes["status"].ToString());
                if (status != 200)
                    return false;
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static async Task<bool> UpdatePlaylist(Playlist playlist)
        {
            User user = User.GetInstance();
            if (!user.IsLoggedIn)
            {
                return false; //Like failed
            }
            // User da login
            List<KeyValuePair<string, string>> lst = new List<KeyValuePair<string, string>>();
            lst.Add(new KeyValuePair<string, string>("Name", playlist.Name));
            lst.Add(new KeyValuePair<string, string>("Description", playlist.Description));
            var content = new FormUrlEncodedContent(lst);
            var url = host + "api/PlayLists?Id="+ playlist.ID;
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + user.Token);
                var response = await client.PutAsync(url, content);
                string result = response.Content.ReadAsStringAsync().Result;
                var jsonRes = JObject.Parse(result);
                int status = Int32.Parse(jsonRes["status"].ToString());
                if (status != 200)
                    return false;
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static async Task<bool> AddSongToMyPlaylist(Song song, Playlist playlist)
        {
            User user = User.GetInstance();
            if (!user.IsLoggedIn || playlist.IsLocal||song.IsLocal)
            {
                return false; //Add failed
            }
            // User da login
            var url = host + "api/PlayLists/Push?IdPlayList=" + playlist.ID+ "&IdSong="+song.ID;
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + user.Token);
                var response = await client.PostAsync(url, new StringContent(""));
                string result = response.Content.ReadAsStringAsync().Result;
                var jsonRes = JObject.Parse(result);
                int status = Int32.Parse(jsonRes["status"].ToString());
                if (status != 200)
                    return false;
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static async Task<bool> RemoveSongFromMyPlaylist(Song song, Playlist playlist)
        {
            User user = User.GetInstance();
            if (!user.IsLoggedIn || playlist.IsLocal || song.IsLocal)
            {
                return false; //Add failed
            }
            // User da login
            var url = host + "api/PlayLists/Remove?IdPlayList=" + playlist.ID + "&IdSong=" + song.ID;
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + user.Token);
                var response = await client.DeleteAsync(url);
                string result = response.Content.ReadAsStringAsync().Result;
                var jsonRes = JObject.Parse(result);
                int status = Int32.Parse(jsonRes["status"].ToString());
                if (status != 200)
                    return false;
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public static async Task<bool> RegisterAsync(string userName, string pass, string name, string email,
            string phone, DateTime birth, Plugin.FilePicker.Abstractions.FileData image)
        {
            string contents = System.Text.Encoding.UTF8.GetString(image.DataArray);
            var content = new MultipartFormDataContent();

            // Add the image:
            MemoryStream stream = new MemoryStream(image.DataArray);
            var imageContent = new StreamContent(image.GetStream());
            imageContent.Headers.ContentType = new MediaTypeHeaderValue("image/png");
            content.Add(imageContent, "Thumbnail", image.FileName);

            // Add another parameter:
            var spliter = name.Split(' ');
            var lastName = spliter[spliter.Length - 1];
            var firstName = "";
            for(int i = 0; i < spliter.Length - 2; i++)
            {
                firstName += spliter[i] + " ";
            }
            firstName = firstName.TrimEnd();
            content.Add(new StringContent(firstName), "FirstName");
            content.Add(new StringContent(lastName), "LastName");
            content.Add(new StringContent(birth.ToString("MM/dd/yyyy")), "Dob");
            content.Add(new StringContent(email), "Email");
            content.Add(new StringContent(phone), "PhoneNumber");
            content.Add(new StringContent(userName), "User");
            content.Add(new StringContent(pass), "Password");
            content.Add(new StringContent(pass), "ConfirmPassword");

            var url = host + "api/Users/Register";
            var client = new HttpClient();
            var response = await client.PostAsync(url, content);
            string result = response.Content.ReadAsStringAsync().Result;
            var jsonRes = JObject.Parse(result);
            int status = Int32.Parse(jsonRes["status"].ToString());
            if (status != 200)
                return false;
            return true;
        }

        public static async Task<User> LoginAsync(string userName, string pass)
        {
            User user = new User() { IsLoggedIn=false};
            string url = host + "api/Users/Login";
            var client = new HttpClient();
            var content = new StringContent(
                JsonConvert.SerializeObject(new { user = userName, password = pass, rememberMe= true }),
                Encoding.UTF8,
                "application/json");
            var result = await client.PostAsync(url, content).ConfigureAwait(false);
            if (result.IsSuccessStatusCode)
            {
                var loginRes = await result.Content.ReadAsStringAsync();
                var jsonRes = JObject.Parse(loginRes);
                if (jsonRes["status"].ToString() == "200")
                {
                    var userLoginData = jsonRes["data"];
                    user.ID = userLoginData["userId"].ToString();
                    user.UserName = userLoginData["username"].ToString();
                    user.Token = userLoginData["token"].ToString();
                    user.IsLoggedIn = true;
                }
            }

            if (user.IsLoggedIn)
            {
                try
                {
                    user = await DataService.GetUserInfo(user);
                }
                catch { }
                user.SaveInfoToFile();
                return user;
            }
            else
                return null;
        }

        public static async Task<User> LoginFacebookAsync(string token)
        {
            User user = new User() { IsLoggedIn = false };
            string url = host + "api/Users/LoginWithFacebook?Token="+token;
            var client = new HttpClient();
            var content = new StringContent(
                JsonConvert.SerializeObject(new { }),
                Encoding.UTF8,
                "application/json"
            );
            var result = await client.PostAsync(url, content);
            if (result.IsSuccessStatusCode)
            {
                var loginRes = await result.Content.ReadAsStringAsync();
                var jsonRes = JObject.Parse(loginRes);
                if (jsonRes["status"].ToString() == "200")
                {
                    var userLoginData = jsonRes["data"];
                    user.ID = userLoginData["userId"].ToString();
                    user.UserName = userLoginData["username"].ToString();
                    user.Token = userLoginData["token"].ToString();
                    user.IsLoggedIn = true;
                }
            }

            if (user.IsLoggedIn)
            {
                try
                {
                    user = await DataService.GetUserInfo(user);
                }
                catch { }
                user.SaveInfoToFile();
                return user;
            }
            else
                return null;
        }



        public static async Task<User> GetUserInfo(User RootUser)
        {
            User user = RootUser;
            string url = host + "api/Users/ById?id="+RootUser.ID;
            var client = new HttpClient();
            string strRes = await GetApiAsync(url);
            var json = JObject.Parse(strRes);
            if (json["status"].ToString() == "200")
            {
                var userData = json["data"];
                user.FirstName = userData["firstName"].ToString();
                user.LastName = userData["lastName"].ToString();
                user.PhoneNumber = userData["phoneNumber"].ToString();
                user.Email = userData["email"].ToString();
                user.Role = userData["role"].ToString();
                user.Thumbnail = userData["thumbnail"].ToString();
                user.DayOfBirdth = DateTime.Parse(userData["dob"].ToString());
            }
            return user;
        }

        public static async Task<Song> LikeOrDisLikeAsync(bool isLike, Song song)
        {
            User user = User.GetInstance();
            if (!user.IsLoggedIn || song.IsLocal)
            {
                return null; //Like failed
            }
            // User da login
            var content = new StringContent(
                JsonConvert.SerializeObject(new { }),
                Encoding.UTF8,
                "application/json"
            );
            var url = "";
            if (isLike)
            {
                url = host + "api/Songs/Like?Id=" + song.ID;
            }
            else
            {
                url = host + "api/Songs/UnLike?Id=" + song.ID;
            }
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + user.Token);
                var response = await client.PostAsync(url, content);
                string result = response.Content.ReadAsStringAsync().Result;
                var jsonRes = JObject.Parse(result);
            }
            catch {
                return null;
            }

            Song new_info = await GetSongByIDAsync(song.ID);
            return new_info;

        }

        public static async Task<Playlist> LikeOrDisLikeAsync(bool isLike, int playlistID)
        {
            User user = User.GetInstance();
            if (!user.IsLoggedIn)
            {
                return null; //Like failed
            }
            // User da login
            var content = new StringContent(
                JsonConvert.SerializeObject(new { }),
                Encoding.UTF8,
                "application/json"
            );
            var url = "";
            if (isLike)
            {
                url = host + "api/PlayLists/Like?Id=" + playlistID;
            }
            else
            {
                url = host + "api/PlayLists/UnLike?Id=" + playlistID;
            }
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + user.Token);
                var response = await client.PostAsync(url, content);
                string result = response.Content.ReadAsStringAsync().Result;
                var jsonRes = JObject.Parse(result);
            }
            catch
            {
                return null;
            }
            Playlist new_info = await GetPlaylistInforOnlyAsync(playlistID.ToString());
            return new_info;
        }


        public static async Task<Song> GetSongByIDAsync(int id)
        {
            try
            {
                string songUrl = host + @"api/Songs/ById?Id=" + id;
                string strSong = await GetApiAsync(songUrl);
                var songJson = JObject.Parse(strSong);
                int status = Int32.Parse(songJson["status"].ToString());
                if (status != 200)
                    return null;

                var item = songJson["data"];
                Song song = new Song()
                {
                    ID = Int32.Parse(item["id"].ToString()),
                    Name = item["name"].ToString(),
                    DateCreate = item["dateCreate"].ToString(),
                    Duration = Int32.Parse(item["duration"].ToString()),
                    DurationString = Song.GetDurationString(Int32.Parse(item["duration"].ToString())),
                    ImageLink = item["thumbnail"].ToString(),
                    LinkLyric = item["lyric"].ToString(),
                    LinkMp3 = item["fileMusic"].ToString(),
                    TotalLike = Int32.Parse(item["totalLike"].ToString()),
                    TotalListen = Int32.Parse(item["totalListen"].ToString()),
                    Singer = item["owners"][0]["nameOwner"].ToString().Trim(),
                    IsLocal = false,
                };
                return song;
            }
            catch
            {
                return null;
            }
        }
    
        public static async Task<List<Comment>> GetCommentsAsync(int id, int type=0)
        {
            // Type=0 => song, type=1 => playlist
            try
            {
                string url = "";
                if (type==0)
                    url = host + @"api/Songs/Comment?Id="+id+"&Index=1&PageSize=20";
                string strComments = await GetApiAsync(url);
                var commentsJson = JObject.Parse(strComments);
                int status = Int32.Parse(commentsJson["status"].ToString());
                if (status != 200)
                    return new List<Comment>();

                var list = commentsJson["data"];
                List<Comment> lstComments = new List<Comment>();
                foreach(var item in list)
                {
                    DateTime dt = DateTime.Parse(item["dateTime"].ToString());
                    string userID = item["userId"].ToString();
                    User user = await GetUserInfo(new User() { ID = userID });
                    string avatar;
                    if (user.Thumbnail == "" || user.Thumbnail == null)
                        avatar = "m_musicicon.png";
                    else
                        avatar = user.Thumbnail;
                    Comment cmt = new Comment()
                    {
                        ID = 0,
                        UserName = item["firstName"].ToString()+ " " + item["lastName"].ToString(),
                        Content = item["content"].ToString(),
                        Time = dt.ToString("dd/MM/yyyy-HH:mm"),
                        ImageLink = avatar,
                    };
                    lstComments.Add(cmt);
                }
                
                return lstComments;
            }
            catch
            {
                return new List<Comment>();
            }
        }

        public static async Task<bool> CommentAsync(int id, string text, int type = 0)
        {
            // Type=0 => song, type=1 => playlist
            User user = User.GetInstance();
            if (!user.IsLoggedIn)
            {
                return false; //Like failed
            }
            // User da login
            var content = new StringContent(
                JsonConvert.SerializeObject(new {
                    objId = id,
                    content = text
                }),
                Encoding.UTF8,
                "application/json"
            );
            var url = "";
            if (type==0)
            {
                url = host + "api/Songs/Comment";
            }
            else
            {
                url = host + "api/PlayLists/Comment";
            }
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + user.Token);
                var response = await client.PostAsync(url, content);
                string result = response.Content.ReadAsStringAsync().Result;
                var jsonRes = JObject.Parse(result);
                if (jsonRes["status"].ToString() == "200")
                    return true;
                return false;
            }
            catch
            {
                return false;
            }
        }

        public static void ListenSong(int id, int type = 0)
        {
            // type=0 -> song, type=1 -> playlist
            var content = new StringContent(
                JsonConvert.SerializeObject(new { }),
                Encoding.UTF8,
                "application/json"
            );
            var url = "";
            if (type == 0)
            {
                url = host + "api/Songs/Listen?Id=" + id;
            }
            else
            {
                return;
            }
            try
            {
                var client = new HttpClient();
                var response = HttpClientExtensions.PatchAsync(client, new Uri(url), content);
                //string result = await response.Result.Content.ReadAsStringAsync();
                //var jsonRes = JObject.Parse(result);
            }
            catch
            {
            }
        }
        
        public static async Task<User> CheckUserIsLoggedinAsync(User user)
        {
            try
            {
                string url = host + "api/Users/CurrentUser";
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + user.Token);
                var response = await client.GetAsync(url);
                string result = response.Content.ReadAsStringAsync().Result;
                var jsonRes = JObject.Parse(result);
                if (jsonRes["status"].ToString() == "200")
                {
                    var userLoginData = jsonRes["data"];
                    user.IsLoggedIn = true;
                    user.UserName = userLoginData["userName"].ToString();
                    user.FirstName = userLoginData["firstName"].ToString();
                    user.LastName = userLoginData["lastName"].ToString();
                    user.PhoneNumber = userLoginData["phoneNumber"].ToString();
                    user.Thumbnail = userLoginData["thumbnail"].ToString();
                    user.Email = userLoginData["email"].ToString();
                    user.Role = userLoginData["role"].ToString();
                    user.DayOfBirdth = DateTime.Parse(userLoginData["dob"].ToString());
                    return user;
                }
                //else if(jsonRes["status"].ToString() == "401")
                //{
                //    user.IsLoggedIn = false;
                //    return user;
                //}
            }
            catch
            {
                // return null;
            }
            user.IsLoggedIn = false;
            return user;
        }
    
        public static async Task<User> UpdateUserInfo(User new_info)
        {
            // Type=0 => song, type=1 => playlist
            User user = User.GetInstance();
            if (!user.IsLoggedIn)
            {
                return null;
            }
            // User da login
            var content = new MultipartFormDataContent();
            content.Add(new StringContent(new_info.DayOfBirdth.ToString("MM/dd/yyyy")), "Dob");
            content.Add(new StringContent(new_info.Email), "Email");
            content.Add(new StringContent(new_info.FirstName), "FirstName");
            content.Add(new StringContent(new_info.LastName), "LastName");
            content.Add(new StringContent(new_info.PhoneNumber), "PhoneNumber");
            
            var url = host + "api/Users?Id="+new_info.ID;
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + user.Token);
                var response = await client.PutAsync(url, content);
                string result = response.Content.ReadAsStringAsync().Result;
                var jsonRes = JObject.Parse(result);
                if (jsonRes["status"].ToString() == "200")
                    return new_info;
                return null;
            }
            catch
            {
                return null;
            }
        }
    
        public static async Task<List<User>> GetAllFolowFriendAsync()
        {
            User user = User.GetInstance();
            if (!user.IsLoggedIn)
            {
                return new List<User>();
            }
            var url = host + "api/Users/AllStatusFriend";
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + user.Token);
                var response = await client.GetAsync(url);
                string result = response.Content.ReadAsStringAsync().Result;
                var jsonRes = JObject.Parse(result);
                if (jsonRes["status"].ToString() != "200")
                    return new List<User>();
                var list = jsonRes["data"];
                List<User> lstFriends = new List<User>();
                foreach (var item in list)
                {
                    string userID = item["receiverId"].ToString();
                    User friend = await GetUserInfo(new User() { ID = userID });
                    string avatar;
                    if (user.Thumbnail == "" || user.Thumbnail == null)
                        avatar = "ni_user_blue.png";
                    else
                        avatar = user.Thumbnail;
                    friend.Thumbnail = avatar;
                    lstFriends.Add(friend);
                }
                return lstFriends;
            }
            catch
            {
                return new List<User>();
            }
        }
        
        public static async void SendFolowRequest(string phone)
        {
            try
            {
                User user = User.GetInstance();
                if (!user.IsLoggedIn)
                {
                    return;
                }
                var url = host + "api/Users/ByPhone?ListPhone=" + phone + "&Index=1&PageSize=1";
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + user.Token);
                var response = await client.GetAsync(url);
                string result = response.Content.ReadAsStringAsync().Result;
                var jsonRes = JObject.Parse(result);
                if (jsonRes["status"].ToString() != "200")
                    return;
                var list = jsonRes["data"];
                foreach (var item in list)
                {
                    string userID = item["id"].ToString();
                    var content = new StringContent(
                        JsonConvert.SerializeObject(new { }),
                        Encoding.UTF8,
                        "application/json"
                    );
                    url = host + "api/Users/SendFriendRequest?ReceiverId=" + userID;
                    var response2 = await client.PostAsync(url, content);
                    string result2 = response2.Content.ReadAsStringAsync().Result;
                }
            }
            catch { }
        }
     
        public static async Task<bool> CheckLikeSongAsync(int songID)
        {
            User user = User.GetInstance();
            if (!user.IsLoggedIn)
            {
                return false;
            }
            var url = host + "api/Songs/CheckLike?Id="+songID;
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + user.Token);
                var response = await client.GetAsync(url);
                string result = response.Content.ReadAsStringAsync().Result;
                var jsonRes = JObject.Parse(result);
                if (jsonRes["status"].ToString() != "200")
                    return false;
                return (jsonRes["data"].ToString().ToLower() == "true");
            }
            catch
            {
                return false;
            }
        }

    }
}
