﻿using BigBluebirdsMusic.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace BigBluebirdsMusic.Services
{
    public class LocalDatabaseService
    {
        string folder = System.Environment.GetFolderPath
        (System.Environment.SpecialFolder.Personal);
        public bool CreateDatabase()
        {
            try
            {
                string path = System.IO.Path.Combine(folder, "bbbmusic.db");
                // System.IO.File.Delete(path);
                bool isDBExists = System.IO.File.Exists(path);
                var connection = new SQLiteConnection(path);
                connection.CreateTable<Playlist>();
                connection.CreateTable<Song>();
                return true;
            }
            catch
            {
                return false;
            }
        }

        // Them Playlist
        public bool AddPlaylist(Playlist p)
        {
            try
            {
                string path = System.IO.Path.Combine(folder, "bbbmusic.db");
                var connection = new SQLiteConnection(path);
                connection.Insert(p);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public List<Playlist> GetAllPlaylist(string filter="")
        {
            try
            {
                string path = System.IO.Path.Combine(folder, "bbbmusic.db");
                var connection = new SQLiteConnection(path);
                if (filter == "")
                    return connection.Table<Playlist>().ToList();
                else
                    return connection.Table<Playlist>().Where(x => x.Name.Contains(filter)).ToList();
            }
            catch
            {
                return null;
            }
        }

        public bool AddSongToPlaylist(Song song, Playlist playlist)
        {
            try
            {
                song.LocalPlayListID = playlist.LocalID;
                string path = System.IO.Path.Combine(folder, "bbbmusic.db");
                var connection = new SQLiteConnection(path);
                connection.Insert(song);
                Playlist new_playlist = connection.Table<Playlist>().Where(x => x.LocalID == playlist.LocalID).First();
                new_playlist.NumberSong = new_playlist.NumberSong + 1;
                connection.Update(new_playlist);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool RemoveSongFromPlaylist(Song song, Playlist playlist)
        {
            try
            {
                song.LocalPlayListID = playlist.LocalID;
                string path = System.IO.Path.Combine(folder, "bbbmusic.db");
                var connection = new SQLiteConnection(path);
                connection.Delete(song);
                if (playlist.NumberSong > 0)
                {
                    playlist.NumberSong = playlist.NumberSong - 1;
                    connection.Update(playlist);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<Song> GetListSongsOfLocalPlaylist(Playlist playlist)
        {
            try
            {
                string path = System.IO.Path.Combine(folder, "bbbmusic.db");
                var connection = new SQLiteConnection(path);
                return connection.Table<Song>().Where(x=>x.LocalPlayListID==playlist.LocalID).ToList();
            }
            catch
            {
                return null;
            }
        }
    
        public bool UpdatePlayList(Playlist playlist_newinfo)
        {
            try
            {
                string path = System.IO.Path.Combine(folder, "bbbmusic.db");
                var connection = new SQLiteConnection(path);
                connection.Update(playlist_newinfo);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool DeletePlayList(Playlist playlist)
        {
            try
            {
                string path = System.IO.Path.Combine(folder, "bbbmusic.db");
                var connection = new SQLiteConnection(path);
                List<Song> songs = GetListSongsOfLocalPlaylist(playlist);
                foreach(Song song in songs)
                {
                    connection.Delete(song);
                }
                connection.Delete(playlist);
                return true;
            }
            catch
            {
                return false;
            }
        }


    }
}
