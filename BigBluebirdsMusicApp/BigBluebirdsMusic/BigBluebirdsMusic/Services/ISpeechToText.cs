﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BigBluebirdsMusic.Services
{
    public interface ISpeechToText
    {
        Task<string> SpeechToTextAsync();
    }
}
