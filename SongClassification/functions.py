import json
import os
import math
import librosa
import numpy as np
from pydub import AudioSegment


SAMPLE_RATE = 22050
TRACK_DURATION = 30 # measured in seconds
SAMPLES_PER_TRACK = SAMPLE_RATE * TRACK_DURATION
SONG_FILES_PATH = r'song_files'
TMP_FILES_PATH = r'tmp'

def file_to_mfcc(file_path, num_mfcc=13, n_fft=2048, hop_length=512, num_segments=10):
    # dictionary to store mapping, labels, and MFCCs
    data = {
        "mfcc": []
    }

    samples_per_segment = int(SAMPLES_PER_TRACK / num_segments)
    num_mfcc_vectors_per_segment = math.ceil(samples_per_segment / hop_length)

    signal, sample_rate = librosa.load(file_path, sr=SAMPLE_RATE)
    # process all segments of audio file
    for d in range(num_segments):
        # calculate start and finish sample for current segment
        start = samples_per_segment * d
        finish = start + samples_per_segment
        # extract mfcc
        mfcc = librosa.feature.mfcc(signal[start:finish], sample_rate, n_mfcc=num_mfcc, n_fft=n_fft, hop_length=hop_length)
        mfcc = mfcc.T
        # store only mfcc feature with expected number of vectors
        if len(mfcc) == num_mfcc_vectors_per_segment:
            data["mfcc"].append(mfcc.tolist())
    new_data = np.array(data["mfcc"])
    new_data = new_data[..., np.newaxis]
    return new_data

def cut_and_convert(file_full_name, file_name, start_second=30):
    file_path = os.path.join(SONG_FILES_PATH, file_full_name)
    dst_wav = os.path.join(TMP_FILES_PATH, file_name + ".wav")
    sound = AudioSegment.from_mp3(file_path)
    startTime = start_second*1000
    endTime = startTime+30*1000
    extract = sound[startTime:endTime]
    extract.export(dst_wav, format="wav")
    return file_to_mfcc(dst_wav)
