ENVIRONMENT

# Python version
```
python 3.7.9
```

# Install package
```
poetry install
```

If any error occurs, please install 2 packages by using pip:

```
pip install matplotlib
```
```
pip install tensorflow
```

# Run server
Config file path in functions.py
```
SONG_FILES_PATH = r'song_files'
TMP_FILES_PATH = r'tmp'
```

Run
```
python server.py
```

Get song type params example
```
{
    "song_name":"Helo",
    "song_id":123,
    "song_code":"00011",
    "file_name":"pop_Ikissedthegirl-KatyPerry_5srj.mp3"
}
```
